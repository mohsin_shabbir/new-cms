<?php 		
 		include_once("common.php");
		include_once("includes/header.php");
?>
        <script type="text/javascript" >
			$(function() {
			$(".say_it").click(function(){
		    var userViewId = $(this).attr("id");			
			//var tbl = 'tbl_property';
			var info = 'configurationView=' + userViewId;
			$("#loader").css("display", "block");
			$.ajax({
			type: "POST",
			url: "getAddress.php",
			data: info,
			success: function(data){
				$(".showPopup").show(1000);
				$("#loader").css("display", "none");
				$("#response").html(data);
				
				//location.reload();
			}
			});
			$(this).parents(".gradeA").animate({ backgroundColor: "red" }, "fast")
			.animate({ opacity: "show" }, "slow");
			
			return false;
			});
			});
			function closePopup()
			{
				$(".showPopup").hide(1000);
    //// 3.  $.window.close();
			}
         </script>
        <script type="text/javascript" >

			$(function() {
			
			$(".delbutton").click(function(){
			var del_id = $(this).attr("id");
			
			var tbl = 'tbl_app_configurations';
			var info = 'id=' + del_id+'&tbl='+ tbl;
			if(confirm("Sure you want to delete this record? There is NO undo!"))
			{
			$.ajax({
			type: "POST",
			url: "delete.php",
			data: info,
			success: function(){
			}
			});
			$(this).parents(".gradeA").animate({ backgroundColor: "red" }, "fast")
			.animate({ opacity: "hide" }, "slow");
			}
			return false;
			});
			});
         </script>
	 <!-- DataTables CSS -->
<link rel="stylesheet" href="plugins/datatables/dataTables.bootstrap.css">
<div class="content-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">View Configurations</h1>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                          <?php if($_REQUEST['mode'] == "add" || $_REQUEST['mode'] == "update" || $_REQUEST['mode'] == "delete"){?>
                          <div class="alert alert-success alert-dismissable">
                                   <button aria-hidden="true" data-dismiss="alert" class="close" type="button">�</button>
                                                   
						        	<strong>Well done!</strong> You have successfully complete this operation.
                     			   </div>
                         <?php } 
						   //if($_SESSION['user_role']==1 || $_SESSION['user_role']==2){?>  <!--<a href="add_app_configurations.php">Add Configurations</a> --><?php //} ?>
						   
                         
                        </div>
                        <!-- /.panel-heading -->
                        <div class="panel-body">
                            <div class="table-responsive">
							<img src="images/loader.gif" id="loader" />
                                <table class="table table-striped table-bordered table-hover" id="dataTables-example">
                                    <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>Feeds Distance</th>
                                            <th>Feeds Limit</th>
                                            <th>Nearby Distance</th>
                                            <th>Nearby Limit</th>
                                            <th>Trending Week Limit</th>
											<th>Created At</th>
                                            <th>Updated At</th>  
                                            <th>Status</th>
                                            <th width="18%">Options</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                <?php	
				$sql = "select * from tbl_app_configurations order by config_id";
				$rs = mysql_query($sql) or die(mysql_error());
				if(mysql_num_rows($rs) == 0)
				global $i;
				$i = 1;
				while($oA=@mysql_fetch_array($rs))
				{
				  
				?>
                             <tr class="gradeA checkStyle" <?php if(stripslashes(strip_tags($oA['status'])) == 0){?> style="color:red" <?php } ?> onclick="return checkStyle();" id = <?php echo $oA["config_id"]; ?>>
                                            <td><?php print $i; ?></td>
                                            <td><?php print stripslashes(strip_tags($oA['local_feeds_distance'])); ?></td>
                                            <td><?php print stripslashes(strip_tags($oA['local_feeds_limits'])); ?></td>
                                            <td><?php print stripslashes(strip_tags($oA['nearby_distance'])); ?></td>
                                            <td><?php print stripslashes(strip_tags($oA['nearby_limit'])); ?></td>
											<td><?php print stripslashes(strip_tags($oA['trending_this_week_limit'])); ?></td>
                                             <td><?php 	$seconds = $oA['created_at'] / 1000;echo date("d/m/Y H:i:s", $seconds); ?></td>
											 <td><?php 	$seconds = $oA['updated_at'] / 1000;echo date("d/m/Y H:i:s", $seconds); ?></td>			
                                            <td class="center">
											<?php 
                                            switch($oA['status']){
                                            case 1:
                                            print 'Active';
                                            break;
                                            case 0:
                                            print '<font color="red">Inactive</font>';
                                            break;
                                            }
                                            ?>
                                            </td>
                                            <td class="center">
											 
                          
														<a class="initialism fadeandscale_open btn btn-success say_it" href="#fadeandscale" id = <?php echo $oA["config_id"]; ?>>View </a>
														<a class="btn btn-info" title="Edit" href="add_app_configurations.php?cid=<?php print $oA['config_id']; ?>">
														<i class="icon-edit icon-white"></i>  
														Edit                                            
														</a>
											<?php 
						  					 if($_SESSION['user_role']==1 || $_SESSION['user_role']==2){?>  
														
														<!--<a class="btn btn-danger delbutton" href="#" title="Delete" id = <?php echo $oA["config_id"]; ?>>
														<i class="icon-trash icon-white"></i> 
														Delete
														</a>-->
                                            <?php } $updated_at = round(microtime(true) * 1000);
										?>
										<input type="hidden" class="form-control" required  name="updated_at" id="updated_at" value="<?php print $updated_at;?>" >
                                            </td>
                                        </tr>
                                   
               <?php $i++;} ?>
               					 </tbody>
                                </table>
                            </div>
                            <!-- /.table-responsive -->
                            
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->

        </div>
        <!-- /#page-wrapper -->
<?php 
include_once("includes/footer.php");
?>
 <!-- DataTables JavaScript -->
    <script src="js/plugins/dataTables/jquery.dataTables.js"></script>
    <script src="js/plugins/dataTables/dataTables.bootstrap.js"></script>
    <script>
  $(document).ready(function() { 
    $('[data-toggle="tooltip"]').tooltip();  
       /// $('#dataTables-example').dataTable();
		 $('#dataTables-example').DataTable({
          "paging": true,
          "lengthChange": true,
          "searching": true,
          "ordering": true,
          "info": true,
          "autoWidth": true,
		  "scrollX": true
        });
    });
    </script>
	<style>
#dataTables-example {
    display: table;
    white-space: nowrap;
}
</style>