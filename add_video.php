<?php  include_once("common.php");
class Data extends Function_Library
{
	function Save()
	{

		parent::Save("ce_videos");
		$id = @mysql_insert_id();
		//header("Location: view_about_us.php?mode=add&cid=".$id);
		?>
			<script type="text/javascript">
            window.location="view_videos.php?mode=add&cid="<?php $id ?>;
            </script>			
		<?php
	
				
		}
		
		function Update()
		{
		   
			parent::Update("ce_videos");
			$id=$_REQUEST['id'];
			//header("Location: view_about_us.php?mode=update&cid=".$_REQUEST['id']);
			?>
			<script type="text/javascript">
            window.location="view_videos.php?mode=update&cid="<?php $id ?>;
            </script>
			<?php
			
			
		}
		
		function ShowDesign()
		{			
			$rs_art = @mysql_query("select * from ce_videos where video_id='".$_REQUEST['cid']."'");
			$row_art = @mysql_fetch_object($rs_art);
			include_once("includes/header.php");
?>

        <div class="content-wrapper">
          <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header"><?php if($_REQUEST['cid']){?>Edit <?php }else{?>Add <?php } ?> Video</h1>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            Add Video Form
                        </div>
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-lg-10 col-md-offset-1 AlignFormCenter" >
                                	<form  name="frm" id="frm" action="" method="post" data-parsley-validate=""  enctype="multipart/form-data">
									<?php
                                    if($_REQUEST['cid'])
                                    {
                                    	print "<input type='hidden' name='video_id' value='".$_REQUEST['cid']."'>";
                                    }
                                    ?>
                                    <?php if($_REQUEST['mode'] == "exist"){?>
                                    
                                    <div class="alert alert-danger alert-dismissable">
                                   <button aria-hidden="true" data-dismiss="alert" class="close" type="button">�</button>
                                    <h4 class="alert-heading">Warning!</h4>                            
						        	<p>Record Already Exit!</p>
                     			   </div>
                                    
                                    <?php } ?>
                                    
                                    	
                                        <div class="form-group">
                                            <label>Name</label>
                                             <input type="text" class="form-control" required  name="video_name" id="video_name" value="<?php print stripslashes($row_art->video_name);?>" >
                                        </div>
										
										<div class="control-group">
                                      <label class="control-label" for="fileInput">Upload Video</label>
                                      <div class="controls">
                                        <input class="input-file uniform_on" id="video_url" name="video_url" type="file">
                                        <input type="hidden" name="video_url_old" id="video_url_old" value="<?php print $row_art->video_url; ?>" />
                                        <?php 
                                        if($row_art->video_url !=''){
                                        ?>
                                                                     
                                               <div class="box-content">
                                                   <div class="row-fluid">
                                                     <?php print '<img src="'.$GLOBALS['upload_thumb_directory'].$row_art->video_url.'" border="0" />'; ?>
                                                   </div>                   
                                               </div>
                                           
                                        <?php
                                         } 
                                        ?>
                                      </div>
                                    </div>
									<br />
									 <div class="col-lg-12 AlignFormCenter" >								
                                     <div class="control-group">
                                      <label>Video Thumbnail </label>
                                      <div class="controls">
                                      <script>
										  var loadFile = function(event) {
										    var output = document.getElementById('output');
										    output.src = URL.createObjectURL(event.target.files[0]);
										  };
										</script>
                                        <input id=thumbnail_photo name="thumbnail_photo" type="file"  onchange="loadFile(event)">
                                        
                                        <input type="hidden" name="thumbnail_photo_old" id="thumbnail_photo_old" value="<?php print $row_art->thumbnail_photo; ?>" />
                                                                                                          
                                          <span class="pip2">
                                         <?php 
                                        $src = '';
                                        if($row_art->thumbnail_photo !=''){
                                        	$src = $GLOBALS['upload_thumb_directory'].$row_art->thumbnail_photo; }
                                        	else
                                        	{
                                        		$src = 'images/img-place.jpg';
                                        	}
                                        ?>  
			                                       <img class="imageThumb4" src="<?php echo $src; ?>"  alt="" id="output"  width="200" height="100"> 
				                                     
				                        <?php                                      
                                        if($row_art->thumbnail_photo !=''){
				                                     /*  <span class="remove2"> 
				                                     	 <a class="delbutton_del" style="color: #FF0000" href="javascript:" name="<?php echo $row_art->thumbnail_photo; ?>" title="Delete" id="<?php echo $_REQUEST['cid']; ?>">Remove Image	</a>
				                                     </span> */
				                                     } ?>
			                           </span>
                                           
                                        
                                      </div>
                                    
                                    </div>
                                    
								 </div>
								 
					
                                        <div class="form-group">
                                            <label>Description</label>
                                           <textarea name="video_short_description"  required id="video_short_description" class="ckeditor"><?php print stripslashes($row_art->video_short_description);?></textarea>
										    <div id="descriptionErrorDiv" class="errorDiv">          
                                       		 <label class="error" id="description_error"></label>
                                       	    </div>
                                        </div>
										<?php 
										$created_at = 0;										
										if(isset($_REQUEST['cid']) && $_REQUEST['cid'] > 0)
										{
											$created_at = $row_art->created_at;	
											$updated_at = round(microtime(true) * 1000);
											?>
											<input type="hidden" class="form-control" required  name="updated_at" id="updated_at" value="<?php print $updated_at;?>" >										
										<?php }
										else
										{
											$created_at = round(microtime(true) * 1000);	
										}
										?>
										<input type="hidden" class="form-control" required  name="created_at" id="created_at" value="<?php print $created_at;?>" >
                                        <div class="form-group">
                                            <label>Status</label>
                                            <select class="form-control" name="video_status" id="video_status">
                                                <option value="1" <?php if($row_art->video_status == "1") print 'selected';?>>Active</option>
                                                <option value="0" <?php if($row_art->video_status == "0") print 'selected';?>>Inactive</option>
                                            </select>
                                            
                                        </div>                                     
                                        <?php
											if($_SESSION['user_role'] != 3)
											{
												?> 
											<input type="hidden" name="approved" id="approved" value="1" />  
											<?php } ?> 
                                        <button type="submit" class="btn btn-default" name="cmdSave">Save</button>
                                        <button type="reset"  onclick="goBack()" class="btn btn-default">Cancel</button>
                                    </form>
                                </div>
                                <!-- /.col-lg-6 (nested) -->
                                
                                <!-- /.col-lg-6 (nested) -->
                            </div>
                            <!-- /.row (nested) -->
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
        </div>
        <!-- /#page-wrapper -->
<?php 
include_once("includes/footer.php");
		}
		}	
		$oA = new Data;
		if(isset($_REQUEST['cmdSave']) or isset($_REQUEST['cmdUser']))
		{
			////////////////////////////////////////////Image Thumbnail/////////////////////
			if ($_FILES["thumbnail_photo"]['tmp_name']!='')
			{
					
				$sqlFile1 = "select thumbnail_photo from ce_videos where video_id='".$_REQUEST['cid']."'";
					
				$resFile1 = mysql_query($sqlFile1) or die(mysql_error());
				$rowFile1 = mysql_fetch_object($resFile1);
				@unlink($GLOBALS['upload_directory'].$rowFile1->thumbnail_photo);
				@unlink($GLOBALS['upload_thumb_directory'].$rowFile1->thumbnail_photo);
				$nnn1=date('YmdHis');
				$fileArray1 = explode('.',$_FILES["thumbnail_photo"]['name']);
				$fileType1 = end($fileArray1);
				$thumbnail_photo = "out_thumb_".$nnn1.$fileArray1[0]. ".".$fileType1;
				$dest_path_thumb1 = $GLOBALS['upload_thumb_directory'].$thumbnail_photo;
				$dest_path1 = $GLOBALS['upload_directory'].$thumbnail_photo;
				$oA->createthumb($_FILES["thumbnail_photo"]["tmp_name"],$dest_path_thumb1,$thumbnail_photo,$thumb_width,$thumb_height); // width, height
				copy($_FILES["thumbnail_photo"]["tmp_name"],$dest_path1);
				$_REQUEST['thumbnail_photo']=$thumbnail_photo;
				if($_FILES['thumbnail_photo']['name'] =="" )
				{
					$thumbnail_photo = $_POST['thumbnail_photo_old'];
				}
				else {
					$thumbnail_photo = $thumbnail_photo;
				}
			}
			///////////////////////////////////////////////////////////////////////////////
			
			if ($_FILES["video_url"]['tmp_name']!='')
	           {
			   
				$sqlFile1 = "select video_url from ce_videos where video_id='".$_REQUEST['cid']."'";
				$resFile1 = mysql_query($sqlFile1) or die(mysql_error());
				$rowFile1 = mysql_fetch_object($resFile1);
				@unlink($GLOBALS['upload_directory'].$rowFile1->video_url);
				@unlink($GLOBALS['upload_thumb_directory'].$rowFile1->video_url);
				$nnn1=date('YmdHis');
				$fileArray1 = explode('.',$_FILES["video_url"]['name']);
				$fileType1 = end($fileArray1);
				$video_url = "event_".$nnn1.$fileArray1[0]. ".".$fileType1; 
				$dest_path_thumb1 = $GLOBALS['upload_thumb_directory'].$video_url;				
				$dest_path1 = $GLOBALS['upload_directory'].$video_url;
				$oA->createthumb($_FILES["video_url"]["tmp_name"],$dest_path_thumb1,$video_url,$thumb_width,$thumb_height); // width, height				
				copy($_FILES["video_url"]["tmp_name"],$dest_path1);
				$_REQUEST['video_url']=$video_url;
					if($_FILES['video_url']['name'] =="" )
					 {
						$video_url = $_POST['video_url_old'];
					 }
					 else {
					 $video_url = $video_url;   
					 }
	}
		
			if($_REQUEST['cid']=='')
			{
				$oA->Save();				
			}
			else
			{
				
				$oA->Update();				
			}
		}
		else
		{		
			$oA->ShowDesign();
		}
?>
<script>
$(document).ready(function() {
	  $('#video_url').on('change', function(evt) {
		  var f = this.files[0];
		  var fileName = f.name;	
		  var fileSize = f.size;		
		  var extension = getExtension(fileName);	
		  if(fileSize>6000000 || extension != 'mp4')
		  {
			alert('Please make sure the size should be less than 5MB and type should be mp4');
            return false;
		  }
		  
	  });
	});
</script>