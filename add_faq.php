	<?php  include_once("common.php");
    class Data extends Function_Library
	{
		function Save()
		{			
			parent::Save("ce_faq");		
			$id = @mysql_insert_id();			
			 ?>
			<script type="text/javascript">
            window.location="view_faq.php?mode=add&cid="<?php $id ?>;
            </script>			
		<?php
	
				
		}
		
		function Update()
		{
		    parent::Update("ce_faq");
			$id=$_REQUEST['id'];
			//header("Location: view_about_us.php?mode=update&cid=".$_REQUEST['id']);
			?>
			<script type="text/javascript">
            window.location="view_faq.php?mode=update&cid="<?php $id ?>;
            </script>
			<?php
			
			
		}
		
		function ShowDesign()
		{			
			$rs_art = @mysql_query("select * from ce_faq where faq_id='".$_REQUEST['cid']."'");
			$row_art = @mysql_fetch_object($rs_art);
			include_once("includes/header.php");
?>
		  

       <div class="content-wrapper">
          <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header"><?php if($_REQUEST['cid']){?>Edit <?php }else{?>Add <?php } ?> FAQ</h1>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            Add FAQ Form
                        </div>
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-lg-10 col-md-offset-1 AlignFormCenter" >
                                	<form  name="frm" id="frm" action="" method="post" role="form"  data-parsley-validate="">
									<?php
                                    if($_REQUEST['cid'])
                                    {
                                    	print "<input type='hidden' name='faq_id' value='".$_REQUEST['cid']."'>";
                                    }
                                    ?>
                                    <?php if($_REQUEST['mode'] == "exist"){?>
                                    
                                    <div class="alert alert-danger alert-dismissable">
                                   <button aria-hidden="true" data-dismiss="alert" class="close" type="button">�</button>
                                    <h4 class="alert-heading">Warning!</h4>                            
						        	<p>Record Already Exit!</p>
                     			   </div>
                                    
                                    <?php } 
									?>
				
                                        <div class="form-group">
                                            <label>Select Category</label>
                                            <select class="form-control" name="faq_category" id="faq_category" required>
                                                <option value="Basics" <?php if($row_art->faq_category == "Basics") print 'selected';?>>Basics</option>
                                                <option value="Children & Guests" <?php if($row_art->faq_category == "Children & Guests") print 'selected';?>>Children & Guests</option>
												 <option value="Nominating a Partner" <?php if($row_art->faq_category == "Nominating a Partner") print 'selected';?>>Nominating a Partner</option>
                                                <option value="Payments & Fees" <?php if($row_art->faq_category == "Payments & Fees") print 'selected';?>>Payments & Fees</option>
												 <option value="Delivery" <?php if($row_art->faq_category == "Delivery") print 'selected';?>>Delivery</option>
                                                <option value="Corporate" <?php if($row_art->faq_category == "Corporate") print 'selected';?>>Corporate</option>
                                            </select>
                                        </div> 
										<?php 
										$created_at = 0;										
										if(isset($_REQUEST['cid']) && $_REQUEST['cid'] > 0)
										{
											$created_at = $row_art->created_at;	
											
											?>
																				
										<?php }
										else
										{
											$created_at = round(microtime(true) * 1000);	
										}
										$updated_at = round(microtime(true) * 1000);
										?>
										<input type="hidden" class="form-control" required  name="updated_at" id="updated_at" value="<?php print $updated_at;?>" >
										<input type="hidden" class="form-control" required  name="created_at" id="created_at" value="<?php print $created_at;?>" >
                                        
                                         
										<div class="col-lg-12 AlignFormCenter" >
                                        <div class="form-group">
                                            <label>Question</label>
                                           <textarea name="faq_question" id="faq_question" class="ckeditor" required><?php print stripslashes($row_art->faq_question);?></textarea>
										    
                                        </div>
								 </div>
										<div class="col-lg-12 AlignFormCenter" >
                                        <div class="form-group">
                                            <label>Answer</label>
                                           <textarea name="faq_answer" id="faq_answer" class="ckeditor" required><?php print stripslashes($row_art->faq_answer);?></textarea>
										    
                                        </div>
								 </div>
										  <div class="form-group">
                                            <label>Status</label>
                                            <select class="form-control" name="status" id="status" <?php if($_SESSION['user_role'] != 1){ echo "disabled"; } ?>>
											<?php 
										
											if($_SESSION['user_role'] == 2 || $_SESSION['user_role'] == 3)
											{
												?>
													<option value="0" selected="selected" >Inactive</option>
												<?php
											}
											else
											{
											?>
                                                <option value="1" <?php if($row_art->status == "1") print 'selected';?>>Active</option>
                                                <option value="0" <?php if($row_art->status == "0") print 'selected';?>>Inactive</option>
										<?php } ?>
                                            </select>                                           
                                        </div>
										  <div class="form-group">
                                            <label>Is Approved?</label>
                                            <select class="form-control" name="approved" id="approved" <?php if($_SESSION['user_role'] != 1){ echo "disabled"; } ?>>
											<?php 
										
											if($_SESSION['user_role'] == 2 || $_SESSION['user_role'] == 3)
											{
												?>													
													<option value="0" selected="selected" >No</option>
												<?php
											}
											else
											{
											?>
                                               <option value="1" <?php if($row_art->approved == "1") print 'selected';?>>Yes</option>
                                                <option value="0" <?php if($row_art->approved == "0") print 'selected';?>>No</option>
										<?php } ?>
                                            </select>                                           
                                        </div>                                  
                                       <div class="box-footer">
											<button type="reset" onclick="goBack()" class="btn btn-default">Cancel</button>
											<button type="submit" class="btn btn-info pull-right" name="cmdSave">Submit</button>
									  </div>
                                    </form>
                                </div>
                                <!-- /.col-lg-6 (nested) -->
                                
                                <!-- /.col-lg-6 (nested) -->
                            </div>
                            <!-- /.row (nested) -->
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
        </div>
        <!-- /#page-wrapper -->
<?php 
include_once("includes/footer.php");
		}
		}	
		$oA = new Data;
		if(isset($_REQUEST['cmdSave']) or isset($_REQUEST['cmdUser']))
		{
			
		
			if($_REQUEST['cid']=='')
			{
				$oA->Save();				
			}
			else
			{
				
				$oA->Update();				
			}
		}
		else
		{		
			$oA->ShowDesign();
		}
?>