<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>..::ClubExec::..</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.5 -->
    <link rel="stylesheet" href="bootstrap/css/bootstrap.min.css">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
    <!-- Ionicons -->
    <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="dist/css/AdminLTE.min.css">
    <!-- AdminLTE Skins. Choose a skin from the css/skins
         folder instead of downloading all of them to reduce the load. -->
    <link rel="stylesheet" href="dist/css/skins/_all-skins.min.css">
	<link rel="stylesheet" type="text/css" href="css/on-off-switch.css"/>

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
	<link href="css/parsely.css" rel="stylesheet">
	<link href="css/custom.css" rel="stylesheet">
  	<link href="css/chosen.css" rel="stylesheet">
    <script src="ckeditor/ckeditor.js"></script>
    <script src="js/jquery.js"></script>
	<script src="js/parsley.js"></script>
	<script src="js/chosen.jquery.js" type="text/javascript"></script>
    <script src="js/custom.js"></script>
	<script>
	function goBack() {
		window.history.go(-1)
	}
	function isNumber(evt) {
		evt = (evt) ? evt : window.event;
		var charCode = (evt.which) ? evt.which : evt.keyCode;
		if (charCode > 31 && (charCode < 48 || charCode > 57)) 
		{
			alert("Please enter numeric values only.");
			return false;
		}
		return true;
	}
</script>

  </head>
  <body class="hold-transition skin-blue sidebar-mini">
    <div class="wrapper">

      <header class="main-header">
        <!-- Logo -->
        <a href="index.php" class="logo">
          <!-- mini logo for sidebar mini 50x50 pixels -->
          <span class="logo-mini"><b></b></span>
          <!-- logo for regular state and mobile devices -->
          <span class="logo-lg"><b>Club</b>Exec</span>
        </a>
        <!-- Header Navbar: style can be found in header.less -->
        <nav class="navbar navbar-static-top" role="navigation">
          <!-- Sidebar toggle button-->
          <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </a>
          <div class="navbar-custom-menu">
            <ul class="nav navbar-nav">
             
              <!-- User Account: style can be found in dropdown.less -->
              <li class="dropdown user user-menu">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                  <img src="images/no_avatar_female.png" class="user-image" alt="User Image">
                  <span class="hidden-xs"><?php if(isset($_SESSION['user_name']) && $_SESSION['user_name'] != ''){ echo $_SESSION['user_name']; } ?></span>
                </a>
                <ul class="dropdown-menu">
                  <!-- User image -->
                  <li class="user-header">
                    <img src="images/no_avatar_female.png" class="img-circle" alt="User Image">
                    <p>
                     <?php if(isset($_SESSION['user_name']) && $_SESSION['user_name'] != ''){ echo $_SESSION['user_name']; } ?>
                      <small>Member since 2017</small>
                    </p>
                  </li>
                  <!-- Menu Body -->
                  <li class="user-body">	
				   <div class="col-xs-3 text-center">
				   <small class="label pull-right bg-green">Roles</small>
                    
                    </div>	
					<?php 
				  if(isset($_SESSION['user_role']) && ($_SESSION['user_role'] == 1 || $_SESSION['user_role'] == 2))
				  {
				  ?>
                    <div class="col-xs-3 text-center">
                     Add
                    </div>
					
                    <div class="col-xs-3 text-center">
                      Edit
                    </div>
                   
					<?php } ?>
					 <div class="col-xs-3 text-center">
                      View
                    </div>		  
				  
				
					
                  </li>
                  <!-- Menu Footer-->
                  <li class="user-footer">
                    <div class="pull-left">
                      <a href="add_users.php?cid=<?php echo $_SESSION['user_id']; ?>" class="btn btn-default btn-flat">Profile</a>
                    </div>
                    <div class="pull-right">
                      <a href="logout.php" class="btn btn-default btn-flat">Sign out</a>
                    </div>
                  </li>
                </ul>
              </li>
              <!-- Control Sidebar Toggle Button -->
             <!-- <li>
                <a href="#" data-toggle="control-sidebar"><i class="fa fa-gears"></i></a>
              </li>-->
            </ul>
          </div>
        </nav>
      </header>
	  <?php 				
				$selectValuation2 = "SELECT * FROM tbl_contact_us where style_status = '0'";
				$sqlExecuteSelectValuation2 = mysql_query($selectValuation2) or die(mysql_error());
				$contactEnquries = mysql_num_rows($sqlExecuteSelectValuation2);
				
				$selectReportedErrors = "SELECT * FROM ce_report_errors where status = '0'";
				$sqlExecuteSelectReported = mysql_query($selectReportedErrors) or die(mysql_error());
				$reportEnquries = mysql_num_rows($sqlExecuteSelectReported);
		
				$selectReviews = "SELECT * FROM ce_reviews where review_approved = '0'";
				$sqlExecuteSelectReviews = mysql_query($selectReviews) or die(mysql_error());
				$reviewsCount = mysql_num_rows($sqlExecuteSelectReviews);
				
				$selectPhoto = "SELECT * FROM ce_photos where is_approved = '0'";
				$sqlExecutePhoto = mysql_query($selectPhoto) or die(mysql_error());
				$photoCount = mysql_num_rows($sqlExecutePhoto);
				$totalEnquries = $contactEnquries + $reportEnquries + $reviewsCount+$photoCount;
				$p =basename($_SERVER['SCRIPT_NAME']);				
							
		   ?>	
	  
	       <!-- Left side column. contains the logo and sidebar -->
      <aside class="main-sidebar">
        <!-- sidebar: style can be found in sidebar.less -->
        <section class="sidebar">
          <!-- Sidebar user panel -->
          <div class="user-panel">
            <div class="pull-left image">
              <img src="images/no_avatar_female.png" class="img-circle" alt="User Image">
            </div>
            <div class="pull-left info">
              <p><?php if(isset($_SESSION['user_name']) && $_SESSION['user_name'] != ''){ echo $_SESSION['user_name']; } ?></p>
              <a href="index.php"><i class="fa fa-circle text-success"></i> Online</a>
            </div>
          </div>
          <!-- search form -->
       <!--   <form action="#" method="get" class="sidebar-form">
            <div class="input-group">
              <input type="text" name="q" class="form-control" placeholder="Search...">
              <span class="input-group-btn">
                <button type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i></button>
              </span>
            </div>
          </form>-->
          <!-- /.search form -->
          <!-- sidebar menu: : style can be found in sidebar.less -->
          <ul class="sidebar-menu">
            <li class="header">MAIN NAVIGATION</li>
            <li class="treeview">
              <a href="index.php" <?php if($p=='index.php'){ echo 'class="activeClass"';}?>>
                <i class="fa fa-dashboard"></i> <span>Dashboard</span> <i class="fa fa-angle-left pull-right"></i>
              </a>              
            </li>
			<li class="treeview">
              <a href="view_category.php" <?php if($p=='view_category.php' || $p=='add_category.php'){ echo 'class="activeClass"';}?> >
                <i class="fa fa-arrows-alt"></i> <span>Categories</span> 
				 <?php
				if($_SESSION['user_role'] == 1){
					echo '<span class="label label-primary pull-right">'; 
					$selectSellingValuation = "SELECT * FROM add_category where approved = '0'";
					$sqlExecuteSelectSellingValuation = mysql_query($selectSellingValuation) or die(mysql_error());
					echo $numOfSellingRows = mysql_num_rows($sqlExecuteSelectSellingValuation);
					echo '</span>';
				}else
				{
					echo '<i class="fa fa-angle-left pull-right"></i>';
				}
				?>
				
              </a>
            </li>
			
            
            <li class="treeview">
              <a href="view_sub_category.php" <?php if($p=='view_sub_category.php' || $p=='add_sub_category.php'){ echo 'class="activeClass"';}?>>
                <i class="fa fa-arrows-alt"></i> <span>Sub Categories</span> 
				 <?php
				if($_SESSION['user_role'] == 1){
					echo '<span class="label label-primary pull-right">'; 
					$selectSellingValuation = "SELECT * FROM tbl_sub_category where approved = '0'";
					$sqlExecuteSelectSellingValuation = mysql_query($selectSellingValuation) or die(mysql_error());
					echo $numOfSellingRows = mysql_num_rows($sqlExecuteSelectSellingValuation);
					echo '</span>';
				}else
				{
					echo '<i class="fa fa-angle-left pull-right"></i>';
				}
				?>
				
              </a>
            </li>
            <li class="treeview">
              <a href="view_outlets.php" <?php if($p=='view_outlets.php' || $p=='add_outlet.php'){ echo 'class="activeClass"';}?>>
                <i class="fa fa-home"></i>
                <span>Outlets</span>
				<?php
				if($_SESSION['user_role'] == 1){
					echo '<span class="label label-primary pull-right">'; 
					$selectSellingValuation = "SELECT * FROM ce_outlet where approved = '0'";
					$sqlExecuteSelectSellingValuation = mysql_query($selectSellingValuation) or die(mysql_error());
					echo $numOfSellingRows = mysql_num_rows($sqlExecuteSelectSellingValuation);
					echo '</span>';
				}else
				{
					echo '<i class="fa fa-angle-left pull-right"></i>';
				}
				?>
               
              </a>
              
            </li>
            <li class="treeview">
              <a href="view_offers.php" <?php if($p=='view_offers.php' || $p=='add_offers.php'){ echo 'class="activeClass"';}?>>
                <i class="fa fa-hand-spock-o"></i>
                <span>Offers</span>
				<?php
				 if($_SESSION['user_role'] == 1){
					echo '<span class="label label-primary pull-right">'; 
					$selectSellingValuation = "SELECT * FROM ce_offers where approved = '0'";
					$sqlExecuteSelectSellingValuation = mysql_query($selectSellingValuation) or die(mysql_error());
					echo $numOfSellingRows = mysql_num_rows($sqlExecuteSelectSellingValuation);
					echo '</span>';
				}else
				{
					echo '<i class="fa fa-angle-left pull-right"></i>';
				} 
				?>
               
              </a>
              
            </li>
            <li class="treeview">
              <a href="view_applicants.php" <?php if($p=='view_applicants.php'){ echo 'class="activeClass"';}?>>
                <i class="fa fa-users"></i> <span>Applicants</span>
				<?php
				if($_SESSION['user_role'] == 1){
					echo '<span class="label label-primary pull-right">'; 				
					$selectSellingValuation = "SELECT * FROM applicants where approved = '0'";
					$sqlExecuteSelectSellingValuation = mysql_query($selectSellingValuation) or die(mysql_error());
					echo $numOfSellingRows = mysql_num_rows($sqlExecuteSelectSellingValuation);					
					echo '</span>';
				}
				else
				{
					echo '<i class="fa fa-angle-left pull-right"></i>';
				}
				?>
               
              </a>
              
            </li>
            <li class="treeview">
              <a href="view_videos.php" <?php if($p=='view_videos.php' || $p=='add_video.php'){ echo 'class="activeClass"';}?>>
               <i class="fa fa-file-video-o"></i> <span>Promotional Videos</span>
                <i class="fa fa-angle-left pull-right"></i>
              </a>
              
            </li>
          <li class="treeview">
              <a href="view_vouchers.php" <?php if($p=='view_vouchers.php' || $p=='add_voucher.php'){ echo 'class="activeClass"';}?>>
                <i class="fa fa-ticket"></i> <span>Vouchers</span>
                <i class="fa fa-angle-left pull-right"></i>
              </a>
            </li>
            <li class="treeview">
              <a href="view_airmiles.php" <?php if($p=='view_airmiles.php' ){ echo 'class="activeClass"';}?>>
                <i class="fa fa-asterisk"></i> <span>Earned Airmiles</span>
              	<i class="fa fa-angle-left pull-right"></i>
              </a>
            </li>
             <li class="treeview">
              <a href="view_redeamed_vouchers.php" <?php if($p=='view_redeamed_vouchers.php' ){ echo 'class="activeClass"';}?>>
                <i class="fa fa-asterisk"></i> <span>Redeemed Vouchers</span>
              	<i class="fa fa-angle-left pull-right"></i>
              </a>
            </li>
            <li class="treeview">
              <a href="view_tags.php" <?php if($p=='view_tags.php' || $p=='add_tag.php'){ echo 'class="activeClass"';}?>>
                <i class="fa fa-tags"></i><span>Tags</span>
                <i class="fa fa-angle-left pull-right"></i>
              </a>
              
            </li>
			<li class="treeview">
              <a href="view_news.php" <?php if($p=='view_news.php' || $p=='add_news.php'){ echo 'class="activeClass"';}?>>
                <i class="fa fa-bullhorn" aria-hidden="true"></i> <span>News</span>
                <i class="fa fa-angle-left pull-right"></i>
              </a>
              
            </li>
            <li class="treeview">
              <a href="view_pages.php" <?php if($p=='view_pages.php' || $p=='add_pages.php'){ echo 'class="activeClass"';}?>>
                <i class="fa fa-pencil-square-o"></i><span>Content</span>
                <i class="fa fa-angle-left pull-right"></i>				
              </a>
              
            </li>
			
			<li class="treeview">
              <a href="view_faq.php" <?php if($p=='view_faq.php' || $p=='add_faq.php'){ echo 'class="activeClass"';}?>>
                <i class="fa fa-pencil-square-o"></i><span>FAQ</span>
                <i class="fa fa-angle-left pull-right"></i>				
              </a>
              
            </li>
			
			
			
				<?php if($_SESSION['user_role']==1){?>
				<li class="treeview">
				<a href="view_users.php" <?php if($p=='view_users.php' || $p=='add_users.php'){ echo 'class="activeClass"';}?>>
				<i class="fa fa-user" aria-hidden="true"></i>
				<span>Administrator</span>
				<i class="fa fa-angle-left pull-right"></i></a>
			</li>
						<?php } ?>
                      
             <li class="treeview">
              <a href="view_app_configurations.php" <?php if($p=='view_app_configurations.php' || $p == 'add_app_configurations.php'){ echo 'class="activeClass"';}?>>
                <i class="fa fa-mobile" aria-hidden="true"></i> <span>Mobile App</span> 
				<i class="fa fa-angle-left pull-right"></i>
              </a>
            </li>
			
			
			
			<li class="treeview">
              <a href="#">
                <i class="fa fa-folder"></i> <span>Enquiries <?php if($totalEnquries > 0){ echo ' ('.$totalEnquries.')';} ?></span>
                <i class="fa fa-angle-left pull-right"></i>
              </a>
              <ul class="treeview-menu">
                
								<li>
                                    <a href="view_reported_errors.php" <?php if($p=='view_reported_errors.php'){ echo 'class="activeClass"';}?>>Reported Errors<?php if($reportEnquries > 0){ echo ' ('.$reportEnquries.')';} ?></a>
                                </li>
								<li>
                                    <a href="view_review_approval.php" <?php if($p=='view_review_approval.php'){ echo 'class="activeClass"';}?>>Review Approvals<?php if($reviewsCount > 0){ echo ' ('.$reviewsCount.')';} ?></a>
                                </li>
								<li>
                                    <a href="photo_approvals.php" <?php if($p=='photo_approvals.php'){ echo 'class="activeClass"';}?>>Photo Approvals<?php if($photoCount > 0){ echo ' ('.$photoCount.')';} ?></a>
                                </li>
              </ul>
            </li>
			
            
          </ul>
        </section>
        <!-- /.sidebar -->
      </aside>