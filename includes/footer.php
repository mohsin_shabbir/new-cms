  <footer class="main-footer">
        <div class="pull-right hidden-xs">
          <b>Version</b> 1.0.0
        </div>
        <strong>Copyright &copy; 2017-2018 <a href="https://www.clubexec.net/web/index.php">ClubExec</a>.</strong> All rights reserved.
      </footer>

      <!-- 
        <ul class="nav nav-tabs nav-justified control-sidebar-tabs">
          <li><a href="#control-sidebar-home-tab" data-toggle="tab"><i class="fa fa-home"></i></a></li>
          <li><a href="#control-sidebar-settings-tab" data-toggle="tab"><i class="fa fa-gears"></i></a></li>
        </ul>
  
        <div class="tab-content">
        
          <div class="tab-pane" id="control-sidebar-home-tab">
            <h3 class="control-sidebar-heading">Coming Soon</h3>
          	Suggestion Required

          </div>
        
          <div class="tab-pane" id="control-sidebar-stats-tab">Stats Tab Content</div>
          
          <div class="tab-pane" id="control-sidebar-settings-tab">
           <h3 class="control-sidebar-heading">Coming Soon</h3>
          	Your Suggestions are valuable.
          </div>
        </div>
      </aside>--><!-- /.control-sidebar -->
      <!-- Add the sidebar's background. This div must be placed
           immediately after the control sidebar -->
      <div class="control-sidebar-bg"></div>
    </div><!-- ./wrapper -->
	<script type="text/javascript">
    var config = {
      '.chosen-select'           : {},
      '.chosen-select-deselect'  : {allow_single_deselect:true},
      '.chosen-select-no-single' : {disable_search_threshold:10},
      '.chosen-select-no-results': {no_results_text:'Oops, nothing found!'},
      '.chosen-select-width'     : {width:"95%"}
    }
    for (var selector in config) {
      $(selector).chosen(config[selector]);
    }
  </script>

    <!-- jQuery 2.1.4 -->
   <!-- <script src="plugins/jQuery/jQuery-2.1.4.min.js"></script>-->
    <!-- Bootstrap 3.3.5 -->
    <script src="bootstrap/js/bootstrap.min.js"></script>
    <!-- FastClick -->
    <script src="plugins/fastclick/fastclick.min.js"></script>
    <!-- AdminLTE App -->
    <script src="dist/js/app.min.js"></script>
    <!-- AdminLTE for demo purposes -->
    <script src="dist/js/demo.js"></script>
		<script src="plugins/datatables/jquery.dataTables.min.js"></script>
    <script src="plugins/datatables/dataTables.bootstrap.min.js"></script>
	
	<script type="text/javascript" src="js/on-off-switch.js"></script>
    <script type="text/javascript" src="js/on-off-switch-onload.js"></script>
<?php
 if($_SESSION['user_role']==3)
 {
 	?>
	<script>
	$(function () {
		$(".checkbox-container").remove();
	});
	</script>
	<?php
 } 
 else
 {
 	?>
	<script type="text/javascript">
    new DG.OnOffSwitchAuto({
        cls:'.custom-switch',
        textOn:"Approved",
        height:32,
		width: 100,
        textOff:"Rejected",
        textSizeRatio:0.35,
        listener:function(del_id, checked){
			var tbl_name = $('#'+del_id).val();
			var fieldName = $('#'+del_id).attr("title");
			var info = 'id='+del_id+'&approveReject=mode&tbl='+ tbl_name+'&primaryKey='+fieldName;							
			$.ajax({
			type: "POST",
			url: "getAddress.php",
			data: info,
			success: function(){
			if(checked)
			{
				$('#'+del_id).parent().parent().parent().removeClass("approved");				
			}
			else
			{
				$('#'+del_id).parent().parent().parent().addClass("approved");
			}
						
			}
			});
			
			return false;
        }
    });
</script>
	<?php
 }
?>
	<script>
      $(function () {
        $("#example1").DataTable();
        $('#example2').DataTable({
          "paging": true,
          "lengthChange": false,
          "searching": false,
          "ordering": true,
          "info": true,
          "autoWidth": false,
		  "scrollX": true
        });
		//$(":input").each(function (i) { $(this).attr('tabindex', i + 1); });
      });
    </script>

	
	
	
	<div class="showPopup" id="showPopup">
		<div style="float:right; margin-right: -32px; margin-top: -10px;"><a href="javascript:" onclick="return closePopup(1)">Close X</a></div>
		
		<br />
		<div class="response" id="response">
		text will goes here.
		</div>
	</div>
	<div class="showImagePopup" id="showPopup">
                            	<div style="float:right; margin-right: -32px; margin-top: -10px;"><a href="javascript:" onclick="return closePopup(2)">Close X</a></div>
                            	
                                <br />
                            	<div class="response" id="responseImage">
                                text will goes here.
                                </div>
  </div>
  </body>
</html>