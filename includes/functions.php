<?php
class Function_Library {
	function GetId($tbname, $field) {
		$rs = @mysql_query ( "select max(" . $field . ") from " . $tbname );
		if (@mysql_result ( $rs, 0, 0 ) == '')
			$id = 1;
			else
				$id = @mysql_result ( $rs, 0, 0 ) + 1;
				return $id;
	}
	function GetReference($tbname, $find, $criteria, $criteria_val) {
		$rs = @mysql_query ( "select " . $find . " from " . $tbname . " where " . $criteria . " = '" . $criteria_val . "'" );
		$row = @mysql_fetch_array ( $rs );
		return $row [$find];
	}
	function GetTime($tm) {
		$rs = @mysql_query ( "select time_format('" . $tm . "','%r') as time" );
		$row = @mysql_fetch_array ( $rs );
		return $row ['time'];
	}
	function date_format($dt, $format) {
		if (trim ( $dt ) != '') {
			switch ($format) {
				case "m-d-Y" :
					return substr ( $dt, 5, 2 ) . "-" . substr ( $dt, 8, 2 ) . "-" . substr ( $dt, 0, 4 );
					break;
				case "d-m-Y" :
					return substr ( $dt, 8, 2 ) . "-" . substr ( $dt, 5, 2 ) . "-" . substr ( $dt, 0, 4 );
					break;
				case "Y-m-d" :
					return substr ( $dt, 6, 4 ) . "-" . substr ( $dt, 0, 2 ) . "-" . substr ( $dt, 3, 2 );
					break;
			}
		}
	}
	function ConvertBusId($bus_id) {
		switch ($bus_id) {
			case "1" :
				return "A";
				break;
			case "2" :
				return "B";
				break;
			case "3" :
				return "C";
				break;
			case "4" :
				return "D";
				break;
			case "5" :
				return "E";
				break;
			case "6" :
				return "F";
				break;
			case "7" :
				return "G";
				break;
			case "8" :
				return "H";
				break;
			case "9" :
				return "I";
				break;
			case "10" :
				return "J";
				break;
			case "11" :
				return "K";
				break;
			case "12" :
				return "L";
				break;
			case "13" :
				return "M";
				break;
		}
	}
	function GetMonth($m_no) {
		switch ($m_no) {
			case "01" :
				return "January";
				break;
			case "02" :
				return "February";
				break;
			case "03" :
				return "March";
				break;
			case "04" :
				return "April";
				break;
			case "05" :
				return "May";
				break;
			case "06" :
				return "Jun";
				break;
			case "07" :
				return "July";
				break;
			case "08" :
				return "August";
				break;
			case "09" :
				return "September";
				break;
			case "10" :
				return "October";
				break;
			case "11" :
				return "November";
				break;
			case "12" :
				return "December";
				break;
		}
	}
	function GetDayName($dt) {
		switch ($this->DayNum ( $dt )) {
			case 1 :
				return 'Sun';
				break;
			case 2 :
				return 'Mon';
				break;
			case 3 :
				return 'Tue';
				break;
			case 4 :
				return 'Wed';
				break;
			case 5 :
				return 'Thu';
				break;
			case 6 :
				return 'Fri';
				break;
			case 7 :
				return 'Sat';
				break;
		}
	}
	function GetNextDate($dt) {
		$rs = @mysql_query ( "select date_add('" . $dt . "', INTERVAL 1 DAY)" );
		return @mysql_result ( $rs, 0, 0 );
	}
	function DayNum($dt) {
		$rs = @mysql_query ( "select DAYOFWEEK('" . $dt . "')" );
		return @mysql_result ( $rs, 0, 0 );
	}
	function GetTableData($tb_name, $order) {
		$rs = @mysql_query ( "select * from " . $tb_name . " order by " . $order );
		return $rs;
	}
	function GetTableFieldsData($tb_name, $fields, $order) {
		$rs = @mysql_query ( "select " . $fields . " from " . $tb_name . " order by " . $order ) or die ( @mysql_error () );
		return $rs;
	}
	function CreateDbDropDown($name, $heading, $tb_name, $field1, $field2, $default_val, $attributes, $javascript, $multi = false) {
		$result = $this->GetTableFieldsData ( $tb_name, $field1 . ", " . $field2, $field2 );
		?>
<select name="<?php print $name; ?>" <?php print $attributes ?>
	<?php print $javascript; ?>>
	<option style="background-color: #CCCCCC; color: #000000"><?php print "== SELECT ".$heading."=="; ?></option>
 		 		<?php
		$cnt = 0;
		$def_cnt = 0;
		while ( $row = @mysql_fetch_array ( $result ) ) {
			if ($cnt % 2 == 0)
				$bgcol = "#FFFFFF";
			else
				$bgcol = "#EEEEEE";
			
			if ($multi == false) {
				?>
	 		 		 		 <option style="background-color:<?php print $bgcol; ?>" value="<?php print $row[$field1]; ?>" <?php if($row[$field1] == $default_val) { print 'selected'; } ?>><?php print $row[$field2]; ?></option>
	 		 		 <?php
			} else {
				?>		 		 		 		 
	 		 		 		 <option style="background-color:<?php print $bgcol; ?>" value="<?php print $row[$field1]; ?>" <?php if($row[$field1] == $default_val[$def_cnt]) { print 'selected'; } ?>><?php print $row[$field2]; ?></option>
	 		 		 <?php
				if ($row [$field1] == $default_val [$def_cnt])
					$def_cnt ++;
			}
			$cnt ++;
		}
		?>
		 </select>
<?php
	}
	function CreateDropDown($name, $heading, &$sql, $field1, $field2, $default_val, $attributes, $javascript, $multi = false, $default = false, $no_val_display = true) {
		$display = 'yes';
		$result = @mysql_query ( $sql );
		if (@mysql_num_rows ( $result ) == 0 && $no_val_display == true) {
			$display = 'yes';
		} elseif (@mysql_num_rows ( $result ) == 0 && $no_val_display == false) {
			$display = 'no';
			print "<font color='#cc0000'>Not Available</font>";
		}
		if ($display == 'yes') {
			?>
<select name="<?php print $name; ?>" <?php print $attributes ?>
	<?php print $javascript; ?>>
	 			 <?php if($default == true) { ?>
				 <option style="background-color: #CCCCCC; color: #000000" value=""><?php print "== Select ".$heading."=="; ?></option>
	 		 		<?php
			}
			$cnt = 0;
			$def_cnt = 0;
			while ( $row = @mysql_fetch_array ( $result ) ) {
				if ($cnt % 2 == 0)
					$bgcol = "#FFFFFF";
				else
					$bgcol = "#EEEEEE";
				
				if ($multi == false) {
					?>
 		 		 		 	<option style="background-color:<?php print $bgcol; ?>" value="<?php print $row[$field1]; ?>" <?php if($row[$field1] == $default_val) { print 'selected'; } ?>><?php print $row[$field2]; ?></option>
 		 		 		 <?php
				} else {
					?>
 		 		 		 	<option style="background-color:<?php print $bgcol; ?>" value="<?php print $row[$field1]; ?>" <?php if($row[$field1] == $default_val[$def_cnt]) { print 'selected'; } ?>><?php print $row[$field2]; ?></option>
 		 		 		 <?php
					if ($row [$field1] == $default_val [$def_cnt]) {
						$def_cnt ++;
					}
				}
				$cnt ++;
			}
			?>
		 	</select>
<?php
		}
	}
	function Display_Data_List(&$sql, $hyper_link, $param, $hidden) {
		$rs = @mysql_query ( $sql ) or die ( @mysql_error () );
		$cnt = 0;
		while ( $row = @mysql_fetch_array ( $rs ) ) {
			if ($cnt % 2 == 0)
				$bgcol = "#FFFFFF";
			else
				$bgcol = "#EEEEEE";
			?>
<tr bgcolor="<?php print $bgcol; ?>">
		 		 		 <?php
			for($c = 0; $c < @mysql_numfields ( $rs ); $c ++) {
				if ($c != $hidden) {
					if ($c == $hyper_link)
						print "<td><a href='#' class='browse_list_heading_a_large'>" . $row [$c] . "</a></td>";
					else
						print "<td>" . $row [$c] . "</td>";
				}
			}
			?>
 		 		 </tr>
<?php
			$cnt ++;
		}
	}
	function HaveParent($parent_id, &$tree_arr, &$arr_cnt, &$ret_cat_id) {
		$rsSub = @mysql_query ( "select cat_id, cat_title, parent_id from cat_main where cat_id='" . $parent_id . "' order by cat_id" );
		$rowSub = @mysql_fetch_array ( $rsSub );
		$tree_arr [$arr_cnt] = $rowSub ['cat_id'];
		$arr_cnt ++;
		
		if ($rowSub ['parent_id'] == 0 && @mysql_num_rows ( $rsSub ) > 0)
			$ret_cat_id = $rowSub ['cat_id'];
		
		if ($parent_id != 0)
			$this->HaveParent ( $rowSub ['parent_id'], $tree_arr, $arr_cnt, $ret_cat_id );
	}
	function ShowCatTree($link, $design) {
		if (! isset ( $_GET ['pid'] )) {
			$tree = "<a href='javascript:void(0)'>Home</a>";
		} else {
			$cat_id = split ( "_", $_GET ['pid'] );
			
			if ($link == true) {
				if ($design == 'front')
					$tree = "<a href='http://" . $_SERVER ['HTTP_HOST'] . "/index.html'>Home</a>";
				elseif ($design == 'forum_admin')
					$tree = "<a href='admin_forums.php?" . $_SESSION ['qstring'] . "'>Home</a>";
				elseif ($design == 'forum_front')
					$tree = "Home";
				else
					$tree = "<a href='category_manager_index.php'>Home</a>";
			} else {
				$tree = "";
			}
			
			$arr_cnt = 0;
			$tree_arr [$arr_cnt] = '0';
			
			$dummy = '';
			$this->HaveParent ( $cat_id [0], $tree_arr, $arr_cnt, $dummy );
			
			for($i = count ( $tree_arr ) - 1; $i >= 0; $i --) {
				if ($i > 0) {
					if ($link == true) {
						if ($design == 'front')
							$tree .= "<a href='/categories/p_" . $tree_arr [$i] . "/" . $this->GetCatPageName ( $this->GetReference ( "cat_main", "cat_title", "cat_id", $tree_arr [$i] ) ) . ".html'><b>" . $this->GetReference ( "cat_main", "cat_title", "cat_id", $tree_arr [$i] ) . "</b></a>&nbsp;<img src='" . PATH . "/images/arrow.gif'>&nbsp;";
						elseif ($design == 'forum_admin')
							$tree .= "<a href='admin_forums.php?" . $_SESSION ['qstring'] . "&pid=" . $tree_arr [$i] . "'><b>" . $this->GetReference ( "cat_main", "cat_title", "cat_id", $tree_arr [$i] ) . "</b></a>&nbsp;<img src='" . PATH . "/images/arrow.gif'>&nbsp;";
						elseif ($design == 'forum_front')
							$tree .= "<b>" . $this->GetReference ( "cat_main", "cat_title", "cat_id", $tree_arr [$i] ) . "</b>&nbsp;<img src='" . PATH . "/images/arrow.gif'>&nbsp;";
						else
							$tree .= "<a href='category_manager_index.php?pid=" . $tree_arr [$i] . "'>" . $this->GetReference ( "cat_main", "cat_title", "cat_id", $tree_arr [$i] ) . "</a>&nbsp;<img src='" . PATH . "/images/arrow.gif'>&nbsp;";
					} else {
						if (trim ( $tree_arr [$i] ) == '')
							$tree .= "";
						else
							$tree .= $this->GetReference ( "cat_main", "cat_title", "cat_id", $tree_arr [$i] ) . " -- ";
					}
				} else {
					if ($design == 'admin')
						$tree .= $this->GetReference ( "cat_main", "cat_title", "cat_id", $tree_arr [$i] );
					elseif ($design == 'forum_admin')
						$tree .= "<b>" . $this->GetReference ( "cat_main", "cat_title", "cat_id", $tree_arr [$i] ) . "</b>";
					elseif ($design == 'forum_front')
						$tree .= "<a href='" . $_SESSION ['caturl_to_buy'] . "'><b>" . $this->GetReference ( "cat_main", "cat_title", "cat_id", $tree_arr [$i] ) . "</b></a>";
					else
						$tree .= "<a href='/categories/p_" . $tree_arr [$i] . "/" . $this->GetCatPageName ( $this->GetReference ( $db, "cat_main", "cat_title", "cat_id", $tree_arr [$i] ) ) . ".html'><b>" . $this->GetReference ( "cat_main", "cat_title", "cat_id", $tree_arr [$i] ) . "</b></a>";
				}
			}
		}
		return $tree;
	}
	function GetCatPageName($cat_title) {
		$cat_page_arr = split ( " ", $cat_title );
		$cat_page_name = "";
		for($i = 0; $i < count ( $cat_page_arr ); $i ++) {
			if ($cat_page_arr [$i] != "&" && $cat_page_arr [$i] != "'" && $cat_page_arr [$i] != "/") {
				if ($i < (count ( $cat_page_arr ) - 1))
					$cat_page_name .= $cat_page_arr [$i] . "_";
				else
					$cat_page_name .= $cat_page_arr [$i];
			}
		}
		
		return str_replace ( "'", "", str_replace ( "+", "_", str_replace ( "=", "_", str_replace ( ">", "_", str_replace ( ",", "_", str_replace ( ")", "_", str_replace ( "(", "_", str_replace ( ".", "", str_replace ( "-", "_", str_replace ( "&", "_", str_replace ( "/", "_", $cat_page_name ) ) ) ) ) ) ) ) ) ) );
	}
	function HaveSubCategory($cat_id, $str, &$default_value, &$value) {
		$str2 = "";
		$rsSub = @mysql_query ( "select * from cat_main where parent_id='" . $cat_id . "' order by cat_id" );
		
		while ( $rowSub = @mysql_fetch_array ( $rsSub ) ) {
			$rsCheck = @mysql_query ( "select count(*) as sub_cat_cnt from cat_main where parent_id='" . $rowSub ['cat_id'] . "'" );
			$rowCheck = @mysql_fetch_array ( $rsCheck );
			if ($rowCheck ['sub_cat_cnt'] >= 1) {
				?>
<option
	style="background-color: #E9E9E9; font-weight: bold; color: #cc0000;"
	value="<?php if($value == '0') { print $value; } else { print $rowSub['cat_id']; } ?>"
	<?php if(trim($rowSub['cat_id']) == trim($default_value)) { print 'selected'; } ?>><?php print $str."&nbsp;&nbsp;&nbsp;&nbsp;".$rowSub['cat_title']; ?></option>
<?php
				$this->HaveSubCategory ( $rowSub ['cat_id'], "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;", $default_value, $value );
			} else {
				?>
<option style="background-color: #FFFFFF; color: #006600"
	value="<?php print $rowSub['cat_id'] ?>"
	<?php if(trim($rowSub['cat_id']) == trim($default_value)) { print 'selected'; } ?>><?php print $str."&nbsp;&nbsp;&nbsp;&nbsp;".$rowSub['cat_title']; ?></option>
<?php
			}
			
			$str2 = "";
		}
	}
	function MakeCatDropDown($sql, &$default_value, $javascript, $value) {
		$str = "";
		$rsMain = @mysql_query ( $sql ) or die ( @mysql_error () );
		
		?>
<select name="cat_id"
	style="font-size: 9px; font-family: verdana; text-transform: uppercase;"
	<?php print $javascript; ?>>
	<option style="background-color: #CCCCCC; color: #000000;">== SELECT ==</option>
	 		 <?php
		
		while ( $rowMain = @mysql_fetch_array ( $rsMain ) ) {
			$rsCheck = @mysql_query ( "select count(*) as sub_cat_cnt from cat_main where parent_id='" . $rowMain ['cat_id'] . "' order by cat_title" );
			$rowCheck = @mysql_fetch_array ( $rsCheck );
			
			if ($rowCheck ['sub_cat_cnt'] >= 1) {
				$str = $rowMain ['cat_title'];
				?>
	 		 		 		 <option
		style="background-color: #E9E9E9; font-weight: bold; color: #cc0000;"
		value="<?php if($value == '0') { print $value; } else { print $rowMain['cat_id']; } ?>"
		<?php if($rowMain['cat_id'] == $default_value) { print 'selected'; } ?>><?php print $str; ?></option>
	 		 		 <?php
				$this->HaveSubCategory ( $rowMain ['cat_id'], "", $default_value, $value );
			} else {
				$str = $rowMain ['cat_title'];
				
				?>		 
	 		 		 		 <option
		style="background-color: #E9E9E9; font-weight: bold; color: #cc0000;"
		value="<?php if($value == '0') { print $value; } else { print $rowMain['cat_id']; } ?>"
		<?php if($rowMain['cat_id'] == $default_value) { print 'selected'; } ?>><?php print $str; ?></option>
	 		 		 <?php
			}
			$str = "";
		}
		?>
	 		 </select>
<?php
	}
	function HaveSubCat($cat_id, &$cat_arr, &$arr_cnt) {
		$rsSub = @mysql_query ( "select * from cat_main where parent_id='" . $cat_id . "' order by cat_id" );
		
		while ( $rowSub = @mysql_fetch_array ( $rsSub ) ) {
			$rsCheck = @mysql_query ( "select count(*) as sub_cat_cnt from cat_main where parent_id='" . $rowSub ['cat_id'] . "'" );
			$rowCheck = @mysql_fetch_array ( $rsCheck );
			if ($rowCheck ['sub_cat_cnt'] >= 1) {
				$this->HaveSubCat ( $rowSub ['cat_id'], $cat_arr, $arr_cnt );
			} else {
				$cat_arr [$arr_cnt] = $rowSub ['cat_id'];
				$arr_cnt ++;
			}
		}
	}
	function GetCatArray($cat_id) {
		$sql = "select cat_id from cat_main where parent_id='" . $cat_id . "' order by cat_id";
		$rsMain = @mysql_query ( $sql ) or die ( @mysql_error () );
		
		if (@mysql_num_rows ( $rsMain ) == 0)
			$cat_arr [0] = $cat_id;
		else
			
			$cat_arr [0] = 0;
		$arr_cnt = 0;
		
		while ( $rowMain = @mysql_fetch_array ( $rsMain ) ) {
			$rsCheck = @mysql_query ( "select count(*) as sub_cat_cnt from cat_main where parent_id='" . $rowMain ['cat_id'] . "' order by cat_title" );
			$rowCheck = @mysql_fetch_array ( $rsCheck );
			
			if ($rowCheck ['sub_cat_cnt'] >= 1) {
				$this->HaveSubCat ( $rowMain ['cat_id'], $cat_arr, $arr_cnt );
			} else {
				$cat_arr [$arr_cnt] = $rowMain ['cat_id'];
				$arr_cnt ++;
			}
		}
		
		$cat_str = '';
		for($i = 0; $i < count ( $cat_arr ); $i ++) {
			if ($i < count ( $cat_arr ) - 1)
				$cat_str .= $cat_arr [$i] . ",";
			else
				$cat_str .= $cat_arr [$i];
		}
		
		return $cat_str;
	}
	function SaveVal($val) {
		return addslashes ( $val );
	}
	function insertUpdateRecord($data = array(), $table_name = "", $primaryKeyValue = "", $primaryKeyName = "", $unique_key_name = "", $condition = "") {
		$column = '';
		$columnValue = '';
		$updateQuery = '';
		$first_key = key ( $data );
		$first_value = $primaryKeyValue;
		// unset($data[$first_key]);
		$dataCount = (count ( $data ) - 1);
		$count = 0;
		foreach ( $data as $key => $value ) {
			if ($key != "cmdSave") {
				$count ++;
				if ($dataCount == $count) {
					$column .= $key;
					$columnValue .= "'" .$this->SaveVal( $value) . "'";
					$updateQuery .= "" . $key . " = '" . $this->SaveVal($value) . "'";
				} else {
					$column .= $key . ' ,';
					$columnValue .= "'" . $this->SaveVal($value) . "' , ";
					$updateQuery .= "" . $key . " = '" . $this->SaveVal($value) . "' , ";
				}
			}
		}
		if (isset ( $first_value ) && $first_value > 0) {
					
			mysql_query ( "UPDATE " . $table_name . " SET  " . $updateQuery . " WHERE " . $primaryKeyName . " = '" . $primaryKeyValue . "'" );
			$updated_at = round ( microtime ( true ) * 1000 );
			$query = "";
			if ($table_name == 'ce_outlet') {
				$query = "UPDATE ce_offers SET updated_at = '" . $updated_at . "' WHERE FIND_IN_SET(" . $primaryKeyValue . " , out_ids)";
			}
			return mysql_query ( $query );
		} else {
				return mysql_query ( "INSERT INTO " . $table_name . " (" . $column . ") VALUES(" . $columnValue . ")" );
			
		}
		exit;
	}
	
	
	function sendPushNotification($title , $description) {		
		$newsDescription = mb_convert_encoding ( $description, "UTF-8", "HTML-ENTITIES" );
		$isSend;
		$result = mysql_query ( "SELECT token_id AS id,device_id AS token FROM `device_tokens` ORDER BY token_id DESC" );
		$body = array ();
		$body ['aps'] ['content-available'] = 1;
		$body ['aps'] ['alert'] = array (
				'title' => $title,
				'body' => $newsDescription
		);
		$body ['aps'] ['badge'] = 1;
		$body ['aps'] ['sound'] = 'default';
		$body ['aps'] ['news'] = $newsDescription;
echo '<pre>';
print_r($body);

		$ctx = stream_context_create ();
		stream_context_set_option ( $ctx, 'ssl', 'passphrase', '' );
		stream_context_set_option ( $ctx, 'ssl', 'local_cert', 'dist.pem' ); // Prod
		//stream_context_set_option($ctx, 'ssl', 'local_cert', 'dev.pem'); // Sandbox
		$fp = stream_socket_client ( 'ssl://gateway.push.apple.com:2195', $err, $errstr, 60, STREAM_CLIENT_CONNECT, $ctx ); // Prod
		//$fp = stream_socket_client('ssl://gateway.sandbox.push.apple.com:2195', $err, $errstr, 60, STREAM_CLIENT_CONNECT, $ctx); //SandBox
		// 'ssl://gateway.sandbox.push.apple.com:2195'
		stream_set_blocking ( $fp, 0 );
		if (! $fp) {
			echo "Failed to connect (stream_socket_client): $err $errstrn";
		} else {
	
			$apple_expiry = time () + (90 * 24 * 60 * 60);
			while ( $row = mysql_fetch_array ( $result ) ) {
				$apple_identifier = $row ["id"];
				echo $apple_identifier;
				$deviceToken = $row ["token"];
echo '<br>'.$deviceToken ;
				$payload = json_encode ( $body );
				$msg = pack ( "C", 1 ) . pack ( "N", $apple_identifier ) . pack ( "N", $apple_expiry ) . pack ( "n", 32 ) . pack ( 'H*', str_replace ( ' ', '', $deviceToken ) ) . pack ( "n", strlen ( $payload ) ) . $payload;
//echo $msg ;
				fwrite ( $fp, $msg );
			 $this->checkAppleErrorResponse($fp);
			}
	
			// usleep(500000);
			fclose ( $fp );
		}
	}
	function checkAppleErrorResponse($fp) {
		$apple_error_response = fread ( $fp, 6 );
	
		if ($apple_error_response) {
			$error_response = unpack ( 'Ccommand/Cstatus_code/Nidentifier', $apple_error_response );
	
			if ($error_response ['status_code'] == '0') {
				$error_response ['status_code'] = '0-No errors encountered';
			} else if ($error_response ['status_code'] == '1') {
				$error_response ['status_code'] = '1-Processing error';
			} else if ($error_response ['status_code'] == '2') {
				$error_response ['status_code'] = '2-Missing device token';
			} else if ($error_response ['status_code'] == '3') {
				$error_response ['status_code'] = '3-Missing topic';
			} else if ($error_response ['status_code'] == '4') {
				$error_response ['status_code'] = '4-Missing payload';
			} else if ($error_response ['status_code'] == '5') {
				$error_response ['status_code'] = '5-Invalid token size';
			} else if ($error_response ['status_code'] == '6') {
				$error_response ['status_code'] = '6-Invalid topic size';
			} else if ($error_response ['status_code'] == '7') {
				$error_response ['status_code'] = '7-Invalid payload size';
			} else if ($error_response ['status_code'] == '8') {
				$error_response ['status_code'] = '8-Invalid token';
			} else if ($error_response ['status_code'] == '255') {
				$error_response ['status_code'] = '255-None (unknown)';
			} else {
				$error_response ['status_code'] = $error_response ['status_code'] . '-Not listed';
			}
			 return true;
		}
	
		 return false;
	}
	
	
	
	
	function Save($tbname) {
		$rs = @mysql_query ( "select * from " . $tbname );
		$sql = 'insert into ' . $tbname . ' set ';
		
		for($c = 0; $c < @mysql_numfields ( $rs ); $c ++) {
			if ($c < @mysql_num_fields ( $rs ) - 1) {
				
				if (isset ( $_REQUEST [@mysql_field_name ( $rs, $c )] )) {
					$sql .= @mysql_field_name ( $rs, $c ) . ' = "' . $this->SaveVal ( $_REQUEST [@mysql_field_name ( $rs, $c )] ) . '",';
				}
			} else {
				
				if (isset ( $_REQUEST [@mysql_field_name ( $rs, $c )] )) {
					$sql .= @mysql_field_name ( $rs, $c ) . ' = "' . $this->SaveVal ( $_REQUEST [@mysql_field_name ( $rs, $c )] ) . '"';
				} else {
					$sql = substr ( $sql, 0, strlen ( $sql ) - 1 );
				}
			}
		}
		// print_r( $sql);
		
		if (! @mysql_query ( $sql ))
			return false;
		else
			return true;
	}
	function Update($tbname) {
		$rs = @mysql_query ( "select * from " . $tbname );
		$sql = "update " . $tbname . " set ";
		
		for($c = 0; $c < @mysql_numfields ( $rs ); $c ++) {
			if ($c < @mysql_num_fields ( $rs ) - 1) {
				if (isset ( $_REQUEST [@mysql_field_name ( $rs, $c )] ))
					$sql .= @mysql_field_name ( $rs, $c ) . ' = "' . $this->SaveVal ( $_REQUEST [@mysql_field_name ( $rs, $c )] ) . '",';
			} else {
				if (isset ( $_REQUEST [@mysql_field_name ( $rs, $c )] )) {
					$sql .= @mysql_field_name ( $rs, $c ) . ' = "' . $this->SaveVal ( $_REQUEST [@mysql_field_name ( $rs, $c )] ) . '"';
				} else {
					// $sql .= @mysql_field_name($rs,$c)." = ''";
					$sql = substr ( $sql, 0, strlen ( $sql ) - 1 );
				}
			}
		}
		
		$sql .= ' where ' . @mysql_field_name ( $rs, 0 ) . ' ="' . $_REQUEST [@mysql_field_name ( $rs, 0 )] . '"';
		if (! @mysql_query ( $sql ))
			return false;
		else
			return true;
	}
	function Save5($tbname) {
		$rs = @mysql_query ( "select * from " . $tbname );
		$sql = 'insert into ' . $tbname . ' set ';
		
		for($c = 0; $c < @mysql_numfields ( $rs ); $c ++) {
			if ($c < @mysql_num_fields ( $rs ) - 1) {
				if (isset ( $_POST [@mysql_field_name ( $rs, $c )] )) {
					$sql .= @mysql_field_name ( $rs, $c ) . ' = "' . mysql_real_escape_string ( $this->SaveVal ( $_POST [@mysql_field_name ( $rs, $c )] ) ) . '",';
				}
			} else {
				if (isset ( $_POST [@mysql_field_name ( $rs, $c )] )) {
					$sql .= @mysql_field_name ( $rs, $c ) . ' = "' . mysql_real_escape_string ( $this->SaveVal ( $_POST [@mysql_field_name ( $rs, $c )] ) ) . '"';
				} else {
					$sql = substr ( $sql, 0, strlen ( $sql ) - 1 );
				}
			}
		}
		// print $sql;
		if (! @mysql_query ( $sql ))
			return false;
		else
			return true;
	}
	function Update5($tbname) {
		$rs = @mysql_query ( "select * from " . $tbname );
		$sql = "update " . $tbname . " set ";
		
		for($c = 0; $c < @mysql_numfields ( $rs ); $c ++) {
			if ($c < @mysql_num_fields ( $rs ) - 1) {
				if (isset ( $_POST [@mysql_field_name ( $rs, $c )] ))
					$sql .= @mysql_field_name ( $rs, $c ) . ' = "' . mysql_real_escape_string ( $this->SaveVal ( $_POST [@mysql_field_name ( $rs, $c )] ) ) . '",';
			} else {
				if (isset ( $_POST [@mysql_field_name ( $rs, $c )] )) {
					$sql .= @mysql_field_name ( $rs, $c ) . ' = "' . mysql_real_escape_string ( $this->SaveVal ( $_POST [@mysql_field_name ( $rs, $c )] ) ) . '"';
				} else {
					// $sql .= @mysql_field_name($rs,$c)." = ''";
					$sql = substr ( $sql, 0, strlen ( $sql ) - 1 );
				}
			}
		}
		
		$sql .= ' where ' . @mysql_field_name ( $rs, 0 ) . ' ="' . $_POST [@mysql_field_name ( $rs, 0 )] . '"';
		
		if (! @mysql_query ( $sql ))
			return false;
		else
			return true;
	}
	function MultiSelectSave($tbname, $field, $criteria_field) {
		@mysql_query ( "delete from " . $tbname . " where " . $criteria_field . " ='" . $_REQUEST [$criteria_field] . "'" );
		
		$rs = @mysql_query ( "select * from " . $tbname );
		
		for($r = 0; $r < count ( $_REQUEST [$field] ); $r ++) {
			$sql = "insert into " . $tbname . " set ";
			
			for($c = 0; $c < @mysql_numfields ( $rs ); $c ++) {
				if ($c < @mysql_num_fields ( $rs ) - 1)
					$sql .= @mysql_field_name ( $rs, $c ) . " = '" . $this->SaveVal ( $_REQUEST [@mysql_field_name ( $rs, $c )] ) . "',";
				else
					$sql .= @mysql_field_name ( $rs, $c ) . " = '" . $this->SaveVal ( $_REQUEST [@mysql_field_name ( $rs, $c )] [$r] ) . "'";
			}
			
			@mysql_query ( $sql ) or die ( @mysql_error () );
		}
	}
	function Delete($tbname, $field) {
		@mysql_query ( "delete from " . $tbname . " where " . $field . " ='" . $_REQUEST ['id'] . "'" );
	}
	function NoDataFound($col_span) {
		print "<tr><td align='center' colspan='" . $col_span . "' style='color:#cc0000'>No Data Found....</td></tr>";
	}
	function DisplaySaveMessage() {
		print "<table class='tb' width='200' align='center' cellpadding='2' style='text-align:center; color: #006600; border-color:#dbdbdb'><tr><td nowrap>Record Saved Successfully....</td></tr></table>";
	}
	function DisplayMessage($msg) {
		print "<table class='tb' width='200' align='center' cellpadding='2' style='text-align:center; color: #006600; border-color:#dbdbdb'><tr><td nowrap>" . $msg . "</td></tr></table>";
	}
	
	// ******************** HTML FORM OBJECTS CREATION FUNCTION *****************************
	function CreateTextBox($name, $value = false, $attributes = false, $style = false, $javascript = false) {
		print "<input type='text' name='" . $name . "' " . $attributes . " value='" . $value . "' " . $style . " " . $javascript . ">";
	}
	function CreateCheckBox($name, $value = false, $attributes = false, $style = false, $javascript = false) {
		print "<input type='checkbox' name='" . $name . "' " . $attributes . " value='" . $value . "' " . $style . " " . $javascript . ">";
	}
	function CreateRadioButton($name, $value = false, $attributes = false, $style = false, $javascript = false) {
		print "<input type='radio' name='" . $name . "' " . $attributes . " value='" . $value . "' " . $style . " " . $javascript . ">";
	}
	function CreateTextArea($name, $value = false, $attributes = false, $style = false, $javascript = false) {
		print "<textarea name='" . $name . "' " . $attributes . " " . $style . " " . $javascript . ">" . $value . "</textarea>";
	}
	function CreateSubmitButton($name, $value = false, $attributes = false, $style = false, $javascript = false) {
		print "<input type='submit' name='" . $name . "' " . $attributes . " value='" . $value . "' " . $style . " " . $javascript . ">";
	}
	function CreateSimpleButton($name, $value = false, $attributes = false, $style = false, $javascript = false) {
		print "<input type='button' name='" . $name . "' " . $attributes . " value='" . $value . "' " . $style . " " . $javascript . ">";
	}
	function CreateFile($name, $value = false, $attributes = false, $style = false, $javascript = false) {
		print "<input type='file' name='" . $name . "' " . $attributes . " value='" . $value . "' " . $style . " " . $javascript . ">";
	}
	function GetBooker($user_id) {
		$user_name = $this->GetReference ( "admin", "uid", "user_id", $user_id );
		
		/*
		 * $user_parent_id = $this->GetReference("admin","parent_id","user_id",$user_id);
		 * if($user_parent_id!=0)
		 * {
		 * $user_parent = $this->GetReference("admin","uid","user_id",$user_parent_id);
		 * //$booker = $user_parent."--".$user_name;
		 * $booker = $user_name;
		 * }
		 * else
		 * {
		 * $booker = $user_name;
		 * }
		 */
		return $user_name;
	}
	function createthumb($sourcefile, $destfile, $filename, $new_w, $new_h) {
		$system = explode ( '.', $filename );
		
		if (preg_match ( '/jpg|JPG|jpeg|JPEG/', $system [1] )) {
			$src_img = imagecreatefromjpeg ( $sourcefile );
		}
		if (preg_match ( '/gif|GIF/', $system [1] )) {
			$src_img = imagecreatefromgif ( $sourcefile );
		}
		if (preg_match ( '/png|PNG/', $system [1] )) {
			$src_img = imagecreatefrompng ( $sourcefile );
		}
		$old_x = imageSX ( $src_img );
		$old_y = imageSY ( $src_img );
		
		$thumb_w = $new_w;
		$thumb_h = $new_h;
		
		$dst_img = ImageCreateTrueColor ( $thumb_w, $thumb_h );
		imagecopyresampled ( $dst_img, $src_img, 0, 0, 0, 0, $thumb_w, $thumb_h, $old_x, $old_y );
		if (preg_match ( "/gif|GIF/", $system [1] )) {
			imagegif ( $dst_img, $destfile );
		} else if (preg_match ( "/png|PNG/", $system [1] )) {
			imagepng ( $dst_img, $destfile );
		} else {
			imagejpeg ( $dst_img, $destfile );
		}
		imagedestroy ( $dst_img );
		imagedestroy ( $src_img );
	}
	function Func_Update_MOVE($id, $nseq, $tbl, $trail, $ext = '') {
		$Var_query = "SELECT * FROM $tbl WHERE trail_point_id=$id";
		// echo $Var_query;
		$rs_value = mysql_query ( $Var_query );
		$row = mysql_fetch_assoc ( $rs_value );
		
		$Var_query = "UPDATE  $tbl set sequence_number=1 WHERE sequence_number=$nseq and trail_id=$trail $ext";
		// echo $Var_query;
		$rs_value = mysql_query ( $Var_query ) or die ( mysql_error () );
		// echo $Var_query;
		$Var_query = "UPDATE $tbl set sequence_number='$nseq' WHERE trail_point_id='$id' and trail_id=$trail ";
		
		$rs_value = mysql_query ( $Var_query ) or die ( mysql_error () );
		
		$Var_query = "UPDATE  $tbl set sequence_number='" . $row ['sequence_number'] . "' WHERE sequence_number=0 or sequence_number=1 and trail_id=$trail $ext";
		// echo $Var_query;
		$rs_value = mysql_query ( $Var_query ) or die ( mysql_error () );
	}
}

// ************ Added By Amin Dad Shah************//
function check_data($tbl, $where) {
	$sql = "select * from $tbl where $where";
	$res = mysql_query ( $sql ) or die ( mysql_error () );
	if (mysql_num_rows ( $res ) == 0)
		return 0;
	else
		return 1;
}

/* Function to View Data */
function view_table_data($tbl, $where) {
	$sql = "select * from $tbl where $where";
	$res = mysql_query ( $sql ) or die ( mysql_error () );
	while ( $row = mysql_fetch_array ( $res ) ) {
		$arr [] = $row;
	}
	return $arr;
}

/* Function to Get Single Colum Only */
function get_single_value($col, $tbl, $where) {
	$sql = "select $col from $tbl where $where";
	$res = mysql_query ( $sql ) or die ( mysql_error () );
	$row = mysql_fetch_array ( $res );
	$arr [] = $row;
	return $arr;
}

/* Function to get Single Row */
function get_single_row($tbl, $where) {
	$sql = "select * from $tbl where $where";
	$res = mysql_query ( $sql ) or die ( mysql_error () );
	$row = mysql_fetch_array ( $res );
	$arr [] = $row;
	return $arr;
}

/* Function to get count */
function get_count($tbl, $where) {
	$sql = "select count(*) as num from " . $tbl . " where " . $where . " ";
	$result = mysql_query ( $sql ) or die ( mysql_error () );
	$getCount = mysql_fetch_array ( $result );
	$showCount = $getCount [num];
	if ($showCount == '0')
		print 0;
	else
		print $showCount;
}

// delete function
function delete($tbl, $where) {
	$sql = "delete from $tbl where $where";
	$result = mysql_query ( $sql ) or die ( mysql_error () );
}
function addMigration($tbname, $primeryKey, $value) {
	$milliseconds = round ( microtime ( true ) * 1000 );
	$statement = "DELETE FROM " . $tbname . " WHERE " . $primeryKey . " = " . $value . "";
	@mysql_query ( "INSERT INTO migration_statements (satement_text , table_name , record_id , created_at , updated_at) VALUES('" . $statement . "' , '" . $tbname . "' , '" . $value . "' , '" . $milliseconds . "' , '" . $milliseconds . "')" );
}


?>