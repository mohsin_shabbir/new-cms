	<?php 
	 include_once("common.php");
    class Data extends Function_Library
	{
		function Save()
		{
			$selectBranches = mysql_query("SELECT * FROM tbl_branches WHERE status = 1") or die(mysql_error());
			if(mysql_num_rows($selectBranches) >= 2)
			{
			?>
			<script type="text/javascript">
           		alert("You can not add more than two branches. Please deactivate anyone first.");
				window.location="view_branch.php?mode=add&cid="<?php $id ?>;
            </script>			
			<?php
			}
			else
			{
			parent::Save("tbl_branches");		
			$id = @mysql_insert_id();
			//header("Location: view_about_us.php?mode=add&cid=".$id);
			 ?>
			<script type="text/javascript">
            window.location="view_branch.php?mode=add&cid="<?php $id ?>;
            </script>			
		<?php
			}
				
		}
		
		function Update()
		{
			$selectBranches1 = mysql_query("SELECT * FROM tbl_branches WHERE status = 1") or die(mysql_error());
		    if($_REQUEST['status'] ==1 && mysql_num_rows($selectBranches1) > 2)
			{
			?>
			<script type="text/javascript">
           		alert("You already have two branches active.");
				window.location="view_branch.php?mode=add&cid="<?php $id ?>;
            </script>			
			<?php
			}
			else
			{
			parent::Update("tbl_branches");
			$id=$_REQUEST['id'];
			?>
			<script type="text/javascript">
            window.location="view_branch.php?mode=update&cid="<?php $id ?>;
            </script>
			<?php
			}
			}
		
		
		function ShowDesign()
		{			
			$rs_art = @mysql_query("select * from tbl_branches where id='".$_REQUEST['cid']."'");
			$row_art = @mysql_fetch_object($rs_art);
			include_once("includes/header.php");
?>
				<script>
                        function reg_valid()
                        {	
						
							////////////////////////////////////////////////////title User Name /////////////////////////////////////////////							
                            if(document.getElementById('branch_address').value ==""  || document.getElementById('branch_address').value.charAt("0") == " ")
                            {                               
								document.getElementById("branchErrorDiv").style.display='block';
								document.getElementById('branch_error').innerHTML='* Please enter branch address.';
                               // document.getElementById('username').focus();
                                return false;
                            }
                            else
                            {
                                document.getElementById('branch_error').innerHTML='';
								document.getElementById("branchErrorDiv").style.display='none';
                            }
							////////////////////////////////////////////////////title User Name /////////////////////////////////////////////
							////////////////////////////////////////////////////title User Name /////////////////////////////////////////////							
                            if(document.getElementById('branch_number').value ==""  || document.getElementById('branch_number').value.charAt("0") == " ")
                            {                               
								document.getElementById("branch_numberErrorDiv").style.display='block';
								document.getElementById('branch_number_error').innerHTML='* Please enter phone number.';
                               // document.getElementById('username').focus();
                                return false;
                            }
                            else
                            {
                                document.getElementById('branch_number_error').innerHTML='';
								document.getElementById("branch_numberErrorDiv").style.display='none';
                            }
							////////////////////////////////////////////////////title User Name /////////////////////////////////////////////
												
						
						
                        }
                    </script>
            
			<script>
            
            function RestrictSpace()
             {
                if (event.keyCode == 32) 
                {
				//alert("Spaces are not allowed.");
                event.returnValue = false;
                return false;
                }
              }
            
            </script>

        <div id="page-wrapper">
          <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header"><?php if($_REQUEST['cid']){?>Edit <?php }else{?>Add <?php } ?> Branch</h1>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            SEO Form
                        </div>
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-lg-6 col-md-offset-3 AlignFormCenter" >
                                	<form  name="frm" id="frm" action="" method="post" role="form" onSubmit="return reg_valid()">
									<?php
                                    if($_REQUEST['cid'])
                                    {
                                    	print "<input type='hidden' name='id' value='".$_REQUEST['cid']."'>";
                                    }
                                    ?>
                                    <?php if($_REQUEST['mode'] == "exist"){?>
                                    
                                    <div class="alert alert-danger alert-dismissable">
                                   <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                                    <h4 class="alert-heading">Warning!</h4>                            
						        	<p>Record Already Exit!</p>
                     			   </div>
                                    
                                    <?php } ?>
                                        
                                        
                                    	<div class="form-group">
                                            <label>Branch Number</label>
                                            <input type="text" class="form-control" name="branch_number" onkeypress="return isNumber(event)"  onBlur = "return reg_valid();" id="branch_number"                              onKeyUp="return RestrictSpace()" value="<?php print stripslashes($row_art->branch_number);?>" >
                                             <div id="branch_numberErrorDiv" class="errorDiv">          
                                       		 <label class="error" id="branch_number_error"></label>
                                      	   </div>
                                        </div>
                                        <div class="form-group">
                                            <label>Branch Status</label>
                                            <select class="form-control" name="status" id="status">
                                                <option value="1" <?php if($row_art->status == "1") print 'selected';?>>Active</option>
                                                <option value="0" <?php if($row_art->status == "0") print 'selected';?>>Inactive</option>
                                            </select>
                                        </div>                                     
                                        
                                        <div class="form-group">
                                            <label>Branch Address</label>
                                           <textarea name="branch_address" id="branch_address" style="width:100%;" rows="5"  onBlur = "return reg_valid();" ><?php print stripslashes($row_art->branch_address);?></textarea>
                                           <div id="branchErrorDiv" class="errorDiv">          
                                       		 <label class="error" id="branch_error"></label>
                                       	    </div>

                                        </div>
                                        
                                        <button type="submit" class="btn btn-default" name="cmdSave">Submit Button</button>
                                        <button type="reset"  onclick="goBack()" class="btn btn-default">Cancel</button>
                                    </form>
                                </div>
                                <!-- /.col-lg-6 (nested) -->
                                
                                <!-- /.col-lg-6 (nested) -->
                            </div>
                            <!-- /.row (nested) -->
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
        </div>
        <!-- /#page-wrapper -->
<?php 
include_once("includes/footer.php");
		}
		}	
		$oA = new Data;
		if(isset($_REQUEST['cmdSave']) or isset($_REQUEST['cmdUser']))
		{
			
		
			if($_REQUEST['cid']=='')
			{
				$oA->Save();				
			}
			else
			{
				
				$oA->Update();				
			}
		}
		else
		{		
			$oA->ShowDesign();
		}
?>