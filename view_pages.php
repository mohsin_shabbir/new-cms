<?php 		
 		include_once("common.php");
		include_once("includes/header.php");
?>
        
        <script type="text/javascript" >
			$(function() {
			$(".delbutton").click(function(){
		    var del_id = $(this).attr("id");
			var tbl = 'ce_content';
			var info = 'id=' + del_id+'&tbl='+ tbl;
			if(confirm("Sure you want to delete this record? There is NO undo!"))
			{
			$.ajax({
			type: "POST",
			url: "delete.php",
			data: info,
			success: function(){
			}
			});
			$(this).parents(".gradeA").animate({ backgroundColor: "red" }, "fast")
			.animate({ opacity: "hide" }, "slow");
			}
			return false;
			});
			});
         </script>
	 <!-- DataTables CSS -->
<link rel="stylesheet" href="plugins/datatables/dataTables.bootstrap.css">
<div class="content-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">View Content</h1>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                          <?php if($_REQUEST['mode'] == "add" || $_REQUEST['mode'] == "update" || $_REQUEST['mode'] == "delete"){?>
                          <div class="alert alert-success alert-dismissable">
                                   <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                                                   
						        	<strong>Well done!</strong> You have successfully complete this operation.
                     			   </div>
                        <?php } 
						   if($_SESSION['user_role']==1 || $_SESSION['user_role']==2 || $_SESSION['user_role']==3){?>  <a href="add_pages.php">Add Content</a> <?php } ?>
						   
						  
                        </div>
                        <!-- /.panel-heading -->
                        <div class="panel-body">
                            <div class="table-responsive">
                                <table class="table table-striped table-bordered table-hover" id="dataTables-example">
                                    <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>Page Name</th>
                                            
                                            <th>Description</th>
											<th>Created At</th>
                                            <th>Updated At</th>  
                                            <th>Status</th>
											 <th>Approved</th>
                                            <th>Options</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                <?php	
				$sql = "select * from ce_content order by content_id desc";
				$rs = mysql_query($sql) or die(mysql_error());
				if(mysql_num_rows($rs) == 0)
				global $i;
				$i = 1;
				while($oA=@mysql_fetch_array($rs))
				{
				  
				?>
                                       <tr class="gradeA <?php if($oA['approved']==0){?> approved <?php } ?>" >
                                            <td><?php print $i; ?></td>
                                            <td><?php print stripslashes(strip_tags($oA['content_type'])); ?></td>
                                          
                                            <td>
											<?php print substr(stripslashes(strip_tags($oA['content_text'])),0, 30);
                                            if(strlen(stripslashes(strip_tags($oA['content_text'])))>30){?>
                                            <a href="add_pages.php?cid=<?php print $oA['content_id']; ?>">  View More...</a>
                                            <?php } ?>
                                            </td>
											 <td><?php 	$seconds = $oA['created_at'] / 1000;echo date("d/m/Y H:i:s", $seconds); ?></td>
											 <td><?php 	$seconds = $oA['updated_at'] / 1000;echo date("d/m/Y H:i:s", $seconds); ?></td>		
                                            <td class="center">
											<?php 
                                            switch($oA['content_status']){
                                            case 1:
                                            print 'Active';
                                            break;
                                            case 0:
                                            print '<font color="red">Inactive</font>';
                                            break;
                                            }
                                            ?>
                                            </td>
											<td>
												<div class="checkbox-container">
													<input type="checkbox" title="content_id" name="<?php print $oA['content_id']; ?>" class="custom-switch" 
													id="<?php print $oA['content_id']; ?>" value="ce_content" <?php if($oA['approved']==1){?> checked <?php } ?> >	
												</div>
											</td>
                                            <td class="center">
											<a class="btn btn-info" title="Edit" href="add_pages.php?cid=<?php print $oA['content_id']; ?>">
                                            <i class="icon-edit icon-white"></i>  
                                            Edit                                            
                                            </a>
											<?php 
						  					 if($_SESSION['user_role']==1 || $_SESSION['user_role']==2){?>  
													  
                                            
                                            <a class="btn btn-danger delbutton" href="#" title="Delete" id = <?php echo $oA["content_id"]; ?>>
                                            <i class="icon-trash icon-white"></i> 
                                            Delete
                                            </a>
                                            <?php } ?>
											
                                            
                                            </td>
                                        </tr>
                                    </tbody>
               <?php $i++;} ?>
                                </table>
                            </div>
                            <!-- /.table-responsive -->
                            
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->

        </div>
        <!-- /#page-wrapper -->
<?php 
include_once("includes/footer.php");
?>
 <!-- DataTables JavaScript -->
    <script src="js/plugins/dataTables/jquery.dataTables.js"></script>
    <script src="js/plugins/dataTables/dataTables.bootstrap.js"></script>

    <script>
  $(document).ready(function() { 
    $('[data-toggle="tooltip"]').tooltip();  
       /// $('#dataTables-example').dataTable();
		 $('#dataTables-example').DataTable({
          "paging": true,
          "lengthChange": true,
          "searching": true,
          "ordering": true,
          "info": true,
          "autoWidth": true,
		  "scrollX": true
        });
    });
    </script>
	<style>
#dataTables-example {
    display: table;
    white-space: nowrap;
}
</style>