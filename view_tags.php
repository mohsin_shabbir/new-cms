<?php 		
 		include_once("common.php");
		include_once("includes/header.php");
?>
        
        <script type="text/javascript" >
			$(function() {
			$(".delbutton").click(function(){
		    var del_id = $(this).attr("id");
			var tbl = 'ce_tags';
			var info = 'id=' + del_id+'&tbl='+ tbl;
			if(confirm("Sure you want to delete this record? There is NO undo!"))
			{
			$.ajax({
			type: "POST",
			url: "delete.php",
			data: info,
			success: function(data){
				if(data == 1)
				{
					$('#'+del_id).parents(".gradeA").animate({ backgroundColor: "red" }, "fast")
					.animate({ opacity: "hide" }, "slow");
				}
				else
				{
					alert("You can't delete, as this tag is attached with some offer.");
				}
			}
			});
		
			}
			return false;
			});
			});
         </script>
	 <!-- DataTables CSS -->
<link rel="stylesheet" href="plugins/datatables/dataTables.bootstrap.css">
	<div class="content-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">View Tags</h1>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                          <?php if($_REQUEST['mode'] == "add" || $_REQUEST['mode'] == "update" || $_REQUEST['mode'] == "delete"){?>
                          <div class="alert alert-success alert-dismissable">
                                   <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                                                   
						        	<strong>Well done!</strong> You have successfully complete this operation.
                     			   </div>
                          <?php } 
						   if($_SESSION['user_role']==1 || $_SESSION['user_role']==2 || $_SESSION['user_role']==3){?> <a href="add_tag.php">Add Tag</a> <?php } ?>
						  
                        </div>
                        <!-- /.panel-heading -->
                        <div class="panel-body">
                            <div class="table-responsive">
                                <table class="table table-striped table-bordered table-hover" id="dataTables-example">
                                    <thead>
                                        <tr>
                                           <th>#</th>
                                            <th>Tag Name</th>
                                           <!-- <th>Sale Applied</th>-->
                                               <th>Created At</th>
                                            <th>Updated At</th>                                   
                                            <th>Status</th>
											 <th>Approved</th>
                                            <th>Options</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                <?php	
				$sql = "select * from ce_tags order by tag_id desc";
				$rs = mysql_query($sql) or die(mysql_error());
				if(mysql_num_rows($rs) == 0)
				global $i;
				$i = 1;
				while($oA=@mysql_fetch_array($rs))
				{
				  
				?>
                                      <tr class="gradeA <?php if($oA['approved']==0){?> approved <?php } ?>" >
                                            <td><?php print $i; ?></td>
                                            <td><?php print stripslashes(strip_tags($oA['tag_name'])); ?></td>
                                           
                                          <?php /*?>  <td><?php
											if($oA['apply_sale'] == 1) { echo 'Yes';}else{echo 'No'; } ?></td><?php */?>
                                             <td><?php 	$seconds = $oA['created_at'] / 1000;echo date("d/m/Y H:i:s", $seconds); ?></td>
											 <td><?php 	$seconds = $oA['updated_at'] / 1000;echo date("d/m/Y H:i:s", $seconds); ?></td>		
                                           
                                            <td class="center">
											<?php 
                                            switch($oA['status']){
                                            case 1:
                                            print 'Active';
                                            break;
                                            case 0:
                                            print '<font color="red">Inactive</font>';
                                            break;
                                            }
                                            ?>
                                            </td>
											<td>
												<div class="checkbox-container">
													<input type="checkbox" title="tag_id" name="<?php print $oA['tag_id']; ?>" class="custom-switch" 
													id="<?php print $oA['tag_id']; ?>" value="ce_tags" <?php if($oA['approved']==1){?> checked <?php } ?> >	
												</div>
											</td>		
                                            <td class="center">
											<a class="btn btn-info" title="Edit" href="add_tag.php?cid=<?php print $oA['tag_id']; ?>">
														<i class="icon-edit icon-white"></i>  
														Edit                                            
														</a>
											<?php 
						  					 if($_SESSION['user_role']==1 || $_SESSION['user_role']==2){?>  
													 
														<a class="btn btn-danger delbutton" href="#" title="Delete" id = <?php echo $oA["tag_id"]; ?>>
														<i class="icon-trash icon-white"></i> 
														Delete
														</a>
                                            <?php } ?>
                                         
                                            </td>
                                        </tr>
                                   
               <?php $i++;} ?>
               					 </tbody>
                                </table>
                            </div>
                            <!-- /.table-responsive -->
                            
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->

        </div>
        <!-- /#page-wrapper -->
<?php 
include_once("includes/footer.php");
?>
 <!-- DataTables JavaScript -->
    <script src="js/plugins/dataTables/jquery.dataTables.js"></script>
    <script src="js/plugins/dataTables/dataTables.bootstrap.js"></script>
    <script>
    $(document).ready(function() {
        $('#dataTables-example').dataTable();
    });
    </script>