	<?php
		include_once("common.php");
		include_once("includes/header.php");
	?>
    <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            Dashboard
            <small>Control panel</small>
          </h1>
         <!-- <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Dashboard</li>
          </ol>-->
        </section>

        <!-- Main content -->
        <section class="content">
          <!-- Small boxes (Stat box) -->
          <!-- /.row -->
          <!-- Main row -->
		   <div class="row">
		   <div class="col-lg-3 col-xs-6">
              <!-- small box -->
              <div class="small-box bg-aqua">
                <div class="inner">
                  <h3><?php
					 $slectBuyers = "SELECT * FROM ce_offers WHERE status = 1";
					 $mysqlBuyers = mysql_query($slectBuyers) or die(mysql_error());
					 echo mysql_num_rows($mysqlBuyers);
					
					 ?></h3>
                  <p>Offers</p>
                </div>
                <div class="icon">
                  <i class="ion ion-bag"></i>
                </div>
                <a href="view_offers.php" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
              </div>
            </div>
                
				<div class="col-lg-3 col-xs-6">
				  <!-- small box -->
				  <div class="small-box bg-green">
					<div class="inner">
					  <h3><?php					
					 $slectBuyers = "SELECT * FROM ce_outlet WHERE status = 1";
					 $mysqlBuyers = mysql_query($slectBuyers) or die(mysql_error());
					 echo mysql_num_rows($mysqlBuyers);
					 ?><sup style="font-size: 20px"></sup></h3>
					  <p>Outlets</p>
					</div>
					<div class="icon">
					  <i class="ion ion-stats-bars"></i>
					</div>
					<a href="view_outlets.php" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
				  </div>
				</div><!-- ./col -->
				<div class="col-lg-3 col-xs-6">
				  <!-- small box -->
				  <div class="small-box bg-yellow">
					<div class="inner">
					  <h3><?php
						 $slectBuyers = "SELECT * FROM tbl_user ";
						 $mysqlBuyers = mysql_query($slectBuyers) or die(mysql_error());
						 echo mysql_num_rows($mysqlBuyers);
						 ?></h3>
					  <p>User Registrations</p>
					</div>
					<div class="icon">
					  <i class="ion ion-person-add"></i>
					</div>
					<a href="view_users.php" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
				  </div>
				</div><!-- ./col -->
					
                
                <div class="col-lg-3 col-md-6">
                    <div class="panel panel-red">
                        <div class="panel-heading">
                            <div class="row">
                                <div class="col-xs-3">
                                    <i class="fa fa-support fa-5x"></i>
                                </div>
                                <div class="col-xs-9 text-right">
                                    <div class="huge">
                                    <?php
									 $slectBuyers = "SELECT * FROM ce_reviews ";
									 $mysqlBuyers = mysql_query($slectBuyers) or die(mysql_error());
									 echo mysql_num_rows($mysqlBuyers);
									 ?>
                                    </div>
                                    <div>Reviews</div>
                                </div>
                            </div>
                        </div>
                        <a href="view_review_approval.php">
                            <div class="panel-footer">
                                <span class="pull-left">More Info</span>
                                <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                                <div class="clearfix"></div>
                            </div>
                        </a>
                    </div>
                </div>
            </div>
            <!-- /.row -->
            <div class="row">
                <div class="col-lg-3 col-md-6">
                    <div class="panel panel-primary">
                        <div class="panel-heading">
                            <div class="row">
                                <div class="col-xs-3">
                                    <i class="fa fa-comments fa-5x"></i>
                                </div>
                                <div class="col-xs-9 text-right">
                                    <div class="huge">
                                    <?php
									 $selectSellingValuation = "SELECT * FROM add_category where status = '1'";
									$sqlExecuteSelectSellingValuation = mysql_query($selectSellingValuation) or die(mysql_error());
									echo $numOfSellingRows = mysql_num_rows($sqlExecuteSelectSellingValuation);
									 ?>
                                    </div>
                                    <div>Categories</div>
                                </div>
                            </div>
                        </div>
                        <a href="view_category.php">
                            <div class="panel-footer">
                                <span class="pull-left">View Details</span>
                                <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                                <div class="clearfix"></div>
                            </div>
                        </a>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6">
                    <div class="panel panel-green">
                        <div class="panel-heading">
                            <div class="row">
                                <div class="col-xs-3">
                                    <i class="fa fa-tasks fa-5x"></i>
                                </div>
                                <div class="col-xs-9 text-right">
                                    <div class="huge">
                                    <?php
									$selectValuation = "SELECT * FROM tbl_sub_category ";
									$sqlExecuteSelectValuation = mysql_query($selectValuation) or die(mysql_error());
									echo $numOfLettingRows = mysql_num_rows($sqlExecuteSelectValuation);
									 ?>
                                    </div>
                                    <div>Sub Categories</div>
                                </div>
                            </div>
                        </div>
                        <a href="view_sub_category.php">
                            <div class="panel-footer">
                                <span class="pull-left">More Info</span>
                                <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                                <div class="clearfix"></div>
                            </div>
                        </a>
                    </div>
                </div>
				<div class="col-lg-3 col-xs-6">
              <!-- small box -->
              <div class="small-box bg-red">
                <div class="inner">
                  <h3>  <?php
					 $slectContact = "SELECT * FROM applicants";
					 $mysqlContact = mysql_query($slectContact) or die(mysql_error());
					 echo mysql_num_rows($mysqlContact);
					 ?></h3>
                  <p>Applicants</p>
                </div>
                <div class="icon">
                  <i class="ion ion-pie-graph"></i>
                </div>
                <a href="view_applicants.php" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
              </div>
            </div>
			<div class="col-lg-3 col-xs-6">
              <!-- small box -->
              <div class="small-box bg-aqua">
                <div class="inner">
                  <h3><?php
									 $slectBuyers = "SELECT * FROM  ce_news ";
									 $mysqlBuyers = mysql_query($slectBuyers) or die(mysql_error());
									 echo mysql_num_rows($mysqlBuyers);
									 ?></h3>
                  <p>News</p>
                </div>
                <div class="icon">
                  <i class="ion ion-bag"></i>
                </div>
                <a href="view_news.php" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
              </div>
            </div>
                
                
            </div>
        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->
   <?php 
		include_once("includes/footer.php");
	?>