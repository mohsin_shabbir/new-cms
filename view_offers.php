<?php
include_once("common.php");
include_once("includes/header.php");
?>
        <script type="text/javascript" >
			$(function() {
			$(".say_it").click(function(){
		    var userViewId = $(this).attr("id");
			//var tbl = 'tbl_property';
			var info = 'offerView=' + userViewId;
			$("#loader").css("display", "block");
			$.ajax({
			type: "POST",
			url: "getAddress.php",
			data: info,
			success: function(data){
				$(".showPopup").show(1000);
				$("#loader").css("display", "none");
				$("#response").html(data);			
				
				//location.reload();
			}
			});
			$(this).parents(".gradeA").animate({ backgroundColor: "red" }, "fast")
			.animate({ opacity: "show" }, "slow");
			
			return false;
			});
			});
			function closePopup()
			{
				$(".showPopup").hide(1000);
    //// 3.  $.window.close();
			}
         </script>
        <script type="text/javascript" >

			$(function() {
			
			$(".delbutton").click(function(){
			var del_id = $(this).attr("id");
			var tbl = 'ce_offers';
			var info = 'id=' + del_id+'&tbl='+ tbl;
			if(confirm("Sure you want to delete this record? There is NO undo!"))
			{
			$.ajax({
			type: "POST",
			url: "delete.php",
			data: info,
			success: function(){
			}
			});
			$(this).parents(".gradeA").animate({ backgroundColor: "red" }, "fast")
			.animate({ opacity: "hide" }, "slow");
			}
			return false;
			});
			});
         </script>
	 <!-- DataTables CSS -->
<link rel="stylesheet" href="plugins/datatables/dataTables.bootstrap.css">
<div class="content-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">View Offers</h1>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                          <?php if($_REQUEST['mode'] == "add" || $_REQUEST['mode'] == "update" || $_REQUEST['mode'] == "delete"){?>
                          <div class="alert alert-success alert-dismissable">
                                   <button aria-hidden="true" data-dismiss="alert" class="close" type="button">�</button>
                                                   
						        	<strong>Well done!</strong> You have successfully complete this operation.
                     			   </div>
                          <?php } 
						   if($_SESSION['user_role']==1 || $_SESSION['user_role']==2 || $_SESSION['user_role']==3){?>  <a href="add_offers.php">Add Offer</a> <?php } ?>
						   
                          
                        </div>
                        <!-- /.panel-heading -->
                        <div class="panel-body">
                            <div class="table-responsive">
								<img src="images/loader.gif" id="loader" />
                                <table class="table table-striped table-bordered table-hover" id="dataTables-example">
                                    <thead>
                                        <tr>
                                            <th>#</th>
											<th>Id</th>
                                            <th>Name</th>
											<th>Outlets</th>
											<th>Tags</th>
                                            <th>Valid From</th>
											<th>Expiry</th>
											<th>Airmiles</th>
											<th>Sponsored</th>											
											<th>Approved</th>
											<th>Created At</th>
                                            <th>Updated At</th>  
                                            <th>Status</th>
											<th>Approved</th>
                                            <th width="18%">Options</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                <?php	
				
					$where = 'status = 1 OR status = 0 ';
				
				$sql = "select * from ce_offers where ".$where." order by off_id desc";
				$rs = mysql_query($sql) or die(mysql_error());
				if(mysql_num_rows($rs) == 0)
				
				$shoot = 1;
				while($oA=@mysql_fetch_array($rs))
				{
				  
				?>
                                       <tr class="gradeA <?php if($oA['approved']==0){?> approved <?php } ?>" >
                                            <td><?php echo $shoot+1 ; ?></td>
											<td><?php print stripslashes(strip_tags($oA['off_id'])); ?></td>
                                            <td><?php print stripslashes(strip_tags($oA['off_name'])); ?></td>
											<td>
											<?php
												$out_ids = explode(",",$oA['out_ids']);	
												for($i=0; $i<count($out_ids); $i++)
												{
													$catName = mysql_fetch_assoc(mysql_query("SELECT * FROM  ce_outlet WHERE out_id = '".$out_ids[$i]."'")); ?>
													<?php print stripslashes(strip_tags($catName['out_name'])).' <br>'; 
												}				
											?>
											</td>
											<td>
											<?php
												$tag_ids = explode(",",$oA['tag_ids']);	
												for($i=0; $i<count($tag_ids); $i++)
												{
													$catName = mysql_fetch_assoc(mysql_query("SELECT * FROM  ce_tags WHERE tag_id = '".$tag_ids[$i]."'")); ?>
													<?php print stripslashes(strip_tags($catName['tag_name'])).' <br>'; 
												}				
											?>
											</td>
                                            <td><?php print stripslashes(strip_tags($oA['off_valid_from'])); ?></td>
                                            <td><?php print stripslashes(strip_tags($oA['off_expiry'])); ?></td>
											 <td><?php print stripslashes(strip_tags($oA['offer_airmiles'])); ?></td>
											 <td><?php if($oA['sponsored'] == 1){echo 'Yes';}else{echo 'No';} ?></td>
											 <td>	<?php 
                                            switch($oA['approved']){
                                            case 1:
                                            print 'Yes';
                                            break;
                                            case 0:
                                            print '<font color="red">No</font>';
                                            break;
                                            }
                                            ?></td>
											 <td><?php 	$seconds = $oA['created_at'] / 1000;echo date("d/m/Y H:i:s", $seconds); ?></td>
											 <td><?php 	$seconds = $oA['updated_at'] / 1000;echo date("d/m/Y H:i:s", $seconds); ?></td>			
                                            <?php /*?><td>
                                 <?php print substr(stripslashes(strip_tags($oA['description'])),0, 30);
								if(strlen(stripslashes(strip_tags($oA['description'])))>30){?>
                                <a href="add_users.php?cid=<?php print $oA['id']; ?>">  View More...</a>
                                <?php } ?>
                                            </td><?php */?>
                                            <td class="center">
											<?php 
                                            switch($oA['status']){
                                            case 1:
                                            print 'Active';
                                            break;
                                            case 0:
                                            print '<font color="red">Inactive</font>';
                                            break;
                                            }
                                            ?>
                                            </td>
											
											<td>
												<div class="checkbox-container">
													<input type="checkbox" title="off_id" name="<?php print $oA['off_id']; ?>" class="custom-switch" 
													id="<?php print $oA['off_id']; ?>" value="ce_offers" <?php if($oA['approved']==1){?> checked <?php } ?> >	
												</div>
											</td>
											
                                            <td class="center">
                                            <a class="initialism fadeandscale_open btn btn-success say_it" href="#fadeandscale" id = <?php echo $oA["off_id"]; ?>>View </a>
											<a class="btn btn-info" title="Edit" href="add_offers.php?cid=<?php print $oA['off_id']; ?>">
														<i class="icon-edit icon-white"></i>  
														Edit                                            
														</a>
											<?php 
						  					 if($_SESSION['user_role']==1 || $_SESSION['user_role']==2){?>  
													 
														<a class="btn btn-danger delbutton" href="#" title="Delete" id = <?php echo $oA["off_id"]; ?>>
														<i class="icon-trash icon-white"></i> 
														Delete
													</a>
                                            <?php } ?>
                                            
                                            </td>
                                        </tr>
                                   
               <?php $shoot++;} ?>
               					 </tbody>
                                </table>
                            </div>
                            <!-- /.table-responsive -->
                            
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->

        </div>
        <!-- /#page-wrapper -->
<?php 
include_once("includes/footer.php");
?>
 <!-- DataTables JavaScript -->
    <script src="js/plugins/dataTables/jquery.dataTables.js"></script>
    <script src="js/plugins/dataTables/dataTables.bootstrap.js"></script>

    <script>
  $(document).ready(function() { 
    $('[data-toggle="tooltip"]').tooltip();  
       /// $('#dataTables-example').dataTable();
		 $('#dataTables-example').DataTable({
          "paging": true,
          "lengthChange": true,
          "searching": true,
          "ordering": true,
          "info": true,
          "autoWidth": true,
		  "scrollX": true
        });
    });
    </script>
	<style>
#dataTables-example {
    display: table;
    white-space: nowrap;
}
</style>
