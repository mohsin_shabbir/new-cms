<?php 
 $address_url = 'http://maps.googleapis.com/maps/api/geocode/json?latlng=51.5272695627314,-0.08910921693588056&sensor=false';
 $address_json = json_decode(file_get_contents($address_url) , true);

 ?>
 <div class="form-group">
  <label>Select User Type</label>
 <select name="fullAddress" id="fullAddress">
 <?php
 foreach($address_json['results'] as $key=>$val){
      $tmp=array();
     foreach($val['address_components'] as $key1=>$val1){
         if($key1!=5)
		 {
            array_push($tmp,$val1['long_name']);
         }
         }
		 ?>
         <option value="<?php echo implode(',',$tmp); ?>"> <?php echo implode(',',$tmp); ?></option>
         <?php
    }
?>
</select>
</div>