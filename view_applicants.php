<?php
include_once("common.php");
include_once("includes/header.php");
?>
        <script type="text/javascript" >
			$(function() {
			$(".say_it").click(function(){
		    var userViewId = $(this).attr("id");
			//var tbl = 'tbl_property';
			var info = 'outletView=' + userViewId;
			$("#loader").css("display", "block");
			$.ajax({
			type: "POST",
			url: "getAddress.php",
			data: info,
			success: function(data){
				$(".showPopup").show(1000);
				$("#loader").css("display", "none");
				$("#response").html(data);
				
				//location.reload();
			}
			});
			$(this).parents(".gradeA").animate({ backgroundColor: "red" }, "fast")
			.animate({ opacity: "show" }, "slow");
			
			return false;
			});
			});
			function closePopup()
			{
				$(".showPopup").hide(1000);
    //// 3.  $.window.close();
			}
         </script>
        <script type="text/javascript" >

			$(function() {
			
			$(".delbutton").click(function(){
			var del_id = $(this).attr("id");
			var tbl = 'applicants';
			var info = 'id=' + del_id+'&tbl='+ tbl;
			if(confirm("Sure you want to delete this record? There is NO undo!"))
			{
			$.ajax({
			type: "POST",
			url: "delete.php",
			data: info,
			success: function(){
			}
			});
			$(this).parents(".gradeA").animate({ backgroundColor: "red" }, "fast")
			.animate({ opacity: "hide" }, "slow");
			}
			return false;
			});
			});
         </script>
	 <!-- DataTables CSS -->
 <link rel="stylesheet" href="plugins/datatables/dataTables.bootstrap.css">
<div class="content-wrapper">
      
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">View Applicants</h1>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                          <?php if($_REQUEST['mode'] == "add" || $_REQUEST['mode'] == "update" || $_REQUEST['mode'] == "delete"){?>
                          <div class="alert alert-success alert-dismissable">
                                   <button aria-hidden="true" data-dismiss="alert" class="close" type="button">�</button>
                                                   
						        	<strong>Well done!</strong> You have successfully complete this operation.
                     			   </div>
                          <?php } 
						   if($_SESSION['user_role']==1 || $_SESSION['user_role']==2 || $_SESSION['user_role']==3){ ?>   <a href="add_applicant.php">Add Applicant</a> <?php } ?>
                          
                        </div>
                        <!-- /.panel-heading -->
                        <div class="panel-body">
                            <div class="table-responsive">
							<img src="images/loader.gif" id="loader" />
                                <table class="table table-striped table-bordered table-hover" id="dataTables-example">
                                    <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>Name</th>
                                            <th>Email</th>
                                            <th>Registered</th> 
											<th>Membership Number</th>
											<th>Gender</th>
											<th>Religion</th>
											<th>Nationality</th>
											<th>DOB</th>
											<th>Phone</th>
											<th>District</th>
											<th>City</th>
											<th>Employer</th>	
											
											<th>Company</th>
											<th>Company Ref.</th>
											<th>Position</th>
											<th>Nominees</th>
											<th>Referred By</th>
											<th>Start</th>
											<th>Expire</th>
											<th>Type</th>
											<th>Fee</th>											
											<th>Payment Mode</th>
														
											<th>Created At</th>
                                            <th>Updated At</th>  
                                            <th>Status</th>
											<th>Approved</th>
                                            <th>Options</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                <?php	
				
					$where = 'status = 1 OR status = 0 ';
				
				$sql = "select * from applicants where ".$where." order by id desc";
				$rs = mysql_query($sql) or die(mysql_error());
				if(mysql_num_rows($rs) == 0)
				global $i;
				$i = 1;
				while($oA=@mysql_fetch_array($rs))
				{
				  
				?>
                                       <tr class="gradeA <?php if($oA['approved']==0){?> approved <?php } ?>" >
                                            <td><?php print $i; ?></td>
                                            <td><?php print stripslashes(strip_tags($oA['fname'])).' '.stripslashes(strip_tags($oA['lname'])); ?></td>
                                            <td><?php print stripslashes(strip_tags($oA['email'])); ?></td>
                                         <td><?php
				 $user = mysql_query("SELECT * FROM users WHERE email = '".stripslashes(strip_tags($oA['email']))."'"); ?>
				<?php if(mysql_num_rows($user) > 0) {echo 'Yes';}else{echo 'No';} ?></td>
											</td> 
											<td><?php print stripslashes(strip_tags($oA['membership_no'])); ?></td>
											<td><?php print stripslashes(strip_tags($oA['gender'])); ?></td>
											<td><?php print stripslashes(strip_tags($oA['religion'])); ?></td>
											<td><?php print stripslashes(strip_tags($oA['nationality'])); ?></td>											
											<td><?php print stripslashes(strip_tags($oA['dob'])); ?></td>
                                            <td><?php print stripslashes(strip_tags($oA['mobile'])); ?></td>
											<td><?php
				 $districtName = mysql_fetch_assoc(mysql_query("SELECT * FROM ce_districts WHERE dist_id = '".$oA['district']."'")); ?>
				<?php print stripslashes(strip_tags($districtName['dist_name'])); ?></td>
											<td>
											<?php
				 $cityName = mysql_fetch_assoc(mysql_query("SELECT * FROM ce_city WHERE city_id = '".$oA['emirate']."'")); ?>
				<?php print stripslashes(strip_tags($cityName['city_name'])); ?>
											</td>
											<td><?php print stripslashes(strip_tags($oA['employer'])); ?></td>
											<td><?php print stripslashes(strip_tags($oA['company'])); ?></td>
											<td><?php print stripslashes(strip_tags($oA['company_ref'])); ?></td>
											<td><?php print stripslashes(strip_tags($oA['position'])); ?></td>
											<td><?php print stripslashes(strip_tags($oA['nominees'])); ?></td>
											
											<td><?php print stripslashes(strip_tags($oA['referred_by'])); ?></td>
											<td><?php print stripslashes(strip_tags($oA['membership_start'])); ?></td>
											<td><?php print stripslashes(strip_tags($oA['membership_expiry'])); ?></td>
											<td><?php print stripslashes(strip_tags($oA['membership_type'])); ?></td>
											<td><?php print stripslashes(strip_tags($oA['fees'])); ?></td>
											<td><?php print stripslashes(strip_tags($oA['payment_method'])); ?></td>
																					
											 <td><?php 	$seconds = $oA['created_at'] / 1000;echo date("d/m/Y H:i:s", $seconds); ?></td>
											 <td><?php 	$seconds = $oA['updated_at'] / 1000;echo date("d/m/Y H:i:s", $seconds); ?></td>			
                                            <?php /*?><td>
                                 <?php print substr(stripslashes(strip_tags($oA['description'])),0, 30);
								if(strlen(stripslashes(strip_tags($oA['description'])))>30){?>
                                <a href="add_users.php?cid=<?php print $oA['id']; ?>">  View More...</a>
                                <?php } ?>
                                            </td><?php */?>
                                            <td class="center">
											<?php 
                                            switch($oA['status']){
                                            case 1:
                                            print 'Active';
                                            break;
                                            case 0:
                                            print '<font color="red">Inactive</font>';
                                            break;
                                            }
                                            ?>
                                            </td>
											
											<td>
												<div class="checkbox-container">
													<input type="checkbox" title="id" name="<?php print $oA['id']; ?>" class="custom-switch" 
													id="<?php print $oA['id']; ?>" value="applicants" <?php if($oA['approved']==1){?> checked <?php } ?> >	
												</div>
											</td>
											
                                            <td class="center">
                                           <!-- <a class="initialism fadeandscale_open btn btn-success say_it" href="#fadeandscale" id = <?php echo $oA["id"]; ?>>View </a>-->
										   <a class="btn btn-info" title="Edit" href="add_applicant.php?cid=<?php print $oA['id']; ?>">
														<i class="icon-edit icon-white"></i>  
														Edit                                            
														</a>
											<?php 
						  					 if($_SESSION['user_role']==1 || $_SESSION['user_role']==2){?>  
														  
														<a class="btn btn-danger delbutton" href="#" title="Delete" id = <?php echo $oA["id"]; ?>>
														<i class="icon-trash icon-white"></i> 
														Delete
													</a>
                                            <?php } ?>
											
											
                                          
                                            
                                            </td>
                                        </tr>
                                   
               <?php $i++;} ?>
               					 </tbody>
                                </table>
                            </div>
                            <!-- /.table-responsive -->
                            
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->

        </div>
        <!-- /#page-wrapper -->
<?php 
include_once("includes/footer.php");
?>
 <!-- DataTables JavaScript -->
    <script src="js/plugins/dataTables/jquery.dataTables.js"></script>
    <script src="js/plugins/dataTables/dataTables.bootstrap.js"></script>
    <script>
  $(document).ready(function() { 
    $('[data-toggle="tooltip"]').tooltip();  
       /// $('#dataTables-example').dataTable();
		 $('#dataTables-example').DataTable({
          "paging": true,
          "lengthChange": true,
          "searching": true,
          "ordering": true,
          "info": true,
          "autoWidth": true,
		  "scrollX": true
        });
    });
    </script>
	<style>
#dataTables-example {
    display: table;
    white-space: nowrap;
}
</style>