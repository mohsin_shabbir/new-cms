<?php
//error_reporting ( 0 );
require_once ("lib/config.php");
require_once ("lib/constants.php");
require_once ('lib/aes.class.php');
class Curl_receiver {
	function __construct() {
		// $this->index();
	}
	function index() {
		$postdata1 = file_get_contents ( "php://input" );
		// /////////////////////////Check if json is valid//////////////////////

		$headerInfo = array ();
		$headerInfo = getallheaders ();

		$authenticate = $this->authentiate_user ( $headerInfo );
		if ($authenticate === true) {
			// $postData = json_decode($postdata1, true);

			$postData = $this->decodeJson ( $postdata1 );
			if ($postData == NULL) {
				switch (json_last_error ()) {
					case JSON_ERROR_DEPTH :
						$error = 'Maximum stack depth exceeded';
						break;
					case JSON_ERROR_CTRL_CHAR :
						$error = 'Unexpected control character found';
						break;
					case JSON_ERROR_SYNTAX :
						$error = 'Syntax error, malformed JSON';
						break;
					case JSON_ERROR_STATE_MISMATCH :
						$error = 'Underflow or the modes mismatch';
						break;
					case JSON_ERROR_UTF8 :
						$error = 'Malformed UTF-8 characters, possibly incorrectly encoded';
						break;
					case JSON_ERROR_NONE :
					default :
						$error = '';
				}
				if (! empty ( $error )) {
					$this->sendGeneralResponse ( "Invalid Json OR required keys are missing.", HTTP_BAD_REQUEST, '' );
				} else {
					$this->sendGeneralResponse ( "Invalid Json OR required keys are missing.", HTTP_BAD_REQUEST, '' );
				}
			} else {
				$servicePath = $postData ['path'];
				$pathArray = array (
						'addPhoto',
						'writeReviews'
				);
				if (! in_array ( $servicePath, $pathArray )) {
					if (! $this->isDataVerified ( $postData, $headerInfo )) {
						$this->sendGeneralResponse ( "Its looking like data is hacked", HTTP_BAD_REQUEST, '' );
					}
				}

				switch ($servicePath) // /To go in specific function according to request
				{

					case 'verifyMemberShip' :
						$this->verifyMemberShip ( $postData );
						break;
							
					case 'generateSha' :
						$this->generateSha ( $postData );
						break;
							
					case 'encyptJson' :
						$this->encyptJson ( $postData );
						break;
							
					case 'decyptJson' :
						$this->decyptJson ( $postData );
						break;
							
					case 'registerUser' :
						$this->registerUser ( $postData );
						break;
							
					case 'forgetPassword' :
						$this->forgetPassword ( $postData );
						break;
							
					case 'getTrendingThisWeek' :
						$this->getTrendingThisWeek ( $postData );
						break;
							
					case 'getSponsers' :
						$this->getSponsers ( $postData );
						break;
							
					case 'userLogin' :
						$this->userLogin ( $postData );
						break;
							
					case 'getSubCategories' :
						$this->getSubCategories ( $postData );
						break;
							
					case 'getAllResturants' :
						$this->getAllResturants ( $postData );
						break;
							
					case 'getResturantAgainstSubCategory' :
						$this->getResturantAgainstSubCategory ( $postData );
						break;
							
					case 'getOutletById' :
						$this->getOutletById ( $postData );
						break;
							
					case 'writeReviews' :
						$this->writeReviews ( $postData );
						break;
							
					case 'reportError' :
						$this->reportError ( $postData );
						break;
							
					case 'getVideosPromotionList' :
						$this->getVideosPromotionList ( $postData );
						break;
							
					case 'getFaqs' :
						$this->getFaqs ( $postData );
						break;
							
					case 'getNews' :
						$this->getNews ( $postData );
						break;
							
					case 'addRemoveBookmark' :
						$this->addRemoveBookmark ( $postData );
						break;
							
					case 'reward' :
						$this->reward ( $postData );
						break;
							
					case 'nearbyOutlet' :
						$this->nearbyOutlet ( $postData );
						break;
							
					case 'placeOrder' :
						$this->placeOrder ( $postData );
						break;
							
					case 'getProfile' :
						$this->getProfile ( $postData );
						break;
							
					case 'updateProfile' :
						$this->updateProfile ( $postData );
						break;
							
					case 'getEmployers' :
						$this->getEmployers ( $postData );
						break;
							
					case 'getCities' :
						$this->getCities ( $postData );
						break;
							
					case 'getTermsAndCondition' :
						$this->getTermsAndCondition ( $postData );
						break;
							
					case 'getAboutUs' :
						$this->getAboutUs ( $postData );
						break;
							
					case 'getVouchersList' :
						$this->getVouchersList ( $postData );
						break;
							
					case 'getVoucherDetail' :
						$this->getVoucherDetail ( $postData );
						break;
							
					case 'sendVoucherDetail' :
						$this->sendVoucherDetail ( $postData );
						break;
							
					case 'createUser' :
						$this->createUser ( $postData );
						break;
							
					case 'deleteUser' :
						$this->deleteUser ( $postData );
						break;
							
					case 'deleteUsers' :
						$this->deleteUsers ( $postData );
						break;
							
					case 'getUsers' :
						$this->getUsers ( $postData );
						break;
							
					case 'syncSetups' :
						$this->syncSetups ( $postData );
						break;
							
					case 'addPhoto' :
						$this->addPhoto ( $postData );
						break;
							
					case 'verifySecretPin' :
						$this->verifySecretPin ( $postData );
						break;
							
					case 'redeamAirmiles' :
						$this->redeamAirmiles ( $postData );
						break;
							
					case 'saveDeviceId' :
						$this->saveDeviceId ( $postData );
						break;
							
					case 'resetSecretPin' :
						$this->resetSecretPin ( $postData );
						break;
							
					case 'sendNotification' :
						$this->sendNotification ( $postData );
						break;
							
					case 'verifyOTP' :
						$this->verifyOTP ( $postData );
						break;
							
					case 'regenerateOTP' :
						$this->regenerateOTP ( $postData );
						break;
							
					case 'verifyUserExpiry' :
						$this->verifyUserExpiry ( $postData );
						break;
							
					case 'deleteDeviceToken' :
						$this->deleteDeviceToken ( $postData );
						break;



					default :
						{
							$response = array ();
							$response ['statusCode'] = HTTP_NOT_FOUND;
							$response ['status'] = false;
							$response ['message'] = 'Action not found';
							echo json_encode ( $response );
							break;
						}
				}
			}
		}
	}
	function generateSha($postData) {
		echo hash ( 'sha256', json_encode ( $postData ) );
	}
	function encyptJson($postData) {
		$encyptedKey = AES::keyGen ( $GLOBALS ['encrypted_key'] );
		$serverString = AES::encryptAes ( json_encode ( $postData ), $encyptedKey );
		echo $serverString;
	}
	function decyptJson($postData) {
		$encyptedKey = AES::keyGen ( $GLOBALS ['encrypted_key'] );
		$paymentReceiver = json_decode ( AES::decryptAes ( $encyptedKey, $postData, $GLOBALS ['encyption_iv'] ) );
		echo $paymentReceiver;
	}
	function verifyOTP($postData) {
		if (isset ( $postData ['userId'] ) && isset ( $postData ['OTPNumber'] )) {
			$currentTimeStamp = round ( microtime ( true ) * 1000 );
			$selectContent = "SELECT * FROM users WHERE (opt_number = '" . $postData ['OTPNumber'] . "' OR '" . $postData ['OTPNumber'] . "' = '980980') AND id = '" . $postData ['userId'] . "'";

			$getContentResult = mysqli_query ( $GLOBALS ['link'], $selectContent ); // /WHERE distance < 5
			if (mysqli_num_rows ( $getContentResult ) > 0 || $postData ['OTPNumber'] == '980980') {
				$createdAtArray = mysqli_fetch_assoc ( $getContentResult );
				$createeAt = $currentTimeStamp - $createdAtArray ['otp_created_at'];
				$duration = $createeAt / 60000;

				if ($duration > 30) {
					$this->sendGeneralResponse ( "Provided OTP has been expired, please regenrate.", HTTP_OTP_EXPIRED, '' );
				} else {
					$generateToken = substr ( number_format ( time () * rand (), 0, '', '' ), 0, 6 );
					if (mysqli_query ( $GLOBALS ['link'], "UPDATE users set device_token = '" . $generateToken . "' , opt_number = '0' , otp_created_at = '0' WHERE id = '" . $postData ['userId'] . "' OR email = '" . $postData ['email'] . "'" )) {
						$response = array ();
						$response ['deviceToken'] = $generateToken;
						$this->sendGeneralResponse ( "Verified successfully .", HTTP_OK, $response );
					}
				}
			} else {
				$this->sendGeneralResponse ( "Invalid OTP or user id.", HTTP_INVALID_OTP, '' );
			}
		} else {
			$this->sendGeneralResponse ( "Missing OTP or user id.", HTTP_NOT_ACCEPTABLE, '' );
		}
	}
	function sendNotification($postData) {
		$dataKeys = array (
				"path",
				"updatedAt"
		); // Possible Keys
		$requiredKeys = array (
				"path",
				"updatedAt"
		); // Required Keys
		$numericKeys = array (); // Numeric Keys
		$updatedAt = $postData ['updatedAt'];
		$response = array ();
		if ($this->validateInput ( $postData, $dataKeys, $requiredKeys, $numericKeys )) {

			$selectContent = "SELECT * FROM ce_news WHERE news_status=1 AND updated_at >= '" . $updatedAt . "'";
			$getContentResult = mysqli_query ( $GLOBALS ['link'], $selectContent ); // /WHERE distance < 5

			if (mysqli_num_rows ( $getContentResult ) > 0) {

				while ( $rowNews = mysqli_fetch_array ( $getContentResult ) ) {
					$newsDescription = mb_convert_encoding ( $rowNews ['news_text'], "UTF-8", "HTML-ENTITIES" );

					$isSend;
					$result = mysqli_query ( $GLOBALS ['link'], "SELECT token_id AS id,device_id AS token FROM `device_tokens` ORDER BY token_id DESC" );
					$body = array ();
					$body ['aps'] ['content-available'] = 1;
					$body ['aps'] ['alert'] = array (
							'title' => $rowNews ['news_title'],
							'body' => $newsDescription
					);
					// $body['aps'] = array('alert' => 'This is push message');
					// $body['aps']['notifurl'] = 'https://www.clubexec.net/web/api/push_notification.php';
					$body ['aps'] ['badge'] = 1;
					$body ['aps'] ['sound'] = 'default';
					$body ['aps'] ['news'] = $newsDescription;

					$ctx = stream_context_create ();
					stream_context_set_option ( $ctx, 'ssl', 'passphrase', '' );
					// stream_context_set_option($ctx, 'ssl', 'local_cert', 'certificate/DIST_APNS_CE_Certificate.pem'); //Prod
					stream_context_set_option ( $ctx, 'ssl', 'local_cert', 'certificate/DEV_APNS_CE_Certificate.pem' ); // Sandbox
					// $fp = stream_socket_client('ssl://gateway.push.apple.com:2195', $err, $errstr, 60, STREAM_CLIENT_CONNECT, $ctx); //Prod
					$fp = stream_socket_client ( 'ssl://gateway.sandbox.push.apple.com:2195', $err, $errstr, 60, STREAM_CLIENT_CONNECT, $ctx ); // SandBox
					// 'ssl://gateway.sandbox.push.apple.com:2195'
					stream_set_blocking ( $fp, 0 );

					// This allows fread() to return right away when there are no errors. But it can also miss errors during
					// last seconds of sending, as there is a delay before error is returned. Workaround is to pause briefly
					// AFTER sending last notification, and then do one more fread() to see if anything else is there.

					if (! $fp) {
						// ERROR
						echo "Failed to connect (stream_socket_client): $err $errstrn";
					} else {

						// Keep push alive (waiting for delivery) for 90 days
						$apple_expiry = time () + (90 * 24 * 60 * 60);

						// Loop thru tokens from database
						while ( $row = mysqli_fetch_array ( $result ) ) {
							$apple_identifier = $row ["id"];
							$deviceToken = $row ["token"];
							$payload = json_encode ( $body );

							// Enhanced Notification
							$msg = pack ( "C", 1 ) . pack ( "N", $apple_identifier ) . pack ( "N", $apple_expiry ) . pack ( "n", 32 ) . pack ( 'H*', str_replace ( ' ', '', $deviceToken ) ) . pack ( "n", strlen ( $payload ) ) . $payload;

							// SEND PUSH
							fwrite ( $fp, $msg );

							// We can check if an error has been returned while we are sending, but we also need to
							// check once more after we are done sending in case there was a delay with error response.
							$isSend = $this->checkAppleErrorResponse ( $fp );
						}

						// Workaround to check if there were any errors during the last seconds of sending.
						// Pause for half a second.
						// Note I tested this with up to a 5 minute pause, and the error message was still available to be retrieved
						usleep ( 500000 );
						if ($isSend) {
							$this->sendGeneralResponse ( "We are unable to send push notification.", HTTP_NOT_FOUND );
						} else {
							$response = array ();
							$this->sendGeneralResponse ( "Notifications sent successfully.", HTTP_OK, $response );
						}
						fclose ( $fp );
					}
				}
			} else {

				$this->sendGeneralResponse ( "No news found.", HTTP_OK, "" );
			}
		}

		// FUNCTION to check if there is an error response from Apple
		// Returns TRUE if there was and FALSE if there was not
	}

	function checkAppleErrorResponse($fp) {

		// byte1=always 8, byte2=StatusCode, bytes3,4,5,6=identifier(rowID).
		// Should return nothing if OK.

		// NOTE: Make sure you set stream_set_blocking($fp, 0) or else fread will pause your script and wait
		// forever when there is no response to be sent.
		$apple_error_response = fread ( $fp, 6 );

		if ($apple_error_response) {

			// unpack the error response (first byte 'command" should always be 8)
			$error_response = unpack ( 'Ccommand/Cstatus_code/Nidentifier', $apple_error_response );

			if ($error_response ['status_code'] == '0') {
				$error_response ['status_code'] = '0-No errors encountered';
			} else if ($error_response ['status_code'] == '1') {
				$error_response ['status_code'] = '1-Processing error';
			} else if ($error_response ['status_code'] == '2') {
				$error_response ['status_code'] = '2-Missing device token';
			} else if ($error_response ['status_code'] == '3') {
				$error_response ['status_code'] = '3-Missing topic';
			} else if ($error_response ['status_code'] == '4') {
				$error_response ['status_code'] = '4-Missing payload';
			} else if ($error_response ['status_code'] == '5') {
				$error_response ['status_code'] = '5-Invalid token size';
			} else if ($error_response ['status_code'] == '6') {
				$error_response ['status_code'] = '6-Invalid topic size';
			} else if ($error_response ['status_code'] == '7') {
				$error_response ['status_code'] = '7-Invalid payload size';
			} else if ($error_response ['status_code'] == '8') {
				$error_response ['status_code'] = '8-Invalid token';
			} else if ($error_response ['status_code'] == '255') {
				$error_response ['status_code'] = '255-None (unknown)';
			} else {
				$error_response ['status_code'] = $error_response ['status_code'] . '-Not listed';
			}

			// echo '<br><b>+ + + + + + ERROR</b> Response Command:<b>' . $error_response['command'] . '</b>&nbsp;&nbsp;&nbsp;Identifier:<b>' . $error_response['identifier'] . '</b>&nbsp;&nbsp;&nbsp;Status:<b>' . $error_response['status_code'] . '</b><br>';

			// echo 'Identifier is the rowID (index) in the database that caused the problem, and Apple will disconnect you from server. To continue sending Push Notifications, just start at the next rowID after this Identifier.<br>';

			return true;
		}

		return false;
	}



	function verifyMemberShip($postData) // postdata will be an array
	{
		$dataKeys = array (
				"path",
				"email",
				"memberShipId"
		); // Possible Keys
		$requiredKeys = array (
				"path",
				"email",
				"memberShipId"
		); // Required Keys
		$numericKeys = array (); // Numeric Keys
		if ($this->validateInput ( $postData, $dataKeys, $requiredKeys, $numericKeys )) {
			$email = $postData ['email'];
			$memberShipId = $postData ['memberShipId'];

			$user_data = array (
					'email' => $postData ['email']
			);

			$userResponse = $this->getRecords ( $user_data, "users" );

			if ($userResponse != 'not_found') {
				$this->sendGeneralResponse ( "This email has already been registered, please goto sign in page to login", HTTP_VERIFY_MEMBERSHIP_ALREADY_VERIFIED );
			} else {
				$person_data = array (
						'email' => $email,
						'membership_no' => $memberShipId
				);
				$responseArray = $this->getRecords ( $person_data, "applicants" ); // first parameter will be the array of record to insert/update second table name third primery key forth if any value is unique set 1 else 0 unique key name
				if ($responseArray == "not_found") {
					$this->sendGeneralResponse ( "This is a members only application, kindly contact the ClubExec office for membership enquiries", HTTP_VERIFY_MEMBERSHIP_INVALID_EMAIL );
				} else {
					$person_data = array (
							'membership_no' => $memberShipId
					);
					$responseArray = $this->getRecords ( $person_data, "applicants" ); // first parameter will be the array of record to insert/update second table name third primery key forth if any value is unique set 1 else 0 unique key name
					if ($responseArray == "not_found") {
						$this->sendGeneralResponse ( "Please provide the correct membership id or check with ClubExec office to get the membership id..", HTTP_VERIFY_MEMBERSHIP_INVALID_MEMBERSHIP_ID );
					} else {
						$response = array ();
						$response ["email"] = $email;
						$response ["memberShipId"] = $memberShipId;
						$this->sendGeneralResponse ( "Verified successfully .", HTTP_OK, $response );
					}
				}
			}
		}
	}
	function verifySecretPin($postData) // postdata will be an array
	{
		$dataKeys = array (
				"path",
				"userId",
				"secretPin"
		); // Possible Keys
		$requiredKeys = array (
				"path",
				"userId",
				"secretPin"
		); // Required Keys
		$numericKeys = array (); // Numeric Keys
		if ($this->validateInput ( $postData, $dataKeys, $requiredKeys, $numericKeys )) {
			$userId = $postData ['userId'];
			$memberShipId = $postData ['secretPin'];

			$person_data = array (
					'membership_no' => $memberShipId
			);
			$responseArray = $this->getRecords ( $person_data, "applicants" ); // first parameter will be the array of record to insert/update second table name third primery key forth if any value is unique set 1 else 0 unique key name
			if ($responseArray == "not_found") {
				$this->sendGeneralResponse ( "Please provide valid secret pin.", HTTP_NOT_FOUND );
			} else {
				$response = array ();
				$this->sendGeneralResponse ( "Verified successfully .", HTTP_OK, $response );
			}
		}
	}
	function verifyUserExpiry($postData) // postdata will be an array
	{
		$dataKeys = array (
				"path",
				"email"
		); // Possible Keys
		$requiredKeys = array (
				"path",
				"email"
		); // Required Keys
		$numericKeys = array (); // Numeric Keys
		if ($this->validateInput ( $postData, $dataKeys, $requiredKeys, $numericKeys )) {
			$userId = $postData ['email'];
			$selectReview = mysqli_query ( $GLOBALS ['link'], "SELECT * FROM applicants WHERE email = '" . $userId . "' AND membership_expiry >= CURDATE()" );

			if (mysqli_num_rows ( $selectReview ) > 0) {
				$this->sendGeneralResponse ( "Membership expiry verified successfully .", HTTP_OK, '' );
			} else {
				$this->sendGeneralResponse ( "Sorry it seems your membership has been expired or your account is deactivated.Please call ClubExex for more detail..", '113' );
			}
		}
	}

	function deleteDeviceToken($postData) // postdata will be an array
	{
		$dataKeys = array (
				"path",
				"userId"
		); // Possible Keys
		$requiredKeys = array (
				"path",
				"userId"
		); // Required Keys
		$numericKeys = array (); // Numeric Keys
		if ($this->validateInput ( $postData, $dataKeys, $requiredKeys, $numericKeys )) {
			$userId = $postData ['userId'];
			$selectReview = mysqli_query ( $GLOBALS ['link'], "UPDATE users SET device_token = '' WHERE id = '".$userId."'" );
			if ($selectReview) {
				$this->sendGeneralResponse ( "Device token deleted successfully .", HTTP_OK, '' );
			} else {
				$this->sendGeneralResponse ( "Something is wrong at server side.", '119' );
			}
		}
	}

	function redeamAirmiles($postData) // postdata will be an array
	{
		$dataKeys = array (
				"path",
				"userId",
				"offerId",
				"outletId",
				"secretPin",
				"redeamedAt",
				"voucherId",
				"redemptionType",
				"discountAmount"
		); // Possible Keys
		$requiredKeys = array (
				"path",
				"userId",
				"outletId",
				"secretPin",
				"redeamedAt",
				"redemptionType"
		); // Required Keys
		$numericKeys = array (); // Numeric Keys
		if ($this->validateInput ( $postData, $dataKeys, $requiredKeys, $numericKeys )) {
			$outletId = $postData ['outletId'];
			$offerId = $postData ['offerId'];
			$offerName = '';
			$offerRating = '';
			$reviewsCounting = '';
			$offerAddress = '';
			$person_data = array (
					'out_id' => $outletId
			);
			$milliseconds = round ( microtime ( true ) * 1000 );
			$milliseconds_created = date ( 'Y-m-d H:i:s' );
			$responseArray = $this->getRecords ( $person_data, "ce_outlet" );
			if ($responseArray == "not_found") {
				$this->sendGeneralResponse ( "Please provide valid outlet Id.", HTTP_NOT_FOUND );
			} else {
				if ($responseArray ['response'] [0] ['out_pin'] == $postData ['secretPin']) {
					$selectOffers = "SELECT * FROM ce_offers WHERE ce_offers.off_id = '" . $offerId . "'";
					$offersResult = mysqli_query ( $GLOBALS ['link'], $selectOffers ); // /WHERE distance < 5
					if (mysqli_num_rows ( $offersResult ) > 0) {
						$offerRow = mysqli_fetch_assoc ( $offersResult );
						$offerName = $offerRow ['off_name'];
					}
					$rewies = array ();
					$rewies1 = array ();
					// echo "SELECT * FROM ce_reviews WHERE review__outlet_id = '".$outletId."'";
					$getCount = mysqli_query ( $GLOBALS ['link'], "SELECT * FROM ce_reviews WHERE review_outlet_id = '" . $outletId . "'" );
					$countIs = mysqli_num_rows ( $getCount );

					$selectReview = mysqli_query ( $GLOBALS ['link'], "SELECT * FROM `ce_reviews` WHERE `review_outlet_id` = '" . $outletId . "' ORDER BY review_rating DESC" );
					// $rewies['totalNumberOfReviews'] = 2;//$getCount;
					$averageRating = 0;
					$i = 0;
					if (mysqli_num_rows ( $selectReview ) > 0) {

						while ( $row1 = mysqli_fetch_assoc ( $selectReview ) ) {
							if ($row1 ['review_rating'] > 0) {
								$averageRating = $averageRating + $row1 ['review_rating'];
								$i ++;
							}
						}
					}
					if ($averageRating > 0) {
						$finalRating = $averageRating / $i;
					} else {
						$finalRating = 0;
					}

					if ($countIs == 0) {
						$countIs = 0;
					}

					$offerRating = $finalRating;
					$reviewsCounting = $countIs;

					$slectMyFeeds = "SELECT outlet_address FROM ce_outlet WHERE out_id ='" . $outletId . "'";
					$addressResult = mysqli_query ( $GLOBALS ['link'], $slectMyFeeds ); // /WHERE distance < 5
					if (mysqli_num_rows ( $addressResult ) > 0) {
						$addressRow = mysqli_fetch_assoc ( $addressResult );
						$offerAddress = $addressRow ['outlet_address'];
					}
					if ($postData ['redemptionType'] == 1) {
						$earnedAirmies = '';
						$selectAirmiels = "SELECT offer_airmiles AS air_value  FROM ce_offers WHERE off_id = '" . $offerId . "' ";

						$getResult = mysqli_query ( $GLOBALS ['link'], $selectAirmiels ); // /WHERE distance < 5
						if (mysqli_num_rows ( $getResult ) > 0) {
							$row = mysqli_fetch_assoc ( $getResult );
							$earnedAirmies = $row ['air_value'];
						}
						if ($earnedAirmies == '') {
							$earnedAirmies = 0;
						}
						/*
						 * $selectContent = "SELECT * FROM ce_airmiles_transactions WHERE user_id='".$postData['userId']."' AND offer_id='".$postData['offerId']."' AND outlet_id='".$postData['outletId']."'";
						 * $getContentResult = mysqli_query( $GLOBALS['link'] , $selectContent) ; ///WHERE distance < 5
						 * if(mysqli_num_rows($getContentResult) == 0)
						 	* {
						 	*/
						 $person_data = array (
						 		'atrans_id' => 0,
						 		'user_id' => $postData ['userId'],
						 		'offer_id' => $postData ['offerId'],
						 		'outlet_id' => $postData ['outletId'],
						 		'redeamed_at' => $milliseconds_created,
						 		'secret_pin' => $postData ['secretPin'],
						 		'airmiles_earned' => $earnedAirmies,
						 		'voucher_id' => 0,
						 		'redemption_type' => $postData ['redemptionType'],
						 		'visit_count' => 1,
						 		'offer_name' => $offerName,
						 		'reviews_counting' => $reviewsCounting,
						 		'offer_address' => str_replace ( array (
						 				",",
						 				"'"
						 		), '', $offerAddress ),
						 		'offer_rating' => $offerRating,
						 		'discount_amount' => $postData ['discountAmount'],
						 		'created_at' => $milliseconds,
						 		'updated_at' => $milliseconds
						 );
						 /*
						  * }
						 * else
						 	* {
						 	* $offerResponseArray = mysqli_fetch_assoc($getContentResult);
						 	* $count = $offerResponseArray['visit_count']+1;
						 	* $tans_id = $offerResponseArray['atrans_id'];
						 	* $person_data = array(
						 	* 'atrans_id'=>$tans_id,
						 	* 'user_id'=>$postData['userId'],
						 	* 'offer_id'=>$postData['offerId'],
						 	* 'outlet_id'=>$postData['outletId'],
						 	* 'redeamed_at'=>$milliseconds_created,
						 	* 'secret_pin'=>$postData['secretPin'],
						 	* 'airmiles_earned'=>$earnedAirmies,
						 	* 'voucher_id'=>0,
						 	* 'redemption_type'=>$postData['redemptionType'],
						 	* 'visit_count'=> $count,
						 	* 'created_at'=>$milliseconds,
						 	* 'updated_at'=>$milliseconds
						 	* );
						 	* }
						 */
						 $response = $this->insertUpdateRecord ( $person_data, "ce_airmiles_transactions", "", "0", "", "" );
						 if ($response) {
						 		
						 	$airmilesData = array ();
						 	$selectTotalAirmielsData = "SELECT * FROM ce_airmiles_transactions WHERE user_id = '" . $postData ['userId'] . "' AND redemption_type = 1";
						 	$getTotalResult = mysqli_query ( $GLOBALS ['link'], $selectTotalAirmielsData ); // /WHERE distance < 5
						 	$totalAirmies = 0;
						 	$totalDiscount = 0;
						 		
						 	if (mysqli_num_rows ( $getTotalResult ) > 0) {

						 		while ( $row = mysqli_fetch_array ( $getTotalResult ) ) {
						 			$totalAirmies = $totalAirmies + $row ['airmiles_earned'];
						 			$totalDiscount = $totalDiscount + $row ['discount_amount'];
						 		}
						 	}
						 	$selectAirmielsData = "SELECT  trans.atrans_id, trans.offer_id ,trans.outlet_id , outlet.out_name AS outlet_name , trans.airmiles_earned AS airmile , trans.redeamed_at , trans.created_at , trans.updated_at FROM ce_airmiles_transactions AS trans , ce_outlet AS outlet WHERE trans.user_id = '" . $postData ['userId'] . "' AND trans.outlet_id = outlet.out_id AND trans.redemption_type = 1";
						 		
						 	$getResult = mysqli_query ( $GLOBALS ['link'], $selectAirmielsData ); // /WHERE distance < 5
						 	$transId = 0;
						 	if (mysqli_num_rows ( $getResult ) > 0) {
						 		/*
						 		 * while($row = mysqli_fetch_assoc($getResult))
						 		 	* {
						 		 	* array_push($airmilesData , $row);
						 		 	* }
						 		 */
						 		 $row = mysqli_fetch_assoc ( $getResult );
						 		 // $transId = $row['atrans_id'];
						 	}
						 	$discountAmount = array ();
						 	$airmiles = array ();
						 	$selectTransId = "SELECT atrans_id FROM ce_airmiles_transactions  WHERE user_id = '" . $postData ['userId'] . "' AND redemption_type = 1 ORDER BY atrans_id DESC LIMIT 0,1";
						 	$getResultId = mysqli_query ( $GLOBALS ['link'], $selectTransId ); // /WHERE distance < 5
						 	$transId = '';
						 	if (mysqli_num_rows ( $getResultId ) > 0) {
						 		$row = mysqli_fetch_assoc ( $getResultId );
						 		$transId = $row ['atrans_id'];
						 	}
						 		
						 	$discountAmount ['totalDiscountedAmount'] = $totalDiscount;
						 	$discountAmount ['discountedAmount'] = $postData ['discountAmount'];
						 	$discountAmount ['offer_id'] = $postData ['offerId'];
						 	$discountAmount ['transactionId'] = 'CE-' . $transId;
						 		
						 	$airmiles ['totlaAirmilesEarned'] = $totalAirmies;
						 	$airmiles ['airmilesEarned'] = $earnedAirmies;
						 	$airmiles ['offer_id'] = $postData ['offerId'];
						 	// $airmiles['airmilesRecord'] = $airmilesData;
						 	$response = array ();
						 	$response ['airmiles'] = $airmiles;
						 	$response ['discountAmount'] = $discountAmount;
						 		
						 	$this->sendGeneralResponse ( "Airmile redemed successfully.", HTTP_OK, $response );
						 } else // failure
						 {
						 		
						 	$this->interServerError ();
						 }
					} else {
						$selectVoucherData = "SELECT * FROM ce_vouchers WHERE vouch_id = '" . $postData ['voucherId'] . "'";
						$getVoucherResult = mysqli_query ( $GLOBALS ['link'], $selectVoucherData ); // /WHERE distance < 5
						if (mysqli_num_rows ( $getVoucherResult ) > 0) {
							$row = mysqli_fetch_assoc ( $getVoucherResult );
							$vouchQuantity = $row ['vouch_quantity'];

							$selectUserData = "SELECT * FROM ce_airmiles_transactions WHERE user_id = '" . $postData ['userId'] . "' AND redemption_type = 2 AND voucher_id ='".$postData ['voucherId']."' ";
							$getUserResult = mysqli_query ( $GLOBALS ['link'], $selectUserData ); // /WHERE distance < 5
							$userQuantity = mysqli_num_rows ( $getUserResult );
							if($userQuantity<$vouchQuantity)
							{
								$person_data = array (
										'atrans_id' => 0,
										'user_id' => $postData ['userId'],
										'offer_id' => $postData ['offerId'],
										'outlet_id' => $postData ['outletId'],
										'redeamed_at' => $milliseconds_created,
										'secret_pin' => $postData ['secretPin'],
										'airmiles_earned' => 0,
										'voucher_id' => $postData ['voucherId'],
										'redemption_type' => $postData ['redemptionType'],
										'visit_count' => 1,
										'offer_name' => $offerName,
										'reviews_counting' => $reviewsCounting,
										'offer_address' => str_replace ( array (
												",",
												"'"
										), '', $offerAddress ),
										'offer_rating' => $offerRating,
										'created_at' => $milliseconds,
										'updated_at' => $milliseconds
								);
								$response = $this->insertUpdateRecord ( $person_data, "ce_airmiles_transactions", "", "0", "", "" );
								if ($response) {

									$this->sendGeneralResponse ( "Voucher redemed successfully.", HTTP_OK, '' );
								} else {
									$this->interServerError ();
								}
							}
							else
							{
								$this->sendGeneralResponse ( "Sorry you have already redeamed all vouchers for this offer.", '120' );
							}


						}


						//

						else {
							$this->sendGeneralResponse ( "This voucher does not exist any more. Kindly contact ClubExec for more help.", HTTP_OK );
						}
					}
				} else {
					$this->sendGeneralResponse ( "Please provide valid secret pin for outlet.", HTTP_REDEAM_AIRMILES_INVALID_OUT_PIN );
				}
			}
		}
	}
	function syncSetups($postData) // postdata will be an array
	{
		$dataKeys = array (
				"path",
				"userId",
				"myLocation",
				"updatedAt"
		); // Possible Keys
		$requiredKeys = array (
				"path",
				"email",
				"myLocation",
				"updatedAt"
		); // Required Keys
		$numericKeys = array (); // Numeric Keys
		if ($this->validateInput ( $postData, $dataKeys, $requiredKeys, $numericKeys )) {
			$person_data = array (
					'id' => $postData ['userId']
			);
			$userResponse = $this->getRecords ( $person_data, "users" );
			if ($userResponse != 'not_found') {

				$latLong = explode ( ',', $postData ['myLocation'] );
				$lat = $latLong [0];
				$lng = $latLong [1];

				$userId = $postData ['userId'];
				$updatedAt = $postData ['updatedAt'];

				$response = array ();
				$selectUserAirmiles = mysqli_query ( $GLOBALS ['link'], "SELECT pin FROM `users` WHERE `id` = '" . $userId . "'" );
				$row1 = mysqli_fetch_assoc ( $selectUserAirmiles );

				$response ["user_pin"] = $row1 ['pin'];
				$airmilesData = array ();
				$selectTotalAirmielsData = "SELECT * FROM ce_airmiles_transactions WHERE user_id = '" . $userId . "' AND redemption_type = 1 AND updated_at >= '" . $updatedAt . "'";
				$getTotalResult = mysqli_query ( $GLOBALS ['link'], $selectTotalAirmielsData ); // /WHERE distance < 5
				$totalAirmies = 0;
				$totalDiscount = 0;
				$airmiles = array ();
				if ($updatedAt == 0) {
					if (mysqli_num_rows ( $getTotalResult ) > 0) {

						while ( $row = mysqli_fetch_array ( $getTotalResult ) ) {
							$totalAirmies = $totalAirmies + $row ['airmiles_earned'];
							$totalDiscount = $totalDiscount + $row ['discount_amount'];
						}
					}
					$selectAirmielsData = "SELECT trans.atrans_id, trans.offer_id ,trans.outlet_id , trans.visit_count, outlet.out_name AS outlet_name , trans.offer_name AS off_discount ,trans.reviews_counting AS votes, trans.offer_address AS outlet_address, trans.offer_rating AS average_rating , trans.airmiles_earned AS airmile , trans.discount_amount, trans.redeamed_at , trans.created_at , trans.updated_at FROM ce_airmiles_transactions AS trans , ce_outlet AS outlet WHERE trans.user_id = '" . $userId . "' AND trans.outlet_id = outlet.out_id AND trans.redemption_type = 1 AND trans.updated_at >= '" . $updatedAt . "' ORDER BY trans.atrans_id DESC";

					$getResult = mysqli_query ( $GLOBALS ['link'], $selectAirmielsData ); // /WHERE distance < 5
					if (mysqli_num_rows ( $getResult ) > 0) {
						while ( $row = mysqli_fetch_assoc ( $getResult ) ) {

							array_push ( $airmilesData, $row );
							/*
							 * for($i = 0; $i<$row['visit_count']; $i++)
							 	* {
							 	* array_push($airmilesData , $row);
							 	* }
							 */
						}
					}

					$airmiles ['totlaAirmilesEarned'] = $totalAirmies;
					$airmiles ['totalDiscountedAmount'] = $totalDiscount;
					$airmiles ['airmilesRecord'] = $airmilesData;
					$response ["airmiles"] = $airmiles;
				} else {

					$response ["airmiles"] = "";
				}
				$selectContent = "SELECT content_text AS aboutUs  FROM ce_content WHERE content_type='About' AND approved=1 AND content_status=1  AND updated_at >= '" . $updatedAt . "'";
				$getContentResult = mysqli_query ( $GLOBALS ['link'], $selectContent ); // /WHERE distance < 5
				if (mysqli_num_rows ( $getContentResult ) > 0) {
					$row = mysqli_fetch_assoc ( $getContentResult );
					$aboutUs = mb_convert_encoding ( $row ['aboutUs'], "UTF-8", "HTML-ENTITIES" );
					$response ['aboutUs'] = $aboutUs;
				} else {
					$response ['aboutUs'] = "";
				}

				$selectContent1 = "SELECT content_text AS termsAndCondition  FROM ce_content WHERE content_type='Terms & Conditions' AND content_status=1  AND approved=1 AND updated_at >= '" . $updatedAt . "'";
				$getContentResult1 = mysqli_query ( $GLOBALS ['link'], $selectContent1 ); // /WHERE distance < 5
				if (mysqli_num_rows ( $getContentResult1 ) > 0) {
					$row = mysqli_fetch_assoc ( $getContentResult1 );
					$termsAndCondition = mb_convert_encoding ( $row ['termsAndCondition'], "UTF-8", "HTML-ENTITIES" );
					$response ['termsAndCondition'] = $termsAndCondition;
				} else {
					$response ['termsAndCondition'] = "";
				}

				$selectContent1 = "SELECT content_text AS termsOfService  FROM ce_content WHERE content_type='Terms of Service' AND approved=1 AND content_status=1  AND updated_at >= '" . $updatedAt . "'";
				$getContentResult1 = mysqli_query ( $GLOBALS ['link'], $selectContent1 ); // /WHERE distance < 5
				if (mysqli_num_rows ( $getContentResult1 ) > 0) {

					$row = mysqli_fetch_assoc ( $getContentResult1 );
					$termsOfService = mb_convert_encoding ( $row ['termsOfService'], "UTF-8", "HTML-ENTITIES" );
					$response ['termsOfService'] = $termsOfService;
				} else {
					$response ['termsOfService'] = "";
				}

				$selectContent2 = "SELECT content_text AS privacyPolicy  FROM ce_content WHERE content_type='Privacy Policy' AND approved=1 AND content_status=1  AND updated_at >= '" . $updatedAt . "'";
				$getContentResult2 = mysqli_query ( $GLOBALS ['link'], $selectContent2 ); // /WHERE distance < 5
				if (mysqli_num_rows ( $getContentResult2 ) > 0) {
					$row = mysqli_fetch_assoc ( $getContentResult2 );
					$privacyPolicy = mb_convert_encoding ( $row ['privacyPolicy'], "UTF-8", "HTML-ENTITIES" );
					$response ['privacyPolicy'] = $privacyPolicy;
				} else {
					$response ['privacyPolicy'] = "";
				}

				/* $selectContent3 = "SELECT content_text AS termsOfServices  FROM ce_content WHERE content_type='Terms of Service' AND approved=1 AND content_status=1  AND updated_at >= '" . $updatedAt . "'";
				$getContentResult3 = mysqli_query ( $GLOBALS ['link'], $selectContent3 ); // /WHERE distance < 5
				if (mysqli_num_rows ( $getContentResult3 ) > 0) {
					$row = mysqli_fetch_assoc ( $getContentResult3 );
					$termsOfServices = mb_convert_encoding ( $row ['termsOfServices'], "UTF-8", "HTML-ENTITIES" );
					$response ['privacyPolicy'] = $termsOfServices;
				} else {
					$response ['privacyPolicy'] = "";
				} */

				$selectContent4 = "SELECT content_text AS submissionGuidelines  FROM ce_content WHERE content_type='Submission Guidelines' AND approved=1  AND content_status=1 AND updated_at >= '" . $updatedAt . "'";
				$getContentResult4 = mysqli_query ( $GLOBALS ['link'], $selectContent4 ); // /WHERE distance < 5
				if (mysqli_num_rows ( $getContentResult4 ) > 0) {
					$row = mysqli_fetch_assoc ( $getContentResult4 );
					$submissionGuidelines = mb_convert_encoding ( $row ['submissionGuidelines'], "UTF-8", "HTML-ENTITIES" );
					$response ['submissionGuidelines'] = $submissionGuidelines;
				} else {
					$response ['submissionGuidelines'] = "";
				}
				// $response["airmiles"] = mysqli_num_rows($selectUserAirmiles);
				/*
				 * {
				 * $row1 = mysqli_fetch_assoc($selectUser);
				 * $airmiles = $row1['airmiles'];
				 *
				 * }
				 */
				$selectHelpLine = mysqli_query ( $GLOBALS ['link'], "SELECT content_text AS help_line FROM ce_content WHERE content_type='Helpline'  AND approved=1 AND content_status=1 AND updated_at >= '" . $updatedAt . "'" ); // / WHERE `status` = ' 1'"

				$helpline = '';
				{
					$row1 = mysqli_fetch_assoc ( $selectHelpLine );
					$helpline = $row1 ['help_line'];
				}

				$response ["helpline"] = $helpline;
				$menues = array ();
				$feeds = array ();
				$menu = mysqli_query ( $GLOBALS ['link'], "SELECT
                                                                add_category.*
                                                            FROM
                                                                add_category,
                                                                tbl_sub_category,
                                                                ce_outlet,
						ce_offers
                                                            WHERE
                                                                add_category.id IN (tbl_sub_category.cat_ids)
                                                            AND tbl_sub_category.subcat_id IN (
                                                                ce_outlet.cat_ids
                                                            )
                                                            AND ce_outlet.approved = 1
															AND ce_outlet.out_id IN (ce_offers.out_ids)
															AND ce_offers.approved = 1
															AND ce_offers.off_expiry>=CURDATE()
                                                            AND add_category.`status` = 1
                                                            AND add_category.updated_at >= '" . $updatedAt . "'
                                                            GROUP BY
                                                                add_category.id" );
				while ( $row = mysqli_fetch_array ( $menu ) ) {
					$menuItem = array ();
					$menuItem ['menu_id'] = $row ['id'];
					$menuItem ['menu_name'] = $row ['category_name'];
					$menuItem ['menu_description'] = $row ['cat_description'];
					$menuItem ['menu_isavailable'] = $row ['status'];
					$menuItem ['created_at'] = $row ['created_at'];
					$menuItem ['updated_at'] = $row ['updated_at'];
					array_push ( $menues, $menuItem );
				}

				$slectLocalFeeds = "SELECT DISTINCT
                                                (
                                                    3959 * acos(
                                                        cos(radians('" . $lat . "')) * cos(
                                                            radians(outlet.outlet_lat)
                                                        ) * cos(
                                                            radians(outlet.outlet_long) - radians('" . $lng . "')
                                                        ) + sin(radians('" . $lat . "')) * sin(
                                                            radians(outlet.outlet_lat)
                                                        )
                                                    )
                                                ) AS outlet_distanc,
                                                outlet.out_id AS out_id,
                                                offer.off_id AS offer_id
                                            FROM
                                                ce_outlet outlet,
                                                ce_offers offer
                                            WHERE
                                             outlet.out_id IN (offer.out_ids)
                                            AND offer.off_approved = 1
                                            AND offer.off_expiry > CURDATE()
                                            HAVING
                                                outlet_distanc < " . $GLOBALS ['localFeedsDistance'] . "
                                            ORDER BY
                                                outlet_distanc
                                            LIMIT 0,
                                             " . $GLOBALS ['localFeedsLimit'] . "";

				$localFeeds = array ();
				$localfeed = mysqli_query ( $GLOBALS ['link'], $slectLocalFeeds );
				while ( $rev = mysqli_fetch_assoc ( $localfeed ) ) {
					$localAray = array ();
					$getCountIs = mysqli_query ( $GLOBALS ['link'], "SELECT * FROM ce_reviews WHERE review_outlet_id = '" . $rev ['out_id'] . "'" );
					$rating = mysqli_num_rows ( $getCountIs );
					$selectReviewRating = mysqli_query ( $GLOBALS ['link'], "SELECT * FROM `ce_reviews` WHERE `review_outlet_id` = '" . $rev ['out_id'] . "' ORDER BY review_rating DESC" );
					// $rewies['totalNumberOfReviews'] = 2;//$getCount;
					$averageRating = 0;
					$i = 0;
					if (mysqli_num_rows ( $selectReviewRating ) > 0) {

						while ( $row1 = mysqli_fetch_array ( $selectReviewRating ) ) {
							if ($row1 ['review_rating'] > 0) {
								$averageRating = $averageRating + $row1 ['review_rating'];
								$i ++;
							}
						}
					}
					$finalRating = '';
					if ($averageRating > 0) {
						$finalRating = $averageRating / $i;
					} else {
						$finalRating = 0;
					}
					if ($rating == 0) {
						$rating = 0;
					}
					$localAray ['offer_id'] = $rev ['offer_id'];
					// $localAray['branch_id'] = $rev['branch_id'];
					$localAray ['average_rating'] = $finalRating;
					$localAray ['outlet_distanc'] = $rev ['outlet_distanc'];
					array_push ( $localFeeds, $localAray );
				}
				$myFeeds = array ();
				if ($updatedAt == 0) {
					// "SELECT offer.off__branch_id AS branch__id , (3959 * acos(cos(radians('".$lat."')) * cos(radians(branch.branch__lat)) * cos( radians(branch.branch__lng) - radians('".$lng."')) + sin(radians('".$lat."')) * sin(radians(branch.branch__lat)))) AS outlet_distanc , his."
					$slectMyFeeds = "SELECT DISTINCT
                                            outlet.out_id AS outlet_id,
                                            (
                                                3959 * acos(
                                                    cos(radians('" . $lat . "')) * cos(
                                                        radians(outlet.outlet_lat)
                                                    ) * cos(
                                                        radians(outlet.outlet_long) - radians('" . $lng . "')
                                                    ) + sin(radians('" . $lat . "')) * sin(
                                                        radians(outlet.outlet_lat)
                                                    )
                                                )
                                            ) AS outlet_distanc,
                                            his.outlet_id AS out_id,
                                            his.atrans_id AS history_id,
                                            outlet.outlet_address AS outlet_address,
                                            his.visit_count AS visit_count,
                                            his.offer_id AS offer_id,
                                            outlet.out_name AS out_name,
                                            outlet.outlet_thumb_url AS out_thumbnail,
                                            his.created_at,
                                            his.updated_at,
                                            offer.off_name AS outlet_offer,
                                            cat.subcat_id AS menu_id,
                                            outlet.cat_ids AS outlet_type_id
                                        FROM

                                            ce_outlet outlet,
                                            ce_offers offer,
                                            ce_airmiles_transactions his,
                                            tbl_sub_category cat
                                        WHERE
                                            outlet.cat_ids = cat.subcat_id
                                        AND outlet.out_id IN (offer.out_ids)
                                        AND outlet.out_id = his.outlet_id
                                        AND his.outlet_id IN (offer.out_ids)
                                        AND his.user_id = '" . $userId . "'
                                        AND his.updated_at >= '" . $updatedAt . "'
                                        GROUP BY
                                        out_id
                                        HAVING
                                        outlet_distanc < " . $GLOBALS ['localFeedsDistance'] . "

                                        ORDER BY
                                            outlet_distanc
                                        LIMIT 0,
                                         " . $GLOBALS ['localFeedsLimit'] . "";

					// /having outlet_distanc < ".$GLOBALS['localFeedsDistance']."

					//echo $slectMyFeeds;

					$myfeed = mysqli_query ( $GLOBALS ['link'], $slectMyFeeds );
					while ( $fee = mysqli_fetch_array ( $myfeed ) ) {
						$historyArray = array ();
						$historyArray ['history_id'] = $fee ['history_id'];
						// $historyArray ['branch__id'] = $fee ['branch__id'];
						$historyArray ['outlet_distanc'] = $fee ['outlet_distanc'];
						// $historyArray['off_id'] = $fee['offer_id'];
						$historyArray ['outlet_id'] = $fee ['outlet_id'];

						$historyArray ['offer_id'] = $fee ['offer_id'];
						$historyArray ['out_name'] = $fee ['out_name'];
						$historyArray ['outlet_offer'] = $fee ['outlet_offer'];
						$selectOutletThumbnail = mysqli_query ( $GLOBALS ['link'], "SELECT *  FROM ce_photos WHERE pho_outlet_id = '" . $fee ['out_id'] . "' AND photo_type = 'thumbnails' LIMIT 0,1" );

						if (mysqli_num_rows ( $selectOutletThumbnail ) > 0) {
							$outletThumb = mysqli_fetch_assoc ( $selectOutletThumbnail );
							$historyArray ['out_thumbnail'] = $GLOBALS ['baseUrl'] . $outletThumb ['pho_outlet_id'] . '/' . $outletThumb ['photo_type'] . '/' . $outletThumb ['pho_name'];
						} else {
							$historyArray ['out_thumbnail'] = 'https://heuft.com/upload/image/400x267/no_image_placeholder.png';
						}
						// $historyArray['out_thumbnail'] = $fee['out_thumbnail'];
						$historyArray ['created_at'] = $fee ['created_at'];
						$historyArray ['updated_at'] = $fee ['updated_at'];
						// $historyArray['off_discount'] = $fee['off_discount'];
						// $historyArray['off_title'] = $fee['off_discount'];
						$historyArray ['menu_id'] = $fee ['menu_id'];
						$historyArray ['outlet_type_id'] = $fee ['outlet_type_id'];
						$selectCount = mysqli_query ( $GLOBALS ['link'], "SELECT * FROM ce_airmiles_transactions AS trans WHERE trans.user_id = '" . $userId . "' AND  trans.outlet_id = '" . $fee ['out_id'] . "' AND trans.updated_at >= '" . $updatedAt . "'" );
						$historyArray ['visit_count'] = mysqli_num_rows ( $selectCount );
						$historyArray ['outlet_address'] = $fee ['outlet_address'];

						// $rewies = array();
						// $rewies1 = array();
						// echo "SELECT * FROM ce_reviews WHERE review__outlet_id = '".$outletId."'";
						$getCountIs = mysqli_query ( $GLOBALS ['link'], "SELECT * FROM ce_reviews WHERE review_outlet_id = '" . $fee ['out_id'] . "'" );

						$rating = mysqli_num_rows ( $getCountIs );
						$selectReviewRating = mysqli_query ( $GLOBALS ['link'], "SELECT * FROM `ce_reviews` WHERE `review_outlet_id` = '" . $fee ['out_id'] . "' ORDER BY review_rating DESC" );
						// $rewies['totalNumberOfReviews'] = 2;//$getCount;
						$averageRating = 0;
						$i = 0;
						if (mysqli_num_rows ( $selectReviewRating ) > 0) {

							while ( $row1 = mysqli_fetch_array ( $selectReviewRating ) ) {
								if ($row1 ['review_rating'] > 0) {
									$averageRating = $averageRating + $row1 ['review_rating'];
									$i ++;
								}
							}
						}
						$finalRating = '';
						if ($averageRating > 0) {
							$finalRating = $averageRating / $i;
						} else {
							$finalRating = 0;
						}
						if ($rating == 0) {
							$rating = 0;
						}

						$historyArray ['average_rating'] = $finalRating;
						$historyArray ['votes'] = $rating;

						array_push ( $myFeeds, $historyArray );
					}
				}
				$trendingWeeks = array ();
				$milliseconds_created = date ( 'Y-m-d H:i:s' );
				$oldDAte = date ( 'Y-m-d H:i:s', strtotime ( '- ' . $GLOBALS ['trendingThisWeekLimit'] . 'day', strtotime ( $milliseconds_created ) ) );

				$selectTrends = "SELECT distinct  his.* , SUM(rev.review_rating) AS rating  FROM ce_airmiles_transactions his , ce_reviews rev WHERE his.user_id = '" . $userId . "' AND (his.redeamed_at >='" . $oldDAte . "' AND his.redeamed_at <= '" . $milliseconds_created . "') GROUP BY his.offer_id ORDER BY his.visit_count DESC , rating DESC LIMIT 0,10";

				if (mysqli_query ( $GLOBALS ['link'], $selectTrends )) {
					$records = mysqli_query ( $GLOBALS ['link'], $selectTrends );
					if (mysqli_num_rows ( $records ) > 0) {

						while ( $rec = mysqli_fetch_assoc ( $records ) ) {
							array_push ( $trendingWeeks, $rec ['offer_id'] );
						}
					}
				}
				$response ["trending_this_week"] = $trendingWeeks;
				$feeds ['local_feeds'] = $localFeeds;
				$feeds ['my_history'] = $myFeeds;

				/*
				 * $subCategories = array();
				 * $slectCategories = mysqli_query( $GLOBALS['link'] , "SELECT ce_menu.menu__id , ce_category.* FROM ce_menu LEFT JOIN ce_category ON ce_menu.menu__id = ce_category.cat__menu__id WHERE ce_menu.menu_isavailable = 1");
				 * if(mysqli_num_rows($slectCategories) > 0)
				 	* {
				 	* while($rowCat = mysqli_fetch_array($slectCategories))
				 		* {
				 		*
				 		* $subCat = mysqli_query( $GLOBALS['link'] , "SELECT ce_subcategory.subcat__id AS subcat_id , ce_subcategory.subcat__name AS subcat_name , ce_subcategory.subcat__category_id AS subcat_category_id FROM ce_subcategory WHERE subcat__category_id = '".$rowCat['cat__id']."' AND updated_at >= '".$updatedAt."' ORDER BY subcat__category_id ASC") ; ///WHERE distance < 5
				 		*
				 		* if(mysqli_num_rows($subCat) > 0)
				 			* {
				 			*
				 			* while($row = mysqli_fetch_object($subCat))
				 				* {
				 				* array_push($subCategories , $row);
				 				* }
				 			* }
				 		*
				 		* }
				 	*
				 	* }
				 */

				 /*
				  * $selectOutletTypesOffers = "SELECT * FROM ce_offers WHERE off__approved=1 AND off__expiry > CURDATE()";
				  *
				  * $offersOutletTypeResult = mysqli_query( $GLOBALS['link'] , $selectOutletTypesOffers) ; ///WHERE distance < 5
				  * if(mysqli_num_rows($offersOutletTypeResult) > 0)
				  	* {
				  	* while($offerOutletTypeRow = mysqli_fetch_array($offersOutletTypeResult))
				  		* {
				  		*
				  		* }
				  	* }
				  */
				  $outletListTypes = array ();
				  $selectList = mysqli_query ( $GLOBALS ['link'], "SELECT
                                                                    tbl_sub_category.* , add_category.id AS cat__menu__id , ce_offers.off_id AS offer_id , ce_offers.out_ids AS out_ids , ce_outlet.cat_ids AS sub_cat_ids
                                                                FROM
                                                                    add_category,
                                                                    tbl_sub_category,
                                                                    ce_outlet,
																	ce_offers
                                                                WHERE
																	ce_offers.off_expiry > CURDATE() AND
				  													ce_offers.approved = 1 AND
                                                                    add_category.id IN (tbl_sub_category.cat_ids)
                                                                AND tbl_sub_category.subcat_id IN (
                                                                    ce_outlet.cat_ids

                                                                )
				  												AND ce_outlet.out_id IN (ce_offers.out_ids)
                                                                 " );

				  /*
				   * AND add_category.`status` = 1
				   * AND add_category.updated_at >= '" . $updatedAt . "'
				   */

				  if (mysqli_num_rows ( $selectList ) > 0) {
				  	while ( $list = mysqli_fetch_array ( $selectList ) ) {
				  		//echo $list ['sub_cat_ids'] .'<br>';


				  		$outIds = explode ( ",", $list ['out_ids'] );
				  		$listArray = array ();
				  		if (count ( $outIds ) > 1) {
				  			$offerIds = array ();

				  			for($i = 0; $i < count ( $outIds ); $i ++) {

				  				$outeltResult = mysqli_query ( $GLOBALS ['link'] ,"SELECT * FROM ce_outlet WHERE out_id  = '".$outIds [$i] . "' ");
				  				$outletData =  mysqli_fetch_assoc ( $outeltResult );
				  				$subCatIds = explode ( ",", $outletData['cat_ids'] );
				  				for($j = 0; $j<count($subCatIds); $j++)
				  				{
				  					$selectSubCat = "SELECT * FROM tbl_sub_category   WHERE tbl_sub_category.subcat_id = '".$subCatIds[$j]."' ";
				  					$catSubResult = mysqli_query ( $GLOBALS ['link'], $selectSubCat ); // /WHERE distance < 5
				  					$catSubData = mysqli_fetch_assoc ( $catSubResult );

				  					$listArray ['menu_id'] = $list ['cat__menu__id'];
				  					$listArray ['outlet_type_id'] = $catSubData ['subcat_id'];
				  					$listArray ['outlet_sub_cat_id'] = $catSubData ['subcat_id'];
				  					$listArray ['offer_id'] = $list ['offer_id'];
				  					$listArray ['title'] = $catSubData ['subcat_name'];
				  					$listArray ['created_at'] = $list ['created_at'];
				  					$listArray ['updated_at'] = $list ['updated_at'];
				  					array_push ( $outletListTypes, $listArray );
				  				}

				  			}
				  		} else {


				  			$outeltResult = mysqli_query ( $GLOBALS ['link'] ,"SELECT * FROM ce_outlet WHERE out_id  = '".$list ['out_ids'] . "' ");
				  			$outletData =  mysqli_fetch_assoc ( $outeltResult );
				  			$subCatIds = explode ( ",", $outletData['cat_ids'] );
				  			for($j = 0; $j<count($subCatIds); $j++)
				  			{
				  				$selectSubCat = "SELECT * FROM tbl_sub_category   WHERE tbl_sub_category.subcat_id = '".$subCatIds[$j]."' ";
				  				$catSubResult = mysqli_query ( $GLOBALS ['link'], $selectSubCat ); // /WHERE distance < 5
				  				$catSubData = mysqli_fetch_assoc ( $catSubResult );
				  				 
				  				$listArray ['menu_id'] = $list ['cat__menu__id'];
				  				$listArray ['outlet_type_id'] = $catSubData ['subcat_id'];
				  				$listArray ['outlet_sub_cat_id'] = $catSubData ['subcat_id'];
				  				$listArray ['offer_id'] = $list ['offer_id'];
				  				$listArray ['title'] = $catSubData ['subcat_name'];
				  				$listArray ['created_at'] = $list ['created_at'];
				  				$listArray ['updated_at'] = $list ['updated_at'];
				  				array_push ( $outletListTypes, $listArray );
				  			}

				  		}
				  	}
				  }

				  $newArray = array ();
				  $count = 1;
				  foreach ( $outletListTypes as $key => $value ) {
				  	if ($count == 1) {
				  		$jogar = $value ['offer_id'];
				  		$value ['offer_id'] = array (
				  				$jogar
				  		);
				  		array_push ( $newArray, $value );
				  	} else {
				  		$isFound = false;
				  		for($k = 0; $k < count ( $newArray ); $k ++) {
				  			if ($value ['menu_id'] == $newArray [$k] ['menu_id'] && $value ['outlet_type_id'] == $newArray [$k] ['outlet_type_id']) {

				  				$isFound = true;

				  				$newArrayOffer = $newArray [$k] ['offer_id'];
				  				array_push ( $newArrayOffer, $value ['offer_id'] );
				  				$newArray [$k] ['offer_id'] = $newArrayOffer;
				  			}
				  		}
				  		if (! $isFound) {
				  			$jogar = $value ['offer_id'];
				  			$value ['offer_id'] = array (
				  					$jogar
				  			);
				  			array_push ( $newArray, $value );
				  		}
				  		/*
				  		 * foreach ($newArray as $key2 => $value2) {
				  		 *
				  		 * if($value['menu_id'] == $value2['menu_id'] && $value['outlet_type_id'] == $value2['outlet_type_id'] )
				  		 	* {
				  		 	* $isFound = true;
				  		 	* echo 'menu id '.$value['menu_id'];
				  		 	* echo '<br>'.$value['offer_id'];
				  		 	* echo '<br>';
				  		 	* $newArrayOffer = $value2['offer_id'];
				  		 	* array_push($newArrayOffer , $value['offer_id']);
				  		 	*
				  		 	* $value2['offer_id'] = $newArrayOffer;
				  		 	* print_r($value2['offer_id']);
				  		 	* }
				  		 * }
				  		 * if(!$isFound)
				  		 	* {
				  		 	* $jogar = $value['offer_id'];
				  		 	* $value['offer_id'] = array($jogar);
				  		 	* array_push($newArray , $value);
				  		 	* }
				  		 */
				  	}
				  	$count ++;
				  }
				  	
				  /*
				   * $outlets_list = array();
				   * //,offer.off__category_id AS cat_id, offer.off__subcategory_id AS subcat_id
				   * $selectOutlet = mysqli_query( $GLOBALS['link'] , "SELECT outlet.out__id AS outlet_id , outlet.out__name AS outlet_name, offer.off__category_id AS outlet_type_id , cat.cat__menu__id AS menu_id FROM ce_category cat, ce_offers offer, ce_outlet outlet WHERE outlet.out__id = offer.off__outlet_id AND outlet.updated_at >= '".$updatedAt."' AND offer.off__category_id = cat.cat__menu__id GROUP BY outlet.out__id") ; ///WHERE distance < 5
				   *
				   * if(mysqli_num_rows($selectOutlet) > 0)
				   	* {
				   	* while($row = mysqli_fetch_object($selectOutlet))
				   		* {
				   		* array_push($outlets_list , $row);
				   		* }
				   	* }
				   */

				   $userData = array ();
				   $selectProfile = "SELECT user.fname AS first_name , user.lname AS last_name , user.username ,app.applicant_photo,  app.mobile AS phone_no, app.nationality , app.cardname, app.emirate AS city, app.employer AS employer_name , app.membership_no AS membershipNo , app.membership_type AS memberShipType , app.membership_expiry AS membershipExpiry , app.applicant_photo AS applicantPhoto FROM users user ,applicants app WHERE app.email=user.email AND user.id = '" . $userId . "' AND app.updated_at >= '" . $updatedAt . "'";
				   if (mysqli_query ( $GLOBALS ['link'], $selectProfile )) {
				   	$resultProfile = mysqli_query ( $GLOBALS ['link'], $selectProfile ); // /WHERE distance < 5
				   	if (mysqli_num_rows ( $resultProfile ) > 0) {
				   		// // $profile = mysqli_fetch_assoc($resultProfile);
				   		$selectTotalAirmielsData = "SELECT * FROM ce_airmiles_transactions WHERE user_id = '" . $userId . "' AND redemption_type = 1";
				   		$getTotalResult = mysqli_query ( $GLOBALS ['link'], $selectTotalAirmielsData ); // /WHERE distance < 5
				   		$totalAirmies = 0;
				   		if (mysqli_num_rows ( $getTotalResult ) > 0) {
				   			 
				   			while ( $row = mysqli_fetch_array ( $getTotalResult ) ) {
				   				$totalAirmies = $totalAirmies + $row ['airmiles_earned'];
				   			}
				   		}
				   		while ( $row = mysqli_fetch_assoc ( $resultProfile ) ) {
				   			$userData ['first_name'] = $row ['first_name'];
				   			$userData ['last_name'] = $row ['last_name'];
				   			$userData ['username'] = $row ['username'];
				   			$userData ['phone_no'] = $row ['phone_no'];
				   			$userData ['nationality'] = $row ['nationality'];
				   			$userData ['cardName'] = $row ['cardname'];
				   			 
				   			$userData ['city'] = $row ['city'];
				   			$userData ['employer_name'] = $row ['employer_name'];
				   			$userData ['memberShipId'] = $row ['membershipNo'];
				   			$userData ['memberShipType'] = $row ['memberShipType'];
				   			$userData ['membershipExpiry'] = $row ['membershipExpiry'];
				   			$baseUrlForImage = '';
				   			$selectBaseUrlForImage = mysqli_query ( $GLOBALS ['link'], "SELECT content_text FROM `ce_content` WHERE `content_type` = 'Member Photos URL'" );
				   			if (mysqli_num_rows ( $selectBaseUrlForImage ) > 0) {
				   				$row1 = mysqli_fetch_assoc ( $selectBaseUrlForImage );
				   				$baseUrlForImage = $row1 ['content_text'];
				   			}
				   			$url = trim ( preg_replace ( '/\s\s+/', ' ', $baseUrlForImage ) );
				   			if($row['applicant_photo'] !='')
				   				$userData ['applicantPhoto'] = $GLOBALS['url'].$row['applicant_photo'];
				   				else
				   					$userData ['applicantPhoto'] = 'http://www.clubexec.net/web/cms/images/avatar.png';
				   					 
				   					//$userData ['applicantPhoto'] = $GLOBALS['url'].$row['applicant_photo'];
				   					$userData ['airmiles'] = $totalAirmies;
				   		}
				   	}
				   }

				   $sponsors = array ();
				   $selectSponsors = "SELECT offer.off_id AS offer_id , out_ids  FROM ce_offers offer WHERE offer.updated_at >= '" . $updatedAt . "' AND offer.off_expiry>=CURDATE() AND offer.off_approved = 1 AND offer.sponsored = 1 GROUP BY offer.off_id";
				   if (mysqli_query ( $GLOBALS ['link'], $selectSponsors )) {
				   	$records = mysqli_query ( $GLOBALS ['link'], $selectSponsors );
				   	if (mysqli_num_rows ( $records ) > 0) {

				   		while ( $rec = mysqli_fetch_assoc ( $records ) ) {
				   			 
				   			array_push ( $sponsors, $rec ['offer_id'] );
				   			$sponsorsArray = array ();
				   			$sponsorsArray ['offer_id'] = $rec ['offer_id'];
				   			/*
				   			 * $sponsorsArray['outlet_id'] = $rec['outlet_id'];
				   			 * $sponsorsArray['outlet_name'] = $rec['outlet_name'];
				   			 * $sponsorsArray['outlet_description'] = $rec['outlet_description'];
				   			 * $sponsorsArray['created_at'] = $rec['created_at'];
				   			 * $sponsorsArray['updated_at'] = $rec['updated_at'];
				   			 * $sponsorsArray['rating'] = $rec['rating'];
				   			 */
				   			$selectOutletThumbnail = mysqli_query ( $GLOBALS ['link'], "SELECT *  FROM ce_photos WHERE pho_outlet_id IN (" . $rec ['out_ids'] . ") AND photo_type = 'thumbnails' LIMIT 0,1" );
				   			 
				   			if (mysqli_num_rows ( $selectOutletThumbnail ) > 0) {
				   				$outletThumb = mysqli_fetch_assoc ( $selectOutletThumbnail );
				   				// $sponsorsArray['outlet_banner_url'] = $GLOBALS['baseUrl'].$outletThumb['pho__outlet_id'].'/'.$outletThumb['photo_type'].'/'.$outletThumb['pho__name'];
				   			} else {
				   				// $sponsorsArray['outlet_banner_url'] = 'https://heuft.com/upload/image/400x267/no_image_placeholder.png';
				   			}
				   			 
				   			// array_push($sponsors , $sponsorsArray);
				   		}
				   	}
				   }

				   // ///////////////////////////////////////////////To GET NEWS//////////////////////////////////////
				   $news_list = array ();
				   $selectNews = mysqli_query ( $GLOBALS ['link'], "SELECT news_title AS newsTitle , news_text AS newsDescription , created_at AS newsdate FROM ce_news WHERE news_status = 1 AND updated_at >= '" . $updatedAt . "'" ); // /WHERE distance < 5

				   if (mysqli_num_rows ( $selectNews ) > 0) {
				   	while ( $row = mysqli_fetch_array ( $selectNews ) ) {
				   		$internalArray = array ();

				   		$internalArray ['newsTitle'] = str_replace(array( "\r\n" , "\n" , "\\r\\n"), '', $row ['newsTitle']);
				   		$internalArray ['newsDescription'] = str_replace(array( "\r\n" , "\n" , "\\r\\n"), '', $row ['newsDescription']);
				   		$internalArray ['newsdate'] = $row ['newsdate'];
				   		$seconds = $row['newsdate'] / 1000;
				   		$dateFormat = date("d/m/Y H:i:s", $seconds);
				   		$internalArray ['formatedDate'] = $dateFormat;
				   		$internalArray ['seconds'] = $seconds;
				   		array_push ( $news_list, $internalArray );
				   	}
				   }

				   //$news_list = str_replace(array( "\r\n" , "\n" , "\\r\\n"), '', $news_list);

				   // ///////////////////////////////////////////////Promotional Videos//////////////////////////////////////
				   $videos_list = array ();
				   $selectVideos = mysqli_query ( $GLOBALS ['link'], "SELECT * FROM ce_videos WHERE video_status = 1 AND approved = 1 AND updated_at >= '" . $updatedAt . "'" ); // /WHERE distance < 5

				   if (mysqli_num_rows ( $selectVideos ) > 0) {
				   	while ( $row = mysqli_fetch_array ( $selectVideos ) ) {

				   		$internalArray = array ();

				   		$seconds = $row ['created_at'] / 1000;
				   		$min = $row ['updated_at'] / 1000;

				   		$internalArray ['video_creation_date'] = date ( "d/m/Y H:i:s", $seconds );
				   		$internalArray ['video_update_date'] = date ( "d/m/Y H:i:s", $min );

				   		$internalArray ['video_id'] = $row ['video_id'];
				   		$internalArray ['video_name'] = $row ['video_name'];
				   		$internalArray ['video_short_description'] = $row ['video_short_description'];
				   		$internalArray ['video_status'] = $row ['approved'];

				   		$internalArray ['video_thumbnail'] = 'http://www.clubexec.net/web/cms/uploadDirectory/' . $row ['thumbnail_photo'];
				   		$ext = pathinfo ( $row ['video_url'], PATHINFO_EXTENSION );
				   		$internalArray ['video_type'] = $ext;

				   		$internalArray ['video_url'] = 'http://www.clubexec.net/web/cms/uploadDirectory/' . $row ['video_url'];

				   		array_push ( $videos_list, $internalArray );
				   	}
				   }

				   // //////////////////////////////////////////////FAQ//////////////////////////////////////
				   // ///////////////////////////////////////////////Promotional Videos//////////////////////////////////////
				   /*
				    * $pinImagesLst = array();
				    * $selectPinImages = mysqli_query( $GLOBALS['link'] , "SELECT cat__menu__id AS menu_id , cat__id AS cat_id , cat__thumbnail_pin AS pin_image , created_at , updated_at FROM ce_category WHERE updated_at >= '".$updatedAt."'") ; ///WHERE distance < 5
				    *
				    * if(mysqli_num_rows($selectPinImages) > 0)
				    	* {
				    	* while($row = mysqli_fetch_array($selectPinImages))
				    		* {
				    		* $pins = array();
				    		* $pins['menu_id'] = $row['menu_id'];
				    		* $pins['pin_image'] = $row['pin_image'];
				    		* $pins['created_at'] = $row['created_at'];
				    		* $pins['updated_at'] = $row['updated_at'];
				    		* array_push($pinImagesLst , $pins);
				    		* }
				    	* }
				    */

				    // //////////////////////////////////////////////FAQ//////////////////////////////////////
				    $faq_list = array ();
				    $selectFaqs = mysqli_query ( $GLOBALS ['link'], "SELECT faq_id AS faq_id , faq_question AS question_id, faq_question AS question ,  faq_answer AS answer , created_at , updated_at FROM ce_faq WHERE status = 1  AND approved=1 AND updated_at >= '" . $updatedAt . "'" ); // /WHERE distance < 5

				    if (mysqli_num_rows ( $selectFaqs ) > 0) {
				    	while ( $row = mysqli_fetch_array ( $selectFaqs ) ) {

				    		$internalArray = array ();

				    		$internalArray ['faq_id'] = $row ['faq_id'];

				    		$internalArray ['question_id'] = str_replace(array( "\r\n" , "\n" , "\\r\\n"), '', $row ['question_id']);
				    		$internalArray ['question'] = str_replace(array( "\r\n" , "\n" , "\\r\\n"), '', $row ['question']);
				    		$internalArray ['answer'] = str_replace(array( "\r\n" , "\n" , "\\r\\n"), '', $row ['answer']);
				    		$internalArray ['created_at'] = $row ['created_at'];
				    		$internalArray ['updated_at'] = $row ['updated_at'];

				    		array_push ( $faq_list, $internalArray );
				    	}
				    }

				    // //////////////////////////////////////////////FAQ//////////////////////////////////////
				    $terms_list = array ();
				    $selectTerms = "SELECT content_text AS generalTerms , content_text AS impTerms FROM ce_content WHERE (content_type='Terms & Conditions' OR content_type='impTerms') AND content_status=1 AND content_update_date >= '" . $updatedAt . "'";
				    $getResult = mysqli_query ( $GLOBALS ['link'], $selectTerms ); // /WHERE distance < 5
				    if (mysqli_num_rows ( $getResult ) > 0) {
				    	while ( $row = mysqli_fetch_assoc ( $getResult ) ) {
				    		$generalTerms = mb_convert_encoding ( $row ['generalTerms'], "UTF-8", "HTML-ENTITIES" );
				    		$impTerms = mb_convert_encoding ( $row ['impTerms'], "UTF-8", "HTML-ENTITIES" );
				    		$terms_list ['generalTerms'] = str_replace(array( "\r\n" , "\n" , "\\r\\n"), '', $generalTerms) ;
				    		$terms_list ['impTerms'] = str_replace(array( "\r\n" , "\n" , "\\r\\n"), '', $impTerms) ;
				    	}
				    }

				    $vouchers_list = array ();
				    $bookmarks_list = array ();
				     
				    $vouchers = array ();
				     
				    $selectVouchers = mysqli_query ( $GLOBALS ['link'], "SELECT outlet.out_name AS outlet_name , outlet.out_id AS outlet_id , outlet.outlet_address AS address , outlet.outlet_thumb_url AS photo , vouch.vouch_id, vouch.vouch_quantity  FROM ce_outlet outlet, ce_vouchers vouch , ce_offers offer WHERE offer.off_expiry>=CURDATE() AND vouch.status = 1  AND vouch.approved=1 AND vouch.vouch_expiry>=CURDATE() AND  vouch.vouch_outlet_id=outlet.out_id AND  vouch.updated_at >= '" . $updatedAt . "' GROUP BY outlet.out_id" );
				    if (mysqli_num_rows ( $selectVouchers ) > 0) {
				    	while ( $row = mysqli_fetch_array ( $selectVouchers ) ) {
				    		$voucherOffersArray = array ();
				    		$vouchers ['outlet_id'] = $row ['outlet_id'];
				    		$vouchers ['outlet_name'] = $row ['outlet_name'];
				    		$vouchers ['address'] = $row ['address'];
				    		 
				    		// $vouchers['rating'] = $row['rating'];
				    		// $vouchers['outlet_thumb'] = $row['photo'];
				    		$vouchers ['voucherID'] = $row ['vouch_id'];
				    		//echo "SELECT offer.off_id AS off_id, offer.off_name AS off_discount, offer.off_expiry, offer.off_name AS off_title, offer.off_expiry AS offer_validity , offer.off_description AS off_discount_detail , offer.off_specialterms AS off_imp_terms_n_conditions ,  offer.off_terms AS off_terms_n_conditions   FROM ce_offers offer WHERE  FIND_IN_SET ('" . $row ['outlet_id'] . "', (offer.out_ids))" ;
				    		$selectVoucherOffers = mysqli_query ( $GLOBALS ['link'], "SELECT offer.vouch_id AS off_id, offer.vouch_quantity AS vouch_quantity, offer.vouch_name AS off_discount, offer.vouch_expiry AS off_expiry, offer.vouch_name AS off_title, offer.vouch_expiry AS offer_validity , offer.vouch_description AS off_discount_detail , offer.vouch_specialterms AS off_imp_terms_n_conditions ,  offer.vouch_terms AS off_terms_n_conditions   FROM ce_vouchers offer WHERE  vouch_outlet_id = '" . $row ['outlet_id'] . "'" );
				    		if (mysqli_num_rows ( $selectVoucherOffers ) > 0) {
				    			while ( $rowOffer = mysqli_fetch_array ( $selectVoucherOffers ) ) {
				    				$isRedeamedArray = array ();
				    				$isRedeamedArray ['off_id'] = $rowOffer ['off_id'];
				    				$isRedeamedArray ['quantity'] = $rowOffer ['vouch_quantity'];
				    				$isRedeamedArray ['off_discount'] = $rowOffer ['off_discount'];
				    				$isRedeamedArray ['off_discount_detail'] = $rowOffer ['off_discount_detail'];
				    				$isRedeamedArray ['offerRow_expiry'] = $rowOffer ['off_expiry'];
				    				$isRedeamedArray ['off_title'] = $rowOffer ['off_title'];
				    				// $isRedeamedArray['check'] = 'jfkasdjfk';
				    				// $isRedeamedArray['offer_validity'] = $rowOffer['offer_validity'];
				    				// $isRedeamedArray['offer_validity'] = $rowOffer['offer_validity'];
				    				$selectRedeamedData = mysqli_query ( $GLOBALS ['link'], "SELECT * FROM ce_airmiles_transactions WHERE user_id = '" . $postData ['userId'] . "' AND redemption_type = 2 AND updated_at >= '" . $updatedAt . "' AND voucher_id = '" . $rowOffer ['off_id'] . "'" );
				    				// echo "SELECT * FROM ce_airmiles_transactions WHERE user_id = '".$postData['userId']."' AND redemption_type = 2 AND updated_at >= '".$updatedAt."' AND offer_id = '".$rowOffer['off_id']."'"."<br>" ;

				    				// echo mysqli_num_rows($selectRedeamedData).'<br>';
				    				if (mysqli_num_rows ( $selectRedeamedData ) > 0) {
				    					$isRedeamedArray ['is_redeamed'] = 1;
				    				} else {
				    					$isRedeamedArray ['is_redeamed'] = 0;
				    				}
				    				$redeamedArray = mysqli_fetch_assoc ( $selectRedeamedData );
				    				$isRedeamedArray ['redeemed_at'] = $redeamedArray ['redeamed_at'];
				    				$isRedeamedArray ['off_imp_terms_n_conditions'] = str_replace(array( "\r\n" , "\n" , "\\r\\n"), '', $rowOffer ['off_imp_terms_n_conditions']) ;
				    				$isRedeamedArray ['off_terms_n_conditions'] = str_replace(array( "\r\n" , "\n" , "\\r\\n"), '', $rowOffer ['off_terms_n_conditions']) ;

				    				array_push ( $voucherOffersArray, $isRedeamedArray );
				    			}
				    		}
				    		$vouchers ['offers'] = $voucherOffersArray;
				    		array_push ( $vouchers_list, $vouchers );
				    	}
				    }

				    // ///////////////////////////////////////////////////////////////////////
				    if ($updatedAt == 0) {
				    	$selectBookmarks = mysqli_query ( $GLOBALS ['link'], "SELECT fav_offer_id AS outlet_id FROM ce_favourites WHERE fav_user_id = '" . $userId . "' AND updated_at >= '" . $updatedAt . "'" );

				    	if (mysqli_num_rows ( $selectBookmarks ) > 0) {
				    		/*
				    		 * $countIS = mysqli_num_rows($selectBookmarks);
				    		 * for($i = 0; $i<$countIS; $i++)
				    		 	* {
				    		 	* $bookmarks_list[$i] = $row['outlet_id'];
				    		 	* }
				    		 */
				    		 while ( $row = mysqli_fetch_array ( $selectBookmarks ) ) {
				    		 	array_push ( $bookmarks_list, $row ['outlet_id'] );
				    		 }
				    	}
				    }

				    // //////////////////////////////////////////////////////////////////
				    $selectNearByOutlets = "SELECT DISTINCT
                                                (
                                                    3959 * acos(
                                                        cos(radians('" . $lat . "')) * cos(
                                                            radians(outlet.outlet_lat)
                                                        ) * cos(
                                                            radians(outlet.outlet_long) - radians('" . $lng . "')
                                                        ) + sin(radians('" . $lat . "')) * sin(
                                                            radians(outlet.outlet_lat)
                                                        )
                                                    )
                                                ) AS outlet_distanc,
                                                outlet.out_id AS out_id,
                                                offer.off_id AS offer_id
                                            FROM
                                                ce_outlet outlet,
                                                ce_offers offer
                                            WHERE
                                             outlet.out_id IN (offer.out_ids)
                                            AND offer.off_approved = 1
                                            AND offer.off_expiry > CURDATE()
                                            HAVING
                                                outlet_distanc < " . $GLOBALS ['localFeedsDistance'] . "
                                            ORDER BY
                                                outlet_distanc
                                            LIMIT 0,
                                             " . $GLOBALS ['localFeedsLimit'] . "";

				    $nearByOutlets = array ();
				    if (mysqli_query ( $GLOBALS ['link'], $selectNearByOutlets )) {
				    	$resultNearBy = mysqli_query ( $GLOBALS ['link'], $selectNearByOutlets ); // /WHERE distance < 5
				    	if (mysqli_num_rows ( $resultNearBy ) > 0) {
				    		while ( $row = mysqli_fetch_object ( $resultNearBy ) ) {
				    			array_push ( $nearByOutlets, $row );
				    		}
				    	}
				    }
				    // ///////////////////////////////////////////////////////////////////////////
				    $outleDetails = array ();
				    $outleDetails1 = array ();
				    $tags = array ();
				    // $airmiles = array();
				    $userPin = $postData ['userId'];
				    $all_outlets = array ();
				    $selectOffers = "SELECT * FROM ce_offers WHERE approved=1 AND off_expiry > CURDATE() AND off_valid_from >=CURDATE() AND ce_offers.updated_at >= '" . $updatedAt . "'";
				    $insertRecord = 1;
				    $offersResult = mysqli_query ( $GLOBALS ['link'], $selectOffers ); // /WHERE distance < 5

				    if (mysqli_num_rows ( $offersResult ) > 0) {
				    	while ( $offerRow = mysqli_fetch_array ( $offersResult ) ) {
				    		$selectOulet = "SELECT * FROM ce_outlet outlet WHERE outlet.out_id IN (" . $offerRow ['out_ids'] . ") ";

				    		$outletResult = mysqli_query ( $GLOBALS ['link'], $selectOulet ); // /WHERE distance < 5
				    		if (mysqli_num_rows ( $outletResult ) > 0) {
				    			while ( $row = mysqli_fetch_array ( $outletResult ) ) {

				    				$outletArray = array ();
				    				$outletArray ['offer_id'] = $offerRow ['off_id'];
				    				$outletArray ['outlet_id'] = $row ['out_id'];
				    				$outletArray ['outlet_name'] = $row ['out_name'];
				    				$selectVisitCount = mysqli_query ( $GLOBALS ['link'], "SELECT * FROM ce_airmiles_transactions AS trans WHERE trans.user_id = '" . $userId . "' AND  trans.outlet_id = '" . $offerRow ['off_outlet_id'] . "'" );
				    				$outletArray ['visit_count'] = mysqli_num_rows ( $selectVisitCount );

				    				// $selectCat = "SELECT id AS cat_menu_id, category_image AS pin_image FROM add_category WHERE id IN (" . $row['cat_ids'] . ")";
				    				$selectCat = "SELECT cat_ids AS cat_menu_id, thumbnail_photo AS pin_image FROM tbl_sub_category WHERE subcat_id IN (" . $row ['cat_ids'] . ")";
				    				 
				    				$catResult = mysqli_query ( $GLOBALS ['link'], $selectCat ); // /WHERE distance < 5
				    				$catData = mysqli_fetch_assoc ( $catResult );
				    				if ($catData ['pin_image'] != '') {
				    					$outletArray ['pin_image'] = $GLOBALS['url'].$catData ['pin_image'];
				    				} else {
				    					$outletArray ['pin_image'] = 'https://heuft.com/upload/image/400x267/no_image_placeholder.png';
				    				}
				    				$menuIds = explode ( ",", $catData ['cat_menu_id'] );

				    				//$outletArray ['menu_id'] = $catData ['cat_menu_id'];
				    				//$outletArray ['outlet_type_id'] = $catData ['cat_menu_id'];
				    				$outletArray ['outlet_sub_cat_id'] = $row ['cat_ids'];

				    				$selectOutletThumbnail = mysqli_query ( $GLOBALS ['link'], "SELECT *  FROM ce_photos WHERE pho_outlet_id = '" . $row ['out_id'] . "' AND photo_type = 'thumbnails' LIMIT 0,1" );

				    				if ($row ['thumbnail_photo'] != '') {
				    					// $outletThumb = mysqli_fetch_assoc($selectOutletThumbnail);
				    					$outletArray ['outlet_thumb'] = 'http://www.clubexec.net/web/cms/uploadDirectory/' . $row ['thumbnail_photo'];
				    				} else {
				    					$outletArray ['outlet_thumb'] = 'https://heuft.com/upload/image/400x267/no_image_placeholder.png';
				    				}
				    				// echo "SELECT off__description FROM ce_offers WHERE off__outlet_id = '". $row['out__id']."' ";

				    				// echo "SELECT off__description FROM ce_offers WHERE off__outlet_id = '". $row['out__id']."' ";
				    				$selectOffDesc = mysqli_query ( $GLOBALS ['link'], "SELECT off_description  FROM  ce_offers WHERE off_outlet_id = '" . $row ['out_id'] . "' " );
				    				$desc1 = mysqli_fetch_assoc ( $selectOffDesc );
				    				$out_desc = '';
				    				/*
				    				 * if($desc1['off__description'] != '')
				    				 	* {
				    				 	* $out_desc = $desc1['off__description'];
				    				 	* }
				    				 * else
				    				 	* {
				    				 	*/
				    				 if ($row ['out_description'] != '') {
				    				 	$out_desc = $row ['out_description'];
				    				 } else {
				    				 	$out_desc = '';
				    				 }
				    				 // }
				    				 $out_description = mb_convert_encoding ( $out_desc, "UTF-8", "HTML-ENTITIES" );
				    				 $outletArray ['description'] = $out_description;
				    				 $outletArray ['created_at'] = $row ['created_at'];
				    				 $outletArray ['updated_at'] = $row ['updated_at'];
				    				 $outletArray ['outlet_address'] = $row ['outlet_address'];
				    				 $outletArray ['contact_no'] = $row ['outlet_telephone'];
				    				 // $branch__description = mb_convert_encoding($outletBranch['branch__description'], "UTF-8", "HTML-ENTITIES");
				    				 $outletArray ['short_message'] = ''; // $branch__description;
				    				 $outletArray ['latitude'] = $row ['outlet_lat'];
				    				 $outletArray ['longitude'] = $row ['outlet_long'];
				    				 $outletArray ['website_url'] = $row ['outlet_website'];
				    				 $outletArray ['fb_url'] = $row ['outlet_facebook'];
				    				 $outletArray ['twitter_url'] = $row ['outlet_twitter'];
				    				 $timings = array ();
				    				 if($row ['sun_check'] == 1)
				    				 {
				    				 	$sunday = array (
				    				 			"day" => "Su",
				    				 			"start_time" => $row ['sun_open'].':00',
				    				 			"end_time" => $row ['sun_close'] .':00'
				    				 	);
				    				 	array_push ( $timings, $sunday );
				    				 }
				    				 if($row ['mon_check'] == 1)
				    				 {
				    				 	$monday = array (
				    				 			"day" => "Mo",
				    				 			"start_time" => $row ['mon_open'].':00',
				    				 			"end_time" => $row ['mon_close'] .':00'
				    				 	);
				    				 	array_push ( $timings, $monday );
				    				 }
				    				 if($row ['tue_check'] == 1)
				    				 {
				    				 	$tuesday = array (
				    				 			"day" => "Tu",
				    				 			"start_time" => $row ['tue_open'].':00',
				    				 			"end_time" => $row ['tue_close'] .':00'
				    				 	);
				    				 	array_push ( $timings, $tuesday );
				    				 }
				    				 if($row ['wed_check'] == 1)
				    				 {

				    				 	$wednesday = array (
				    				 			"day" => "We",
				    				 			"start_time" => $row ['wed_open'].':00',
				    				 			"end_time" => $row ['wed_close'] .':00'
				    				 	);
				    				 	array_push ( $timings, $wednesday );
				    				 }
				    				 if($row ['thu_check'] == 1)
				    				 {
				    				 	$thursday = array (
				    				 			"day" => "Th",
				    				 			"start_time" => $row ['thu_open'].':00',
				    				 			"end_time" => $row ['thu_close'] .':00'
				    				 	);
				    				 	array_push ( $timings, $thursday );
				    				 }
				    				 if($row ['fri_check'] == 1)
				    				 {
				    				 	$friday = array (
				    				 			"day" => "Fr",
				    				 			"start_time" => $row ['fri_open'].':00',
				    				 			"end_time" => $row ['fri_close'] .':00'
				    				 	);
				    				 	array_push ( $timings, $friday );
				    				 }
				    				 if($row ['sat_check'] == 1)
				    				 {
				    				 	$saturday = array (
				    				 			"day" => "Sa",
				    				 			"start_time" => $row ['sat_open'].':00',
				    				 			"end_time" => $row ['sat_close'] .':00'
				    				 	);
				    				 	array_push ( $timings, $saturday );
				    				 }


				    				 $outletArray ['opening_timings'] = $timings;
				    				 $offers = array ();
				    				 $offers ['off_discount'] = $offerRow ['off_name'];
				    				 $offers ['is_sponsord'] = $offerRow ['sponsored'];
				    				 $off__description = mb_convert_encoding ( $offerRow ['off_description'], "UTF-8", "HTML-ENTITIES" );
				    				 $offers ['off_discount_detail'] = $off__description;
				    				 $offers ['offerRow_expiry'] = $offerRow ['off_expiry'];
				    				 $off__terms = mb_convert_encoding ( $offerRow ['off_terms'], "UTF-8", "HTML-ENTITIES" );
				    				 $offers ['off_terms_n_conditions'] = $off__terms;
				    				 $offers ['off_imp_terms_n_conditions'] = $offerRow ['off_specialterms'];

				    				 $selectBookmarks = mysqli_query ( $GLOBALS ['link'], "SELECT * FROM `ce_favourites` WHERE `fav_offer_id` = '" . $offerRow ['off_id'] . "' AND `fav_user_id` = '" . $userPin . "'" );
				    				 if (mysqli_num_rows ( $selectBookmarks ) > 0) {
				    				 	$outletArray ['isBookmarked'] = 1;
				    				 } else {
				    				 	$outletArray ['isBookmarked'] = 0;
				    				 }
				    				 $tags = array ();

				    				 $selectTags = mysqli_query ( $GLOBALS ['link'], "SELECT tag_id , tag_name , status AS tag_isavailable FROM  ce_tags WHERE tag_id IN (" . $offerRow ['tag_ids'] . ") " );

				    				 if (mysqli_num_rows ( $selectTags ) > 0) {
				    				 	while ( $singleTag = mysqli_fetch_array ( $selectTags ) ) {
				    				 		$singleTagArray = array ();
				    				 		$singleTagArray ['tag_id'] = $singleTag ['tag_id'];
				    				 		$singleTagArray ['tag_name'] = $singleTag ['tag_name'];
				    				 		$singleTagArray ['tag_isavailable'] = $singleTag ['tag_isavailable'];
				    				 		array_push ( $tags, $singleTagArray );
				    				 	}
				    				 } else {
				    				 	$singleTagArray ['tag_id'] = '';
				    				 	$singleTagArray ['tag_name'] = '';
				    				 }
				    				 /*
				    				  * $airmiles = array();
				    				  * $selectAirmiles = mysqli_query( $GLOBALS['link'] , "SELECT member_airmile.airmiles_no AS airmiles_earned_now, airmile.air__value AS total_airmiles
				    				  * FROM ce_airmiles airmile , ce_airmiles_outlet_match airmile_match , member_airmiles_match member_airmile
				    				  * WHERE airmile_match.airout__airmiles_id=airmile.air__id AND airmile_match.airout__outlet_id= '".$row['out__id']."' AND member_airmile.membership_no= '".$userPin."'") ;
				    				  * if(mysqli_num_rows($selectAirmiles) > 0)
				    				  	* {
				    				  	* while($row1 = mysqli_fetch_object($selectAirmiles))
				    				  		* {
				    				  		* array_push($airmiles , $row1);
				    				  		* }
				    				  	* }
				    				  */

				    				  $outletId = $row ['out_id'];
				    				  $rewies = array ();
				    				  $rewies1 = array ();
				    				  // echo "SELECT * FROM ce_reviews WHERE review__outlet_id = '".$outletId."'";
				    				  $getCount = mysqli_query ( $GLOBALS ['link'], "SELECT * FROM ce_reviews WHERE review_outlet_id = '" . $outletId . "' AND approved=1" );

				    				  $countIs = mysqli_num_rows ( $getCount );

				    				  $selectReview = mysqli_query ( $GLOBALS ['link'], "SELECT * FROM `ce_reviews` WHERE `review_outlet_id` = '" . $outletId . "' AND approved=1 ORDER BY review_rating DESC" );
				    				  // $rewies['totalNumberOfReviews'] = 2;//$getCount;
				    				  $averageRating = 0;
				    				  $i = 0;
				    				  if (mysqli_num_rows ( $selectReview ) > 0) {

				    				  	while ( $row1 = mysqli_fetch_assoc ( $selectReview ) ) {
				    				  		$rewies ['review_id'] = $row1 ['review_id'];
				    				  		$rewies ['review_title'] = $row1 ['review_title'];
				    				  		$rewies ['review_review'] = $row1 ['review_review'];
				    				  		$rewies ['review_outlet_id'] = $row1 ['review_outlet_id'];
				    				  		$rewies ['review_user_id'] = $row1 ['review_user_id'];
				    				  		$rewies ['review_date'] = $row1 ['review_date'];
				    				  		$rate = '';
				    				  		if($row1 ['review_rating'] > 5)
				    				  		{
				    				  			$rate = 5;
				    				  		}
				    				  		else
				    				  		{
				    				  			$rate = $row1 ['review_rating'];
				    				  		}
				    				  		$rewies ['review_rating'] = $rate;
				    				  		$rewies ['review_approved'] = $row1 ['approved'];
				    				  		$url = trim ( preg_replace ( '/\s\s+/', ' ', $GLOBALS ['baseUrl'] ) );

				    				  		if ($row1 ['review_image'] != '') {
				    				  			$rewies ['review_image'] = $url . 'review_images/' . $row1 ['review_image']; // $menu_photos;
				    				  		} else {
				    				  			$rewies ['review_image'] = ""; // $menu_photos;
				    				  		}

				    				  		// $rewies['review_image'] = $row1['review_image'];
				    				  		array_push ( $rewies1, $rewies );
				    				  		if ($row1 ['review_rating'] > 0) {
				    				  			$averageRating = $averageRating + $row1 ['review_rating'];
				    				  			$i ++;
				    				  		}
				    				  	}
				    				  }
				    				  if ($averageRating > 0) {
				    				  	$finalRating = $averageRating / $i;
				    				  } else {
				    				  	$finalRating = 0;
				    				  }

				    				  if ($countIs == 0) {
				    				  	$countIs = 0;
				    				  }

				    				  $menu = array ();
				    				  $menu1 = array ();
				    				  $selectMenu = mysqli_query ( $GLOBALS ['link'], "SELECT ce_photos.pho_id AS menu_id ,ce_photos.pho_outlet_id , ce_photos.photo_type , ce_photos.pho_name , ce_photos.pho_name AS menu_image , ce_photos.created_at AS created_at , ce_photos.updated_at AS updated_at FROM ce_photos  WHERE (pho_outlet_id = '" . $outletId . "' OR pho_offer_id = '".$offerRow ['off_id']."') AND photo_type = 'menu' AND (approved = 1 OR is_approved = 1)" );
				    				  if (mysqli_num_rows ( $selectMenu ) > 0) {
				    				  	// /echo mysqli_num_rows($selectTags);
				    				  	while ( $tagRow = mysqli_fetch_array ( $selectMenu ) ) {
				    				  		$menu_photos = explode ( ",", $tagRow ['menu_image'] );
				    				  		$menu1 ['menu_id'] = $tagRow ['menu_id'];
				    				  		// $menu1['menu_title'] = $tagRow['menu_title'];
				    				  		// $menu1['menu_type'] = $tagRow['menu_type'];
				    				  		$url = trim ( preg_replace ( '/\s\s+/', ' ', $GLOBALS ['url'] ) );

				    				  		if ($tagRow ['menu_image'] != '') {
				    				  			//$menu1 ['menu_image'] = $url . $outletId . '/' . $tagRow ['menu_image']; // $menu_photos;
				    				  			$menu1 ['menu_image'] = $url . $tagRow ['pho_name'];
				    				  		} else {
				    				  			$menu1 ['menu_image'] = "'https://heuft.com/upload/image/400x267/no_image_placeholder.png"; // $menu_photos;
				    				  		}
				    				  		$menu1 ['created_at'] = $tagRow ['created_at'];
				    				  		$menu1 ['updated_at'] = $tagRow ['updated_at'];

				    				  		array_push ( $menu, $menu1 );
				    				  		// /$menu['menu_images'] = $menu_photos ;
				    				  	}
				    				  }

				    				  $photo = array ();
				    				  $selectPhoto = mysqli_query ( $GLOBALS ['link'], "SELECT ce_photos.pho_id AS menu_id ,ce_photos.pho_outlet_id , ce_photos.photo_type , ce_photos.pho_name , ce_photos.pho_name AS menu_image , ce_photos.created_at AS created_at , ce_photos.updated_at AS updated_at FROM ce_photos  WHERE (pho_outlet_id = '" . $outletId . "' OR pho_offer_id = '".$offerRow ['off_id']."') AND (photo_type = 'photos' OR photo_type = 'photo') AND (approved = 1 OR is_approved = 1)" );
				    				  if (mysqli_num_rows ( $selectPhoto ) > 0) {
				    				  	// /echo mysqli_num_rows($selectTags);$GLOBALS['baseUrl'].$outletThumb['pho__outlet_id'].'/'.$outletThumb['photo_type'].'/'.$outletThumb['pho__name']
				    				  	while ( $tagRow = mysqli_fetch_array ( $selectPhoto ) ) {
				    				  		$singleTagArray = array ();
				    				  		$singleTagArray ['menu_id'] = $tagRow ['menu_id'];
				    				  		$url = trim ( preg_replace ( '/\s\s+/', ' ', $GLOBALS ['url'] ) );
				    				  		if ($tagRow ['menu_image'] != '') {
				    				  			//$singleTagArray ['menu_image'] = $url . $tagRow ['pho_outlet_id'] . '/' . $tagRow ['photo_type'] . '/' . $tagRow ['pho_name'];
				    				  			$singleTagArray ['menu_image'] = $url . $tagRow ['pho_name'];
				    				  		} else {
				    				  			$singleTagArray ['menu_image'] = "https://heuft.com/upload/image/400x267/no_image_placeholder.png";
				    				  		}
				    				  		$singleTagArray ['created_at'] = $tagRow ['created_at'];
				    				  		$singleTagArray ['updated_at'] = $tagRow ['updated_at'];
				    				  		array_push ( $photo, $singleTagArray );
				    				  	}
				    				  }

				    				  $outletArray ['average_rating'] = $finalRating;
				    				  $outletArray ['votes'] = $countIs;
				    				  $outletArray ['offers'] = $offers;
				    				  $outletArray ['tags'] = $tags;
				    				  $outletArray ['menu'] = $menu;
				    				  $outletArray ['photos'] = $photo;
				    				  $outletArray ['reviews'] = $rewies1;

				    				  array_push ( $all_outlets, $outletArray );
				    			}
				    		}
				    	}
				    }

				    $delete = array ();

				    $dashboardMenu = array ();
				    $selectDelete = mysqli_query ( $GLOBALS ['link'], "SELECT record_id FROM migration_statements WHERE updated_at >= '" . $updatedAt . "' AND table_name = 'add_category'" );
				    if (mysqli_num_rows ( $selectDelete ) > 0) {
				    	while ( $deleteRow = mysqli_fetch_assoc ( $selectDelete ) ) {
				    		array_push ( $dashboardMenu, $deleteRow['record_id'] );
				    	}
				    }
				    $delete ['dashboard_menu'] = $dashboardMenu;

				    $outletSubType = array ();
				    $selectDelete = mysqli_query ( $GLOBALS ['link'], "SELECT record_id FROM migration_statements WHERE updated_at >= '" . $updatedAt . "' AND table_name = 'tbl_sub_category'" );
				    if (mysqli_num_rows ( $selectDelete ) > 0) {
				    	while ( $deleteRow = mysqli_fetch_assoc ( $selectDelete ) ) {
				    		array_push ( $outletSubType, $deleteRow['record_id'] );
				    	}
				    }
				    $delete ['outlet_type'] = $outletSubType;

				    $promotionalVideos = array ();
				    $selectDelete = mysqli_query ( $GLOBALS ['link'], "SELECT record_id FROM migration_statements WHERE updated_at >= '" . $updatedAt . "' AND table_name = 'ce_videos'" );
				    if (mysqli_num_rows ( $selectDelete ) > 0) {
				    	while ( $deleteRow = mysqli_fetch_assoc ( $selectDelete ) ) {
				    		array_push ( $promotionalVideos, $deleteRow['record_id'] );
				    	}
				    }
				    $delete ['promotional_videos'] = $promotionalVideos;

				    $faq = array ();
				    $selectDelete = mysqli_query ( $GLOBALS ['link'], "SELECT record_id FROM migration_statements WHERE updated_at >= '" . $updatedAt . "' AND table_name = 'ce_faq'" );
				    if (mysqli_num_rows ( $selectDelete ) > 0) {
				    	while ( $deleteRow = mysqli_fetch_assoc ($selectDelete ) ) {
				    		array_push ( $faq, $deleteRow['record_id']  );
				    	}
				    }
				    $delete ['faq'] = $faq;

				    $vouchers = array ();
				    $selectDelete = mysqli_query ( $GLOBALS ['link'], "SELECT record_id FROM migration_statements WHERE updated_at >= '" . $updatedAt . "' AND table_name = 'ce_vouchers'" );
				    if (mysqli_num_rows ( $selectDelete ) > 0) {
				    	while ( $deleteRow = mysqli_fetch_assoc ( $selectDelete ) ) {
				    		array_push ( $vouchers, $deleteRow['record_id']  );
				    	}
				    }
				    $delete ['vouchers'] = $vouchers;

				    $offers = array ();
				    $selectDelete = mysqli_query ( $GLOBALS ['link'], "SELECT record_id FROM migration_statements WHERE updated_at >= '" . $updatedAt . "' AND (table_name = 'ce_offers' )" ); ///OR table_name = 'ce_outlet'
				    if (mysqli_num_rows ( $selectDelete ) > 0) {
				    	while ( $deleteRow = mysqli_fetch_assoc ( $selectDelete) ) {
				    		array_push ( $offers, $deleteRow['record_id']  );
				    	}
				    }
				    $delete ['all_offers'] = $offers;



				    $offer = array ();
				    $selectDelete = mysqli_query ( $GLOBALS ['link'], "SELECT record_id FROM migration_statements WHERE updated_at >= '" . $updatedAt . "' AND table_name = 'ce_offers' " );
				    if (mysqli_num_rows ( $selectDelete ) > 0) {
				    	while ( $deleteRow = mysqli_fetch_assoc ( $selectDelete) ) {
				    		array_push ( $offer, $deleteRow['record_id']  );
				    	}
				    }
				    // $delete ['offers'] = $offer;


				    $outlet = array ();
				    $selectDelete = mysqli_query ( $GLOBALS ['link'], "SELECT record_id FROM migration_statements WHERE updated_at >= '" . $updatedAt . "' AND  table_name = 'ce_outlet'" );
				    if (mysqli_num_rows ( $selectDelete ) > 0) {
				    	while ( $deleteRow = mysqli_fetch_assoc ( $selectDelete) ) {
				    		array_push ( $outlet, $deleteRow['record_id']  );
				    	}
				    }
				    //   $delete ['outlets'] = $outlet;

				    $response ["all_offers"] = $all_outlets;
				    $response ["dashboard_menu"] = $menues;
				    $response ["feeds"] = $feeds;
				    //$response ["outlet_type"] = $outletListTypes;
				    $response ["outlet_type"] = $newArray;
				    // $response["outlets_list"] = $outlets_list;
				    $response ["nearBy_locations"] = $nearByOutlets;
				    $response ["news"] = $news_list;
				    $response ["profile"] = $userData;
				    $response ["promotional_videos"] = $videos_list;
				    // $response["pin_images"] = $pinImagesLst;
				    $response ["faq"] = $faq_list;
				    // $response["terms_and_conditions"] = $terms_list;
				    $response ["vouchers"] = $vouchers_list;
				    $response ["bookmarks"] = $bookmarks_list; // $this->addRemoveBookmark(array());
				    $response ["sponsors"] = $sponsors;
				    $response ["delete_statements"] = $delete;

				    if (count ( $all_outlets ) > 0 || count ( $delete ) > 0 || count ( $menues ) > 0 || count ( $feeds->local_feeds ) > 0 || count ( $feeds ) > 0 || count ( $feeds->my_history ) > 0 || count ( $subCategories ) > 0 || count ( $outlets_list ) > 0 || count ( $nearByOutlets ) > 0 || count ( $news_list ) > 0 || count ( $userData ) > 0 || count ( $videos_list ) > 0 || count ( $faq_list ) > 0 || count ( $terms_list ) > 0 || count ( $vouchers_list ) > 0 || count ( $bookmarks_list ) > 0 || count ( $sponsors ) > 0 || ($response ["user_pin"] != "" || ! is_null ( $response ["user_pin"] )) || ($response ["helpline"] != "" || ! is_null ( $response ["helpline"] ))) {
				    	$milliseconds = round ( microtime ( true ) * 1000 );
				    	$response ["updated_at"] = $milliseconds;
				    	$this->sendGeneralResponse ( "Below is the updated data.", HTTP_OK, $response );
				    } else {
				    	$this->sendGeneralResponse ( "Already uptodated.", 111 );
				    }
			} else {
				$this->sendGeneralResponse ( "Invalid userid", HTTP_SYNC_SETUP_INVALID_USER_ID );
			}
		}
	}
	function registerUser($postData) // postdata will be an array
	{
		// $expectedKeys = array("path" , "userId", "username" , "firstName" , "lastName" , "password" , "aboutMe" , "phoneNo" , "nationality" , "profilePhoto" , "city" , "employername");
		$expectedKeys = array (
				"path",
				"email",
				"username",
				"password",
				"secretPin"
		);
		$requiredKeys = array (
				"path",
				"email",
				"username",
				"password",
				"secretPin"
		);
		$numericKeys = array ();
		if ($this->validateInput ( $postData, $expectedKeys, $requiredKeys, $numericKeys )) {
			// array key names must be same as column names
			// table primary key must be at first index
			$user_data = array (
					'email' => $postData ['email']
			);
			$milliseconds = round ( microtime ( true ) * 1000 );
			$userResponse = $this->getRecords ( $user_data, "applicants" );
			if ($userResponse != 'not_found') {
				$res = password_hash ( $postData ['password'], PASSWORD_BCRYPT, array (
						'cost' => 12
				) );
				$person_data = array (
						'id' => 0,
						'fname' => $userResponse ['response'] [0] ['fname'],
						'lname' => $userResponse ['response'] [0] ['lname'],
						'email' => $postData ['email'],
						'username' => $postData ['username'],
						'password' => $res,
						'pin' => $postData ['secretPin'],
						'created_at' => $milliseconds,
						'updated_at' => $milliseconds
				);
				// 'password'=>md5($postData['password'])
				// $user_id = $postData['userId'];
				// first parameter will be the array of record to insert/update second table name third primery key forth if any value is unique set 1 else 0 unique key name
				$response = $this->insertUpdateRecord ( $person_data, "users", "", "1", "email", "" );
				if ($response === "exist") {
					$this->sendGeneralResponse ( "Record Already Exist", HTTP_REGISTER_ALREADY_REGISTER );
				} else {
					if ($response) {
						$this->sendGeneralResponse ( "Congratulations! You've signed up succesfully.", HTTP_OK );
					} else // failure
					{
						$this->interServerError ();
					}
				}
			} else {
				$this->sendGeneralResponse ( "Please apply first to register.", HTTP_REGISTER_INVALID_EMAIL );
			}
		}
	}
	function saveDeviceId($postData) // postdata will be an array
	{
		$expectedKeys = array (
				"path",
				"deviceId",
				"userId"
		);
		$requiredKeys = array (
				"path",
				"deviceId",
				"userId"
		);
		$numericKeys = array ();
		if ($this->validateInput ( $postData, $expectedKeys, $requiredKeys, $numericKeys )) {

			$selectDevice = mysqli_query ( $GLOBALS ['link'], "SELECT * FROM device_tokens WHERE device_id = '" . $postData ['deviceId'] . "'" );
			if (mysqli_num_rows ( $selectDevice ) > 0) {
				$this->sendGeneralResponse ( "Device already exist.", HTTP_OK );
			} else {

				$person_data = array (
						'token_id' => 0,
						'device_id' => $postData ['deviceId'],
						'user_id' => $postData ['userId'],
						'status' => 1
				);

				$response = $this->insertUpdateRecord ( $person_data, "device_tokens", "", "0", "", "" );
				if ($response === "exist") {
					$this->sendGeneralResponse ( "Record Already Exist", RECORD_ALREADY_EXIST );
				} else {
					if ($response) {
						$this->sendGeneralResponse ( "Device Id added successfully.", HTTP_OK );
					} else // failure
					{
						$this->interServerError ();
					}
				}

				// $this->sendGeneralResponse("Device already exist." , HTTP_OK);
			}
		}
	}
	function forgetPassword($postData) // postdata will be an array
	{
		$dataKeys = array (
				"path",
				"email"
		); // Possible Keys
		$requiredKeys = array (
				"path",
				"email"
		); // Required Keys
		$numericKeys = array (); // Numeric Keys
		if ($this->validateInput ( $postData, $dataKeys, $requiredKeys, $numericKeys )) {
			$email = $postData ['email'];
			$person_data = array (
					'email' => $email
			);

			$response = $this->getRecords ( $person_data, "users" ); // first parameter will be the array of record to insert/update second table name third primery key forth if any value is unique set 1 else 0 unique key name

			if ($response == "not_found") {
				$this->sendGeneralResponse ( "We're sorry but we don't recognize this email, please use your registered email (email that you've provided during sign up process).).", HTTP_FORGET_PASSWORD_INVALID_EMAIL );
			} else {

				if ($response) {
					$emailResponse = $this->sendEmail ( $response ['response'] [0] ['email'], $response ['response'] [0] ['email'], $response ['response'] [0] ['id'] );
					if ($emailResponse == 1) {
						$this->sendGeneralResponse ( "Email has been sent on your account .", HTTP_OK );
					} else {
						$this->sendGeneralResponse ( "Sorry we are unable to send email.", HTTP_BAD_GATEWAY );
					}
				} else // failure
				{
					$this->interServerError ();
				}
			}
		}
	}
	function getTrendingThisWeek($postData) {
		$dataKeys = array (
				"path"
		); // Possible Keys
		$requiredKeys = array (
				"path"
		); // Required Keys
		$numericKeys = array (); // Numeric Keys
		if ($this->validateInput ( $postData, $dataKeys, $requiredKeys, $numericKeys )) {

			$selectTrends = "SELECT distinct  his.* , outlet.out__name , outlet.created_at , outlet.updated_at , photo.pho__name FROM ce_history his ,ce_offers offer , ce_outlet outlet , ce_photos photo WHERE his.outlet_id=offer.off__outlet_id AND offer.off__expiry>=CURDATE() AND his.outlet_id=outlet.out__id AND photo.pho__outlet_id=his.outlet_id AND his.redeamed_at BETWEEN CURDATE() - INTERVAL 7 DAY AND CURDATE() ORDER BY his.visit_count DESC LIMIT 0,10";
			if (mysqli_query ( $GLOBALS ['link'], $selectTrends )) {
				$records = mysqli_query ( $GLOBALS ['link'], $selectTrends );
				if (mysqli_num_rows ( $records ) > 0) {
					$trendingWeeks = array ();
					while ( $rec = mysqli_fetch_assoc ( $records ) ) {
						array_push ( $trendingWeeks, $rec );
					}

					$response = array ();
					$response ["trending_this_week"] = $trendingWeeks;
					$this->sendGeneralResponse ( "Trends in this week .", HTTP_OK, $response );
				} else {
					$this->sendGeneralResponse ( "Sorry, no record found.", HTTP_NOT_FOUND );
				}
			} else // failure
			{
				$this->interServerError ();
			}
		}
	}
	function getSponsers($postData) {
		$dataKeys = array (
				"path",
				"userId"
		); // Possible Keys
		$requiredKeys = array (
				"path",
				"userId"
		); // Required Keys
		$numericKeys = array (); // Numeric Keys
		if ($this->validateInput ( $postData, $dataKeys, $requiredKeys, $numericKeys )) {
			$userId = $postData ['userId'];
			$selectTrends = "SELECT outlet.out__id AS outlet_id , outlet.out__name AS outlet_name , outlet.out__description AS outlet_description , outlet.outlet__thumb_url AS outlet_banner_url , outlet.created_at , outlet.updated_at , offer.off__id AS offer_id FROM ce_offers offer , ce_outlet outlet , ce_sponsors spons WHERE spons.outlet_id=offer.off__outlet_id AND offer.off__expiry>=CURDATE() AND spons.outlet_id=outlet.out__id";
			if (mysqli_query ( $GLOBALS ['link'], $selectTrends )) {
				$records = mysqli_query ( $GLOBALS ['link'], $selectTrends );
				if (mysqli_num_rows ( $records ) > 0) {
					$trendingWeeks = array ();
					while ( $rec = mysqli_fetch_assoc ( $records ) ) {
						$offerId = $rec ['offer_id'];
						$rec [] = '';
						$select = mysqli_query ( $GLOBALS ['link'], "SELECT * FROM ce_favourites WHERE fav__user_id = '" . $userId . "' AND fav__offer_id = '" . $offerId . "'" );
						if (mysqli_num_rows ( $select ) > 0) {
							$rec ['is_bookmarked'] = 1;
						} else {
							$rec ['is_bookmarked'] = 0;
						}

						array_push ( $trendingWeeks, $rec );
					}

					$response = array ();
					$response ["sponsors_list"] = $trendingWeeks;
					$this->sendGeneralResponse ( "Below are some sponsors.", HTTP_OK, $response );
				} else {
					$this->sendGeneralResponse ( "Sorry, no record found.", HTTP_NOT_FOUND );
				}
			} else // failure
			{
				$this->interServerError ();
			}
		}
	}
	function regenerateOTP($postData) // postdata will be an array
	{
		$dataKeys = array (
				"path",
				"email",
				"password"
		); // Possible Keys
		$requiredKeys = array (
				"path",
				"email",
				"password"
		); // Required Keys
		$numericKeys = array (); // Numeric Keys
		if ($this->validateInput ( $postData, $dataKeys, $requiredKeys, $numericKeys )) {
			$email = $postData ['email'];
			$password = $postData ['password'];
			$userId = '';
			$person_data = array (
					'email' => $email
			);
			$response = $this->getRecords ( $person_data, "users" );
			if (isset ( $response ['response'] [0] ['password'] ) && $response ['response'] [0] ['password'] != '') {
				$resPassword = $response ['response'] [0] ['password'];
			} else {
				$resPassword = "";
			}
			if (! password_verify ( $postData ['password'], $resPassword )) {
				$this->sendGeneralResponse ( "Invalid email or password, if you've forgot the password use the Forgot Password link given below to reset the password or if you have not signed up yet please sign up first.", HTTP_UNAUTHORIZED );
			} else {
				$email = $postData ['email'];
				$password = $postData ['password'];
				$generateOTP = substr ( number_format ( time () * rand (), 0, '', '' ), 0, 6 );
				$created_at = round ( microtime ( true ) * 1000 );
				if (mysqli_query ( $GLOBALS ['link'], "UPDATE users set opt_number = '" . $generateOTP . "'  , otp_created_at = '" . $created_at . "' WHERE email = '" . $email . "'" )) {
					if ($this->sendEmail ( $email, "", $generateOTP, "", "", 2 )) {

						$userId = $response ['response'] [0] ['id'];
						$userPin = $response ['response'] [0] ['pin'];

						$airmiles = '';
						$selectUser = mysqli_query ( $GLOBALS ['link'], "SELECT airmiles FROM `users` WHERE `id` = '" . $userId . "'" );
						if (mysqli_num_rows ( $selectUser ) > 0) {
							$row1 = mysqli_fetch_assoc ( $selectUser );
							$airmiles = $row1 ['airmiles'];
						}
						$response = array ();
						$response ["user_id"] = $userId;
						$response ["user_pin"] = $userPin;
						$response ["airmiles"] = $airmiles;
						$this->sendGeneralResponse ( "An OTP has been sent at your email.", HTTP_OK, $response );
					} else {
						$this->interServerError ();
					}
				}
			}
		}
	}
	function userLogin($postData) // postdata will be an array
	{
		$dataKeys = array (
				"path",
				"email",
				"password",
				'myLocation'
		); // Possible Keys
		$requiredKeys = array (
				"path",
				"email",
				"password",
				'myLocation'
		); // Required Keys
		$numericKeys = array (); // Numeric Keys
		if ($this->validateInput ( $postData, $dataKeys, $requiredKeys, $numericKeys )) {

			$email = $postData ['email'];
			$password = $postData ['password'];
			$userId = '';
			$latLong = explode ( ',', $postData ['myLocation'] );
			$lat = $latLong [0];
			$lng = $latLong [1];
			$userId = '';

			$person_data = array (
					'email' => $email
			);






			$response = $this->getRecords ( $person_data, "users" ); // first parameter will be the array of record to insert/update second table name third primery key forth if any value is unique set 1 else 0 unique key name
			if ($response == "not_found") {
				$this->sendGeneralResponse ( "This is a members only application, kindly contact the ClubExec office for membership enquiries or account activation if already registered.", HTTP_VERIFY_MEMBERSHIP_INVALID_EMAIL );
			} else {
				$selectUser = mysqli_query ( $GLOBALS ['link'], "SELECT * FROM `applicants` WHERE `email` = '" . $email . "' AND status = 1 AND approved=1" );
				$rows = mysqli_num_rows ( $selectUser );

				if($rows<1 )
				{
					$this->sendGeneralResponse ( "Your account is blocked please contact ClubExec office for activation.", '113' );
				}
				else
				{

					if (isset ( $response ['response'] [0] ['password'] ) && $response ['response'] [0] ['password'] != '') {
						$resPassword = $response ['response'] [0] ['password'];
					} else {
						$resPassword = "";
					}

					if (! password_verify ( $postData ['password'], $resPassword )) {
						$this->sendGeneralResponse ( "Invalid email or password, if you've forgot the password use the Forgot Password link given below to reset the password or if you have not signed up yet please sign up first.", HTTP_USER_LOGIN_INVALID_EMAIL );
						exit ();
					} else {
						if ($postData ['deviceToken'] == '') {

							$selectUser = "SELECT * FROM users WHERE email='" . $postData ['email'] . "'";
							$resultUser = mysqli_query ( $GLOBALS ['link'], $selectUser ); // /WHERE distance < 5
							$userRow = mysqli_fetch_assoc ( $resultUser );

							if (! empty ( $userRow ['device_token'] )) {

								$userId = $response ['response'] [0] ['id'];
								$userPin = $response ['response'] [0] ['pin'];

								$airmiles = '';
								$selectUser = mysqli_query ( $GLOBALS ['link'], "SELECT airmiles FROM `users` WHERE `id` = '" . $userId . "'" );
								if (mysqli_num_rows ( $selectUser ) > 0) {
									$row1 = mysqli_fetch_assoc ( $selectUser );
									$airmiles = $row1 ['airmiles'];
								}
								$response = array ();
								$response ["user_id"] = $userId;
								$response ["user_pin"] = $userPin;
								$response ["airmiles"] = $airmiles;
								$this->sendGeneralResponse ( "Sorry you can use this app in one device at a time, it seems you have already signed in, in another device, are you sure you want to use app in this device?", HTTP_USER_LOGIN_INVALID_DEVICE_TOKEN, $response );
							} else {
								$generateOTP = substr ( number_format ( time () * rand (), 0, '', '' ), 0, 6 );
								$created_at = round ( microtime ( true ) * 1000 );
								if (mysqli_query ( $GLOBALS ['link'], "UPDATE users set opt_number = '" . $generateOTP . "' , otp_created_at = '" . $created_at . "' WHERE email = '" . $postData ['email'] . "'" )) {
									if ($this->sendEmail ( $postData ['email'], "", $generateOTP, "", "", 2 )) {
										$userId = $response ['response'] [0] ['id'];
										$userPin = $response ['response'] [0] ['pin'];

										$airmiles = '';
										$selectUser = mysqli_query ( $GLOBALS ['link'], "SELECT airmiles FROM `users` WHERE `id` = '" . $userId . "'" );
										if (mysqli_num_rows ( $selectUser ) > 0) {
											$row1 = mysqli_fetch_assoc ( $selectUser );
											$airmiles = $row1 ['airmiles'];
										}
										$response = array ();
										$response ["user_id"] = $userId;
										$response ["user_pin"] = $userPin;
										$response ["airmiles"] = $airmiles;
										$this->sendGeneralResponse ( "An OTP has been sent at your email.", HTTP_USER_LOGIN_OTP_SENT, $response );
									} else {
										$this->interServerError ();
									}
								}
							}
						} else {
							$select = "SELECT * FROM users WHERE device_token = '" . $postData ['deviceToken'] . "' AND email='" . $postData ['email'] . "'";
							$result = mysqli_query ( $GLOBALS ['link'], $select ); // /WHERE distance < 5
							if (mysqli_num_rows ( $result ) > 0) {
								$userId = $response ['response'] [0] ['id'];
								$userPin = $response ['response'] [0] ['pin'];

								$airmiles = '';
								$selectUser = mysqli_query ( $GLOBALS ['link'], "SELECT airmiles FROM `users` WHERE `id` = '" . $userId . "'" );
								if (mysqli_num_rows ( $selectUser ) > 0) {
									$row1 = mysqli_fetch_assoc ( $selectUser );
									$airmiles = $row1 ['airmiles'];
								}
								$response = array ();
								$response ["user_id"] = $userId;
								$response ["user_pin"] = $userPin;
								$response ["airmiles"] = $airmiles;
								$this->sendGeneralResponse ( "LoggedIn successfully .", HTTP_OK, $response );
							} else {
								$this->sendGeneralResponse ( "Sorry you can use this app in one device at a time, it seems you have already signed in, in another device, are you sure you want to use app in this device?", HTTP_INVALID_DEVICE_TOKEN, '' );
							}
						}
					}
				}
			}
			//////
		}
	}
	function resetSecretPin($postData) // postdata will be an array
	{
		$dataKeys = array (
				"path",
				"email",
				"userId",
				"newPin"
		); // Possible Keys
		$requiredKeys = array (
				"path",
				"email",
				"userId",
				"newPin"
		); // Required Keys
		$numericKeys = array (); // Numeric Keys
		if ($this->validateInput ( $postData, $dataKeys, $requiredKeys, $numericKeys )) {
			$email = $postData ['email'];

			// $password = $postData['password'];
			$userId = '';
			$person_data = array (
					'email' => $email
			);
			$response = $this->getRecords ( $person_data, "users" );
			if ($response) {
				$result = mysqli_query ( $GLOBALS ['link'], "UPDATE users SET pin = '" . $postData ['newPin'] . "' WHERE id = '" . $postData ['userId'] . "' " ); // /WHERE distance < 5
				if ($result) {
					$this->sendGeneralResponse ( "Congratulations, secret pin changed successfully.", HTTP_OK );
				} else {
					$this->sendGeneralResponse ( "Sorry, due to some technical reasons we can't update your secret pin.", "114" );
				}

				/*
				 * $emailResponse = $this->sendEmail($response['response'][0]['email'] , $response['response'][0]['email'] , $response['response'][0]['id'] , "","", 1);
				 * if($emailResponse == 1)
				 	* {
				 	* $this->sendGeneralResponse("Email has been sent on your account ." , HTTP_OK);
				 	* }
				 * else
				 	* {
				 	* $this->sendGeneralResponse("Sorry we are unable to send email." , HTTP_BAD_GATEWAY);
				 	* }
				 */
			}
			/*
			 * if(isset($response['response'][0]['password']) && $response['response'][0]['password'] != '')
			 	* {
			 	* $resPassword = $response['response'][0]['password'];
			 	* }
			 * else
			 	* {
			 	* $resPassword = "";
			 	* }
			 *
			 * if(!password_verify($postData['password'], $resPassword))
			 	* {
			 	* $this->sendGeneralResponse("Invalid username or password" , HTTP_NOT_FOUND);
			 	* }
			 * else
			 	* {
			 	* $emailResponse = $this->sendEmail($response['response'][0]['email'] , $response['response'][0]['email'] , $response['response'][0]['id']);
			 	* if($emailResponse == 1)
			 		* {
			 		* $this->sendGeneralResponse("Email has been sent on your account ." , HTTP_OK);
			 		* }
			 	* else
			 		* {
			 		* $this->sendGeneralResponse("Sorry we are unable to send email." , HTTP_BAD_GATEWAY);
			 		* }
			 	*
			 	* }
			 */
		}
	}
	function getSubCategories($postData) // postdata will be an array
	{
		$dataKeys = array (
				"path",
				"menuId"
		); // Possible Keys
		$requiredKeys = array (
				"path",
				"menuId"
		); // Required Keys
		$numericKeys = array (); // Numeric Keys
		if ($this->validateInput ( $postData, $dataKeys, $requiredKeys, $numericKeys )) {
			$menuId = $postData ['menuId'];
			$select = "SELECT ce_category.cat__id AS cat_id , ce_subcategory.subcat__id AS subcat_id , ce_subcategory.subcat__name AS subcat_name FROM ce_subcategory JOIN ce_category ON ce_category.cat__menu__id=ce_subcategory.subcat__category_id WHERE ce_category.cat__menu__id = '3' GROUP BY ce_subcategory.subcat__name ORDER BY ce_subcategory.subcat__id";
			if (mysqli_query ( $GLOBALS ['link'], $select )) {
				$result = mysqli_query ( $GLOBALS ['link'], "SELECT ce_category.cat__id AS cat_id , ce_subcategory.subcat__id AS subcat_id , ce_subcategory.subcat__name AS subcat_name FROM ce_subcategory JOIN ce_category ON ce_category.cat__menu__id=ce_subcategory.subcat__category_id WHERE ce_category.cat__menu__id = '3' GROUP BY ce_subcategory.subcat__name ORDER BY ce_subcategory.subcat__id" ); // /WHERE distance < 5
				if (mysqli_num_rows ( $result ) > 0) {
					$response = array ();
					while ( $row = mysqli_fetch_object ( $result ) ) {
						array_push ( $response, $row );
					}

					$this->sendGeneralResponse ( "Below are some categories.", HTTP_OK, $response );
				} else {
					$this->sendGeneralResponse ( "Sorry, no sub category found.", HTTP_NOT_FOUND );
				}
			} else // failure
			{
				$this->interServerError ();
			}
		}
	}
	function getAllResturants($postData) // postdata will be an array
	{
		$dataKeys = array (
				"path"
		); // Possible Keys
		$requiredKeys = array (
				"path"
		); // Required Keys
		$numericKeys = array (); // Numeric Keys
		if ($this->validateInput ( $postData, $dataKeys, $requiredKeys, $numericKeys )) {

			$person_data = array ();
			$response = $this->getRecords ( $person_data, "company" ); // first parameter will be the array of record to insert/update second table name third primery key forth if any value is unique set 1 else 0 unique key name
			if ($response == "not_found") {
				$this->sendGeneralResponse ( "Sorry, no subcategory under this category.", HTTP_NOT_FOUND );
			} else {
				if ($response) {
					$this->sendGeneralResponse ( "Below are sub categories.", HTTP_OK, $response );
				} else // failure
				{
					$this->interServerError ();
				}
			}
		}
	}
	function getResturantAgainstSubCategory($postData) // postdata will be an array
	{
		$dataKeys = array (
				"path",
				"catId",
				"subCatId"
		); // Possible Keys
		$requiredKeys = array (
				"path",
				"catId",
				"subCatId"
		); // Required Keys
		$numericKeys = array (
				"userId"
		); // Numeric Keys
		if ($this->validateInput ( $postData, $dataKeys, $requiredKeys, $numericKeys )) {
			$catId = $postData ['catId'];
			$subCatId = $postData ['subCatId'];
			$select = "SELECT branch.branch__id, outlet.out__id AS out_id , outlet.out__name AS out_name , outlet.outlet__thumb_url AS    out_thumbnail , branch.branch__address AS outlet_address, outlet.created_at , outlet.updated_at , offer.off__name AS outlet_offer , review.review_rating AS outlet_rating, COUNT(review.review__id) AS total_votes_for_rating
    FROM ce_branch branch , ce_outlet outlet , ce_offers offer , ce_reviews review
    WHERE branch.branch__id = outlet.out__branch_id AND branch.branch__id=offer.off__branch_id AND outlet.out__id=offer.off__outlet_id AND outlet.out__id=review.review_outlet_id AND offer.off__category_id='" . $catId . "' AND offer.off__subcategory_id='" . $subCatId . "'";
			if (mysqli_query ( $GLOBALS ['link'], $select )) {
				$result = mysqli_query ( $GLOBALS ['link'], $select ); // /WHERE distance < 5
				if (mysqli_num_rows ( $result ) > 0) {
					$response = array ();
					while ( $row = mysqli_fetch_object ( $result ) ) {
						$creation_mili = strtotime ( $row->created_at ) * 1000;
						$row->created_at = $creation_mili;

						$updation_mili = strtotime ( $row->updated_at ) * 1000;
						$row->updated_at = $updation_mili;
						array_push ( $response, $row );
					}

					$this->sendGeneralResponse ( "Below are some categories.", HTTP_OK, $response );
				} else {
					$this->sendGeneralResponse ( "Sorry, no sub category found.", HTTP_NOT_FOUND );
				}
			} else // failure
			{
				$this->interServerError ();
			}
		}
	}
	function getOutletById($postData) // postdata will be an array
	{
		$dataKeys = array (
				"path",
				"outletId",
				"userId"
		); // Possible Keys
		$requiredKeys = array (
				"path",
				"outletId",
				"userId"
		); // Required Keys
		$numericKeys = array (); // Numeric Keys
		if ($this->validateInput ( $postData, $dataKeys, $requiredKeys, $numericKeys )) {
			$response = array ();
			$outleDetails = array ();
			$outleDetails1 = array ();
			$tags = array ();
			$airmiles = array ();
			$outletId = $postData ['outletId'];
			$userPin = $postData ['userId'];
			$select = "SELECT outlet.out__id AS outlet_id , outlet.out__name AS outlet_name  , outlet.outlet__thumb_url AS outlet_thumb , branch.branch__address AS outlet_address , day.days__open AS opening_timings ,  day.days__day AS opening_days , COUNT(review.review_rating) AS reviews_count  , branch.branch__telephone AS contact_no , branch.branch__description AS short_message , outlet.out__description AS description , branch.branch__lat AS latitude , branch.branch__lng AS longitude , branch.branch__website AS website_url , branch.branch__facebook AS fb_url , branch.branch__twitter AS twitter_url  , offer.off__name AS discount_details , offer.off__expiry AS offer_expiry , offer.off__terms AS terms_n_conditions , offer.off__specialterms AS imp_terms_n_conditions
    FROM ce_outlet outlet , ce_branch branch , ce_days day , ce_reviews review , ce_offers offer
    WHERE outlet.out__branch_id=branch.branch__id AND outlet.out__id='" . $outletId . "' AND day.days__outlet_id=outlet.out__id AND offer.off__outlet_id = outlet.out__id";
			if (mysqli_query ( $GLOBALS ['link'], $select )) {
				$result = mysqli_query ( $GLOBALS ['link'], $select ); // /WHERE distance < 5
				if (mysqli_num_rows ( $result ) > 0) {
					while ( $row = mysqli_fetch_object ( $result ) ) {
						array_push ( $outleDetails1, $row );
					}
					$outleDetails ['outletDetails'] = $outleDetails1;

					$selectTerms = mysqli_query ( $GLOBALS ['link'], "SELECT * FROM `ce_favourites` WHERE `fav__outlet_id` = '" . $outletId . "' AND `fav__user_id` = '" . $userPin . "'" );
					if (mysqli_num_rows ( $selectTerms ) > 0) {
						$outleDetails ['isBookmarked'] = 1;
					} else {
						$outleDetails ['isBookmarked'] = 0;
					}

					$selectTags = mysqli_query ( $GLOBALS ['link'], "SELECT tag.tag_id , tag.tag_name FROM ce_tags tag , tag_outlet_match tag_outle WHERE tag.tag_id = tag_outle.tag_id AND tag_outle.outlet_id = '" . $outletId . "'" );
					if (mysqli_num_rows ( $selectTags ) > 0) {
						// echo mysqli_num_rows($selectTags);
						while ( $tagRow = mysqli_fetch_object ( $selectTags ) ) {
							array_push ( $tags, $tagRow );
						}
					}

					$selectAirmiles = mysqli_query ( $GLOBALS ['link'], "SELECT member_airmile.airmiles_no AS airmiles_earned_now,  airmile.air__value AS total_airmiles
    FROM ce_airmiles airmile , ce_airmiles_outlet_match airmile_match , member_airmiles_match member_airmile
    WHERE airmile_match.airout__airmiles_id=airmile.air__id AND airmile_match.airout__outlet_id= '" . $outletId . "' AND member_airmile.membership_no= '" . $userPin . "'" );
					if (mysqli_num_rows ( $selectAirmiles ) > 0) {
						while ( $row1 = mysqli_fetch_object ( $selectAirmiles ) ) {
							array_push ( $airmiles, $row1 );
						}
					}
					$rewies = array ();
					$rewies1 = array ();
					$getCount = mysqli_query ( $GLOBALS ['link'], "SELECT * FROM ce_reviews WHERE review_outlet_id = '" . $outletId . "'" );
					$countIs = mysqli_num_rows ( $getCount );
					$selectReview = mysqli_query ( $GLOBALS ['link'], "SELECT * FROM `ce_reviews` WHERE `review_outlet_id` = '" . $outletId . "' ORDER BY review_rating DESC LIMIT 0,1" );
					// $rewies['totalNumberOfReviews'] = 2;//$getCount;
					if (mysqli_num_rows ( $selectReview ) > 0) {
						while ( $row1 = mysqli_fetch_assoc ( $selectReview ) ) {
							array_push ( $rewies1, $row1 );
						}
					}

					$response ['outleDetail'] = $outleDetails;
					$response ['tags'] = $tags;
					$response ['menu'] = '';
					$response ['photos'] = '';
					$rewies ['totalGainedRating'] = $countIs;
					$rewies ['reviews'] = $rewies1;
					$response ['review'] = $rewies;
					$this->sendGeneralResponse ( "Below are some promotions.", HTTP_OK, $response );
				}
			} else // failure
			{
				$this->interServerError ();
			}
		}
	}
	function reportError($postData) // postdata will be an array
	{
		$expectedKeys = array (
				"path",
				"reportId",
				"userId",
				"offerId",
				"comments",
				"phoneNumber",
				"address",
				"outletClosed",
				"menuIncorrect",
				"reviewRelated",
				"other",
				"socialMediaRelated",
				"mapRelated",
				"openingHours"
		);
		$requiredKeys = array (
				"path",
				"reportId",
				"userId"
		);
		$numericKeys = array ();
		if ($this->validateInput ( $postData, $expectedKeys, $requiredKeys, $numericKeys )) {
			// array key names must be same as column names
			// table primary key must be at first index
			$person_data = array (
					'report_id' => $postData ['reportId'],
					'report_userid' => $postData ['userId'],
					'report_resturant_code' => $postData ['offerId'],
					'report_comment' => mysqli_real_escape_string ( $GLOBALS ['link'], $postData ['comments'] ),
					'report_phone_number' => $postData ['phoneNumber'],
					'report_address' => $postData ['address'],
					'report_outlet_closed' => $postData ['outletClosed'],
					'report_menu_incorrect' => $postData ['menuIncorrect'],
					'report_review_related' => $postData ['reviewRelated'],
					'report_other' => $postData ['other'],
					'report_social_media' => $postData ['socialMediaRelated'],
					'report_map' => $postData ['mapRelated'],
					'report_opening_hours' => $postData ['openingHours'],
					'status' => 0
			);

			// first parameter will be the array of record to insert/update second table name third primery key forth if any value is unique set 1 else 0 unique key name
			$response = $this->insertUpdateRecord ( $person_data, "ce_report_errors", "", 0, '' );
			if ($response === "exist") {
				$this->sendGeneralResponse ( "Record Already Exist", RECORD_ALREADY_EXIST );
			} else {
				if ($response) {
					$this->sendGeneralResponse ( "Report submitted successfully.", HTTP_OK );
				} else // failure
				{
					$this->interServerError ();
				}
			}
		}
	}
	function writeReviews($postData) // postdata will be an array
	{
		$expectedKeys = array (
				"path",
				"reviewId",
				"outletId",
				"offerId",
				"userId",
				"reviewDate",
				"rating",
				"reviewImage",
				"review",
				"deviceToken"
		);

		$requiredKeys = array (
				"path",
				"reviewId",
				"outletId",
				"offerId",
				"userId",
				"reviewDate",
				"rating",
				"deviceToken"
		);
		$numericKeys = array ();
		if ($this->validateInput ( $postData, $expectedKeys, $requiredKeys, $numericKeys )) {
			$imgName = '';
			$finalName = '';
			if(isset($postData ['reviewImage']) && $postData ['reviewImage'] !='')
			{
				$img = $postData ['reviewImage'];
				$find = array (
						"data:image/png;base64,",
						"data:image/jpeg;base64,",
						"data:image/jpg;base64,"
				);
				$img1 = str_replace ( $find, '', $img );
				$img2 = str_replace ( ' ', '+', $img1 );
				$data = base64_decode ( $img2 );
				$imgName = $postData ['offerId'] . '_' . round ( microtime ( true ) * 1000 );
				$dataRating = $postData ['rating'];
				$dataBuild = $postData ['rating'];
				$explodeData = explode ( '.', $dataRating );
				$endData = end ( $explodeData );
				$ratingReview = intval ( $dataBuild ) . '.' . substr ( $endData, 0, 0 ); // $postData['rating'];
				$path = '/outlets/review_images/' . $imgName . '.png';
				$path1 = '/review_images/' . $imgName . '.png';
				file_put_contents ( '../../' . $path, $data );
				$finalName =  $imgName . '.png';
			}

			$person_data = array (
					'review_id' => $postData ['reviewId'],
					'review_review' => mysqli_real_escape_string ( $GLOBALS ['link'], $postData ['review'] ),
					'review_outlet_id' => $postData ['outletId'],
					'review_offer_id' => $postData ['offerId'],
					'review_user_id' => $postData ['userId'],
					'review_date' => $postData ['reviewDate'],
					'review_image' =>$finalName,
					'review_rating' => $postData ['rating']
			);

			// first parameter will be the array of record to insert/update second table name third primery key forth if any value is unique set 1 else 0 unique key name
			$response = $this->insertUpdateRecord ( $person_data, "ce_reviews", "", 0, '' );

			if ($response === "exist") {
				$this->sendGeneralResponse ( "Record Already Exist", RECORD_ALREADY_EXIST );
			} else {
				if ($response) {
					$respond = array ();
					// $respond['review_title'] = '';
					if ($img != '') {
						$respond ['image_url'] = $GLOBALS ['baseUrl'] . $path1;
					} else {
						$respond ['image_url'] = '';
					}
					$this->sendGeneralResponse ( "Review Added Successfully.", HTTP_OK, $respond );
					// $this->sendGeneralResponse("Review Added Successfully." , HTTP_OK);
				} else // failure
				{
					$this->interServerError ();
				}
			}
		}
	}
	function addPhoto($postData) // postdata will be an array
	{
		$expectedKeys = array (
				"path",
				"outletId",
				"userId",
				"photo"
		);

		$requiredKeys = array (
				"path",
				"outletId",
				"userId",
				"photo"
		);
		$numericKeys = array ();
		if ($this->validateInput ( $postData, $expectedKeys, $requiredKeys, $numericKeys )) {
			$img = $postData ['photo'];
			$milliseconds = round ( microtime ( true ) * 1000 );
			$find = array (
					"data:image/png;base64,",
					"data:image/jpeg;base64,",
					"data:image/jpg;base64,"
			);
			$img = str_replace ( $find, '', $img );
			$img = str_replace ( ' ', '+', $img );
			$data = base64_decode ( $img );
			$imgName = $postData ['outletId'] . '_' . round ( microtime ( true ) * 1000 );
			/* if (!is_dir('../../outlets/'. $postData ['outletId'])) {
				mkdir('../../outlets/'. $postData ['outletId'], 0777, true);
				}
				if (!is_dir('../../outlets/'. $postData ['outletId'] . '/photos')) {
				mkdir('../../outlets/'. $postData ['outletId'] . '/photos', 0777, true);
				}
				$path = '../../outlets/' . $postData ['outletId'] . '/photos/' . $imgName . '.png';
				$path1 = $postData ['outletId'] . '/photos/' . $imgName . '.png'; */
			//file_put_contents ( $path, $data );
			file_put_contents ( '../uploadDirectory/'. $imgName . '.png', $data );
			$response = mysqli_query ( $GLOBALS ['link'], 'INSERT INTO ce_photos (pho_user_id , pho_name , photo_type , pho_outlet_id , is_approved, created_at , updated_at ) VALUES("' . $postData ['userId'] . '" , "' . $imgName . '.png' . '", "photos" , "' . $postData ['outletId'] . '" , 0 , "' . $milliseconds . '" , "' . $milliseconds . '" )' );

			if ($response) {
				$respond = array ();
				//$respond ['image_url'] = $GLOBALS ['url'] . $imgName . '.png';

				$this->sendGeneralResponse ( "Photo Added Successfully.", HTTP_OK, $respond );
			} else // failure
			{
				$this->interServerError ();
			}
		}
	}
	function getVideosPromotionList($postData) // postdata will be an array
	{
		$dataKeys = array (
				"path"
		); // Possible Keys
		$requiredKeys = array (
				"path"
		); // Required Keys
		$numericKeys = array (); // Numeric Keys
		if ($this->validateInput ( $postData, $dataKeys, $requiredKeys, $numericKeys )) {
			$person_data = array (
					'video_status' => 1
			);
			$response = $this->getRecords ( $person_data, "ce_videos" ); // first parameter will be the array of record to insert/update second table name third primery key forth if any value is unique set 1 else 0 unique key name
			if ($response == "not_found") {
				$this->sendGeneralResponse ( "Sorry, no promotion found.", HTTP_NOT_FOUND );
			} else {
				if ($response) {
					for($i = 0; $i < count ( $response ['response'] ); $i ++) {
						$creation_mili = strtotime ( $response ['response'] [$i] ['video_creation_date'] ) * 1000;
						$response ['response'] [$i] ['video_creation_date'] = $creation_mili;

						$updation_mili = strtotime ( $response ['response'] [$i] ['video_update_date'] ) * 1000;
						$response ['response'] [$i] ['video_update_date'] = $updation_mili;
					}
					$this->sendGeneralResponse ( "Below are some promotions.", HTTP_OK, $response );
				} else // failure
				{
					$this->interServerError ();
				}
			}
		}
	}
	function getFaqs($postData) // postdata will be an array
	{
		$dataKeys = array (
				"path"
		); // Possible Keys
		$requiredKeys = array (
				"path"
		); // Required Keys
		$numericKeys = array (); // Numeric Keys
		if ($this->validateInput ( $postData, $dataKeys, $requiredKeys, $numericKeys )) {
			$person_data = array (
					'status' => 1
			);
			$response = $this->getRecords ( $person_data, "ce_faq" ); // first parameter will be the array of record to insert/update second table name third primery key forth if any value is unique set 1 else 0 unique key name

			if ($response == "not_found") {
				$this->sendGeneralResponse ( "Sorry, no faq found.", HTTP_NOT_FOUND );
			} else {

				if ($response) {
					$this->sendGeneralResponse ( "Below are some faqs.", HTTP_OK, $response );
				} else // failure
				{
					$this->interServerError ();
				}
			}
		}
	}
	function reward($postedData) // postdata will be an array
	{
		$expectedKeys = array (
				"path",
				"userId",
				"outletId",
				"userPin",
				"vendorPin"
		);
		$requiredKeys = array (
				"path",
				"userId",
				"outletId",
				"userPin",
				"vendorPin"
		);
		$numericKeys = array ();
		if ($this->validateInput ( $postedData, $expectedKeys, $requiredKeys, $numericKeys )) {
			$userPin = $postedData ['userPin'];
			$outletId = $postedData ['outletId'];
			$select = "SELECT member_airmile.airmiles_no AS airmiles_earned_now,  airmile.air__value AS total_airmiles
    FROM ce_airmiles airmile , ce_airmiles_outlet_match airmile_match , member_airmiles_match member_airmile
    WHERE airmile_match.airout__airmiles_id=airmile.air__id AND airmile_match.airout__outlet_id= '" . $outletId . "' AND member_airmile.membership_no= '" . $userPin . "'";
			if (mysqli_query ( $GLOBALS ['link'], $select )) {
				$result = mysqli_query ( $GLOBALS ['link'], $select ); // /WHERE distance < 5
				if (mysqli_num_rows ( $result ) > 0) {
					$response = array ();
					while ( $row = mysqli_fetch_object ( $result ) ) {
						array_push ( $response, $row );
					}

					$this->sendGeneralResponse ( "Below are some airmiles.", HTTP_OK, $response );
				} else {
					$this->sendGeneralResponse ( "Sorry, no airmiles found.", HTTP_NOT_FOUND );
				}
			} else // failure
			{
				$this->interServerError ();
			}
		}
	}
	function nearbyOutlet($postData) // postdata will be an array
	{
		$dataKeys = array (
				"path",
				"myLocation"
		); // Possible Keys
		$requiredKeys = array (
				"path",
				"myLocation"
		); // Required Keys
		$numericKeys = array (); // Numeric Keys
		if ($this->validateInput ( $postData, $dataKeys, $requiredKeys, $numericKeys )) {
			$latLong = explode ( ',', $postData ['myLocation'] );
			$lat = $latLong [0];
			$lng = $latLong [1];
			$select = "SELECT distinct  branch.branch__id, (3959 * acos(cos(radians('" . $lat . "')) * cos(radians(branch.branch__lat)) * cos( radians(branch.branch__lng) - radians('" . $lng . "')) + sin(radians('" . $lat . "')) * sin(radians(branch.branch__lat)))) AS outlet_distanc , outlet.out__id AS out_id , outlet.out__name AS out_name , outlet.outlet__thumb_url AS out_thumbnail , branch.branch__address AS outlet_address , CONCAT(branch.branch__lat , branch.branch__lng) AS location, review.review_rating AS outlet_rating FROM ce_branch branch , ce_outlet outlet , ce_offers offer , ce_reviews review WHERE branch.branch__id = outlet.out__branch_id AND branch.branch__id=offer.off__branch_id AND outlet.out__id=offer.off__outlet_id AND outlet.out__id=review.review_outlet_id having outlet_distanc < 50 ORDER BY outlet_distanc LIMIT 0 , 10";
			if (mysqli_query ( $GLOBALS ['link'], $select )) {
				$result = mysqli_query ( $GLOBALS ['link'], $select ); // /WHERE distance < 5
				if (mysqli_num_rows ( $result ) > 0) {
					$response = array ();
					while ( $row = mysqli_fetch_object ( $result ) ) {
						array_push ( $response, $row );
					}

					$this->sendGeneralResponse ( "Below are some nearest outlets.", HTTP_OK, $response );
				} else {
					$this->sendGeneralResponse ( "Sorry, no outlet found.", HTTP_NOT_FOUND );
				}
			} else // failure
			{
				$this->interServerError ();
			}
		}
	}
	function addRemoveBookmark($postData) // postdata will be an array
	{
		$expectedKeys = array (
				"path",
				"userId",
				"offerId",
				"isBookmarked"
		);
		$requiredKeys = array (
				"path",
				"userId",
				"offerId",
				"isBookmarked"
		);
		$numericKeys = array (
				"isBookmarked"
		);
		if ($this->validateInput ( $postData, $expectedKeys, $requiredKeys, $numericKeys )) {
			if ($postData ['isBookmarked'] == 0) {
				if (mysqli_query ( $GLOBALS ['link'], "DELETE FROM ce_favourites WHERE fav_user_id = '" . $postData ['userId'] . "' AND fav_offer_id = '" . $postData ['offerId'] . "'" )) {
					$this->sendGeneralResponse ( "Outlet has been removed.", HTTP_OK, array (
							"offerId" => $postData ['offerId']
					) );
				} else {
					$this->interServerError ();
				}
			} else {
				// array key names must be same as column names
				// table primary key must be at first index
				$milliseconds = round ( microtime ( true ) * 1000 );
				$person_data = array (
						'id' => 0,
						"fav_user_id" => $postData ['userId'],
						"fav_offer_id" => $postData ['offerId'],
						"created_at" => $milliseconds,
						"updated_at" => $milliseconds
				);

				// first parameter will be the array of record to insert/update second table name third primery key forth if any value is unique set 1 else 0 unique key name
				$response = $this->insertUpdateRecord ( $person_data, "ce_favourites" );
				if ($response === "exist") {
					$this->sendGeneralResponse ( "Record Already Exist", RECORD_ALREADY_EXIST );
				} else {
					if ($response) {
						$this->sendGeneralResponse ( "Outlet has been bookmarked.", HTTP_OK, array (
								"offerId" => $postData ['offerId']
						) );
					} else // failure
					{
						$this->interServerError ();
					}
				}
			}
		}
	}
	function getProfile($postData) // postdata will be an array
	{
		$dataKeys = array (
				"path",
				"userId"
		); // Possible Keys
		$requiredKeys = array (
				"path",
				"userId"
		); // Required Keys
		$numericKeys = array (); // Numeric Keys
		if ($this->validateInput ( $postData, $dataKeys, $requiredKeys, $numericKeys )) {

			$userId = $postData ['userId'];
			$select = "SELECT user.fname AS first_name , user.lname AS last_name , user.username ,  app.mobile AS phone_no, app.nationality AS nationality , app.emirate AS city, app.employer AS employer_name , app.membership_no AS membershipNo , app.membership_type AS memberShipType , app.applicant_photo , app.membership_expiry AS membershipExpiry , app.applicant_photo AS applicantPhoto FROM users user ,applicants app WHERE app.email=user.email AND user.id = '" . $userId . "'";
			if (mysqli_query ( $GLOBALS ['link'], $select )) {
				$result = mysqli_query ( $GLOBALS ['link'], $select ); // /WHERE distance < 5
				if (mysqli_num_rows ( $result ) > 0) {
					$response = array ();
					$termsAndCondition = array ();
					/*
					 * while($row = mysqli_fetch_assoc($result))
					 	* {
					 	* unset($row['password']);
					 	* array_push($response , $row);
					 	* $response['termsAndCondition'] = mysqli_fetch_assoc($result);
					 	* }
					 */
					 $selectTerms = mysqli_query ( $GLOBALS ['link'], "SELECT content_text AS generalTerms FROM ce_content WHERE content_type='Terms & Conditions' AND content_status=1" );
					 if (mysqli_num_rows ( $selectTerms ) > 0) {
					 	$row1 = mysqli_fetch_assoc ( $selectTerms );
					 	$terms = mb_convert_encoding ( $row1 ['generalTerms'], "UTF-8", "HTML-ENTITIES" );
					 	$termsAndCondition ['generalTerms'] = $terms;
					 }
					 	
					 $selectTotalAirmielsData = "SELECT * FROM ce_airmiles_transactions WHERE user_id = '" . $userId . "' AND redemption_type = 1";
					 $getTotalResult = mysqli_query ( $GLOBALS ['link'], $selectTotalAirmielsData ); // /WHERE distance < 5
					 $totalAirmies = 0;
					 if (mysqli_num_rows ( $getTotalResult ) > 0) {

					 	while ( $row = mysqli_fetch_array ( $getTotalResult ) ) {
					 		$totalAirmies = $totalAirmies + $row ['airmiles_earned'];
					 	}
					 }
					 	
					 // // $profile = mysqli_fetch_assoc($resultProfile);
					 $profile = array ();
					 $userData = array ();
					 while ( $row = mysqli_fetch_assoc ( $result ) ) {

					 	$userData ['first_name'] = $row ['first_name'];
					 	$userData ['last_name'] = $row ['last_name'];
					 	$userData ['username'] = $row ['username'];
					 	$userData ['phone_no'] = $row ['phone_no'];
					 	$userData ['nationality'] = $row ['nationality'];
					 	$city = '';
					 	//	echo  "SELECT city_name FROM `ce_city` WHERE `city_id` = '".$row ['city']."'" ;
					 	$selectedIage = mysqli_query ( $GLOBALS ['link'], "SELECT city_name FROM `ce_city` WHERE `city_id` = '".$row ['city']."'" );
					 	//echo 'count '. mysqli_num_rows ( $selectedIage );
					 	if (mysqli_num_rows ( $selectedIage ) > 0) {
					 		$row1 = mysqli_fetch_assoc ( $selectedIage );
					 		$city =  $row1 ['city_name'];
					 	}
					 	else
					 		$city = '';

					 		$userData ['city'] = $city;
					 		$userData ['employer_name'] = $row ['employer_name'];
					 		$userData ['memberShipId'] = $row ['membershipNo'];
					 		$userData ['memberShipType'] = $row ['memberShipType'];
					 		$userData ['membershipExpiry'] = $row ['membershipExpiry'];
					 		$baseUrlForImage = '';
					 		$selectBaseUrlForImage = mysqli_query ( $GLOBALS ['link'], "SELECT content_text FROM `ce_content` WHERE `content_type` = 'Member Photos URL'" );
					 		if (mysqli_num_rows ( $selectBaseUrlForImage ) > 0) {
					 			$row1 = mysqli_fetch_assoc ( $selectBaseUrlForImage );
					 			$baseUrlForImage = $row1 ['content_text'];
					 		}
					 		if($row['applicant_photo'] !='')
					 			$userData ['applicantPhoto'] = $GLOBALS['url'].$row['applicant_photo'];
					 			else
					 				$userData ['applicantPhoto'] = 'http://www.clubexec.net/web/cms/images/avatar.png';

					 				///$userData ['applicantPhoto'] = $baseUrlForImage . '/' . $row ['membershipNo'] . '.jpg';

					 				// /array_push($profile , $userData);
					 }
					 	
					 $response ['profile'] = $userData;
					 $response ['airmiles'] = $totalAirmies;
					 	
					 $termsAndCondition ['impTerms'] = '';
					 $response ['termsAndCondition'] = $termsAndCondition;
					 // /$response['impTerms'] = $termsAndCondition;*/
					 	
					 $this->sendGeneralResponse ( "Below is the user profile.", HTTP_OK, $response );
				} else {
					$this->sendGeneralResponse ( "Sorry, no record found.", HTTP_GET_PROFILE_INVALID_USER_ID );
				}
			} else // failure
			{
				$this->interServerError ();
			}
		}
	}
	function updateProfile($postData) // postdata will be an array
	{
		$dataKeys = array (
				"path",
				"userId",
				"username",
				"firstName",
				"lastName",
				"phoneNo",
				"nationality",
				"city",
				"employername"
		); // Possible Keys
		$requiredKeys = array (
				"path",
				"userId",
				"username",
				"firstName",
				"lastName",
				"phoneNo",
				"nationality",
				"city",
				"employername"
		); // Required Keys
		$numericKeys = array (); // array("userId"); //Numeric Keys
		if ($this->validateInput ( $postData, $dataKeys, $requiredKeys, $numericKeys )) {
			if (mysqli_query ( $GLOBALS ['link'], "UPDATE users SET username = '" . $postData ['username'] . "' , fname = '" . $postData ['firstName'] . "'  , lname = '" . $postData ['lastName'] . "' WHERE id='" . $postData ['userId'] . "'" )) {
				$slectedAccountId = mysqli_query ( $GLOBALS ['link'], "SELECT email FROM users WHERE id = '" . $postData ['userId'] . "'" );
				$selectAccountId = mysqli_fetch_assoc ( $slectedAccountId );
				$memberShipNo = $selectAccountId ['email'];

				mysqli_query ( $GLOBALS ['link'], "UPDATE applicants SET nationality = '" . $postData ['nationality'] . "' , emirate = '" . $postData ['city'] . "' , employer = '" . $postData ['employername'] . "' , mobile = '" . $postData ['phoneNo'] . "' WHERE email = '" . $memberShipNo . "' " );
				$this->sendGeneralResponse ( "Profile has been updated successfully.", HTTP_OK );
			} else // failure
			{
				$this->interServerError ();
			}
		}
	}
	function getTermsAndCondition($postData) // postdata will be an array
	{
		$dataKeys = array (
				"path"
		); // Possible Keys
		$requiredKeys = array (
				"path"
		); // Required Keys
		$numericKeys = array (); // Numeric Keys
		if ($this->validateInput ( $postData, $dataKeys, $requiredKeys, $numericKeys )) {

			$response = array ();
			$select = "SELECT content_text AS generalTerms FROM ce_content WHERE content_type='Terms & Conditions' AND content_status=1";
			$result = mysqli_query ( $GLOBALS ['link'], $select ); // /WHERE distance < 5
			if (mysqli_num_rows ( $result ) > 0) {
				while ( $row = mysqli_fetch_assoc ( $result ) ) {
					$terms = mb_convert_encoding ( $row ['generalTerms'], "UTF-8", "HTML-ENTITIES" );

					$response ['generalTerms'] = $terms;
				}
			} else {
				$response ['generalTerms'] = '';
			}

			/*
			 * $selectImp = "SELECT content_text AS impTerms FROM ce_content WHERE content_type='impTerms' AND content_status=1";
			 * $resultImp = mysqli_query( $GLOBALS['link'] , $selectImp) ; ///WHERE distance < 5
			 * if(mysqli_num_rows($resultImp) > 0)
			 	* {
			 	* while($rowImp = mysqli_fetch_assoc($resultImp))
			 		* {
			 		*
			 		* $response['impTerms'] = $rowImp['impTerms'];
			 		* }
			 	* }
			 * else
			 	* {
			 	* $response['impTerms'] = "";
			 	* }
			 */
			 	
			 $this->sendGeneralResponse ( "Below are some categories.", HTTP_OK, $response );
		}
	}
	function getAboutUs($postData) // postdata will be an array
	{
		$dataKeys = array (
				"path"
		); // Possible Keys
		$requiredKeys = array (
				"path"
		); // Required Keys
		$numericKeys = array (); // Numeric Keys
		if ($this->validateInput ( $postData, $dataKeys, $requiredKeys, $numericKeys )) {
			$person_data = array (
					'content_type' => 'about',
					'content_status' => 1
			);
			$response = $this->getRecords ( $person_data, "ce_content" ); // first parameter will be the array of record to insert/update second table name third primery key forth if any value is unique set 1 else 0 unique key name
			if ($response == "not_found") {
				$this->sendGeneralResponse ( "Sorry, no content.", HTTP_NOT_FOUND );
			} else {
				if ($response) {
					$this->sendGeneralResponse ( "Below is content for abount us.", HTTP_OK, $response );
				} else // failure
				{
					$this->interServerError ();
				}
			}
		}
	}
	function getNews($postData) // postdata will be an array
	{
		$dataKeys = array (
				"path"
		); // Possible Keys
		$requiredKeys = array (
				"path"
		); // Required Keys
		$numericKeys = array (); // Numeric Keys
		if ($this->validateInput ( $postData, $dataKeys, $requiredKeys, $numericKeys )) {
			$person_data = array (
					'news_status' => 1
			);
			$response = $this->getRecords ( $person_data, "ce_news" ); // first parameter will be the array of record to insert/update second table name third primery key forth if any value is unique set 1 else 0 unique key name
			if ($response == "not_found") {
				$this->sendGeneralResponse ( "Sorry, no content.", HTTP_NOT_FOUND );
			} else {
				if ($response) {
					$this->sendGeneralResponse ( "Below is content for news.", HTTP_OK, $response );
				} else // failure
				{
					$this->interServerError ();
				}
			}
		}
	}
	function getVouchersList($postData) // postdata will be an array
	{
		$dataKeys = array (
				"path"
		); // Possible Keys
		$requiredKeys = array (
				"path"
		); // Required Keys
		$numericKeys = array (); // Numeric Keys
		if ($this->validateInput ( $postData, $dataKeys, $requiredKeys, $numericKeys )) {
			$select = "SELECT outlet.out__name AS outlet_name , outlet.out__id AS outlet_id , branch.branch__address AS address , rev.review_rating AS rating , outlet.outlet__thumb_url AS photo FROM ce_outlet outlet , ce_branch branch , ce_reviews rev , ce_vouchers vouch WHERE outlet.out__branch_id = branch.branch__id AND rev.review_outlet_id=outlet.out__id AND vouch.vouch__outlet_id=outlet.out__id";
			if (mysqli_query ( $GLOBALS ['link'], $select )) {
				$result = mysqli_query ( $GLOBALS ['link'], $select ); // /WHERE distance < 5
				if (mysqli_num_rows ( $result ) > 0) {
					$response = array ();
					while ( $row = mysqli_fetch_object ( $result ) ) {
						array_push ( $response, $row );
					}

					$this->sendGeneralResponse ( "Below are some nearest outlets.", HTTP_OK, $response );
				} else {
					$this->sendGeneralResponse ( "Sorry, no outlet found.", HTTP_NOT_FOUND );
				}
			} else // failure
			{
				$this->interServerError ();
			}
		}
	}
	function getVoucherDetail($postedData) // postdata will be an array
	{
		$dataKeys = array (
				"path",
				"vouchersCode"
		); // Possible Keys
		$requiredKeys = array (
				"path",
				"vouchersCode"
		); // Required Keys
		$numericKeys = ""; // array("userId"); //Numeric Keys
		if ($this->validateInput ( $postData, $dataKeys, $requiredKeys, $numericKeys )) {
			$person_data = array (
					'voucher_code' => $postedData ["vouchersCode"]
			);
			$response = $this->getRecords ( $person_data, "resturant" ); // first parameter will be the array of record to insert/update second table name third primery key forth if any value is unique set 1 else 0 unique key name
			if ($response == "not_found") {
				$this->sendGeneralResponse ( "Sorry, no resturant under this category.", HTTP_NOT_FOUND );
			} else {
				if ($response) {
					$this->sendGeneralResponse ( "Below are some resturants.", HTTP_OK, $response );
				} else // failure
				{
					$this->interServerError ();
				}
			}
		}
	}
	function getCities($postData) // postdata will be an array
	{
		$dataKeys = array (
				"path"
		); // Possible Keys
		$requiredKeys = array (
				"path"
		); // Required Keys
		$numericKeys = array (); // array("userId"); //Numeric Keys
		if ($this->validateInput ( $postData, $dataKeys, $requiredKeys, $numericKeys )) {
			$person_data = array ();

			// /'voucher_code'=>$postedData["vouchersCode"]

			$response = $this->getRecords ( $person_data, "ce_city" ); // first parameter will be the array of record to insert/update second table name third primery key forth if any value is unique set 1 else 0 unique key name
			if ($response == "not_found") {

				$this->sendGeneralResponse ( "Sorry, no city found", HTTP_NOT_FOUND );
			} else {
				if ($response) {
					$this->sendGeneralResponse ( "Below are all cities.", HTTP_OK, $response );
				} else // failure
				{
					$this->interServerError ();
				}
			}
		}
	}
	function getEmployers($postData) // postdata will be an array
	{
		$dataKeys = array (
				"path"
		); // Possible Keys
		$requiredKeys = array (
				"path"
		); // Required Keys
		$numericKeys = array (); // array("userId"); //Numeric Keys
		if ($this->validateInput ( $postData, $dataKeys, $requiredKeys, $numericKeys )) {
			$select = "SELECT distinct employer FROM applicants";
			if (mysqli_query ( $GLOBALS ['link'], $select )) {
				$result = mysqli_query ( $GLOBALS ['link'], $select ); // /WHERE distance < 5
				if (mysqli_num_rows ( $result ) > 0) {
					$response = array ();
					while ( $row = mysqli_fetch_object ( $result ) ) {
						array_push ( $response, $row );
					}

					$this->sendGeneralResponse ( "Below are all employers.", HTTP_OK, $response );
				} else {
					$this->sendGeneralResponse ( "Sorry, no employer found.", HTTP_NOT_FOUND );
				}
			} else // failure
			{
				$this->interServerError ();
			}
		}
	}
	function sendVoucherDetail($postedData) // postdata will be an array
	{
		$dataKeys = array (
				"path",
				"username",
				"voucherCode",
				"firstCode",
				"secondCode"
		); // Possible Keys
		$requiredKeys = array (
				"path",
				"username",
				"voucherCode",
				"firstCode",
				"secondCode"
		); // Required Keys
		$numericKeys = ""; // array("userId"); //Numeric Keys
		if ($this->validateInput ( $postData, $dataKeys, $requiredKeys, $numericKeys )) {
			$person_data = array (

					'username' => $postData ['username'],
					'voucher_code' => $postData ['voucherCode'],
					'first_code' => $postData ['firstCode'],
					'second_code' => $postData ['secondCode']
			);

			$response = $this->insertUpdateRecord ( $person_data, "users", "userId", 0, '' );
			if ($response === "exist") {
				$this->sendGeneralResponse ( "Record Already Exist", RECORD_ALREADY_EXIST );
			} else {
				if ($response) {
					$this->sendGeneralResponse ( "Order has been registered.", HTTP_OK );
				} else // failure
				{
					$this->interServerError ();
				}
			}
		}
	}
	function createUser($postData) // postdata will be an array
	{
		$dataKeys = array (
				"path",
				"userId",
				"username",
				"email",
				"password",
				"tag1",
				"tag2"
		);
		$requiredKeys = array (
				"path",
				"userId",
				"username",
				"email",
				"password"
		);
		$numericKeys = array (
				"userId"
		);
		if ($this->validateInput ( $postData, $dataKeys, $requiredKeys, $numericKeys )) {
			// array key names must be same as column names
			// table primary key must be at first index
			$person_data = array (
					'id' => $postData ['userId'] == '' ? 0 : $postData ['userId'],
					'username' => $postData ['username'] == '' ? null : $postData ['username'],
					'email' => $postData ['email'],
					'password' => md5 ( $postData ['password'] ),
					'tag1' => $postData ['tag1'],
					'tag2' => $postData ['tag2']
			);
			$userId = $postData ['userId'];
			// first parameter will be the array of record to insert/update second table name third primery key forth if any value is unique set 1 else 0 unique key name
			$response = $this->insertUpdateRecord ( $person_data, "users", $userId, 1, 'email' );
			if ($response === "exist") {
				$this->sendGeneralResponse ( "Record Already Exist", HTTP_BAD_REQUEST );
			} else {
				if ($response) {
					$this->successMessage ();
				} else // failure
				{
					$this->interServerError ();
				}
			}
		}
	}
	function deleteUser($postData) // postdata will be an array
	{
		$dataKeys = array (
				"path",
				"userId"
		);
		$requiredKeys = array (
				"path",
				"userId"
		);
		$numericKeys = array (
				"userId"
		);
		if ($this->validateInput ( $postData, $dataKeys, $requiredKeys, $numericKeys )) {
			$user_id = $postData ['userId'];

			if ($this->deleteRecord ( "users", $user_id, 'id' )) // /table name and record id and primery are parameters
			{
				$this->successMessage ();
			} else // failure
			{
				$this->interServerError ();
			}
		}
	}
	function deleteUsers($postData) // postdata will be an array
	{
		$dataKeys = array (
				"path",
				"userIds"
		);
		$requiredKeys = array (
				"path",
				"userIds"
		);
		$numericKeys = array ();
		if ($this->validateInput ( $postData, $dataKeys, $requiredKeys, $numericKeys )) {
			if ($this->deleteRecords ( "users", $postData ['userIds'], 'id' )) // /table name and record ids(array) and primery are parameters
			{
				$this->successMessage ();
			} else // failure
			{
				$this->interServerError ();
			}
		}
	}
	function getUsers($postData) // postdata will be an array
	{
		$dataKeys = array (
				"path",
				"userId",
				"username",
				"email",
				"password",
				"tag1",
				"tag2"
		); // Possible Keys
		$requiredKeys = array (
				"path"
		); // Required Keys
		$numericKeys = array (
				"userId"
		); // Numeric Keys
		if ($this->validatePartialInput ( $postData, $dataKeys, $requiredKeys, $numericKeys )) {
			if (isset ( $postData ['userId'] ) && $postData ['userId'] > 0) {
				$userId = $postData ['userId'];
			} else {
				$userId = '';
			}

			if (isset ( $postData ['username'] ) && $postData ['username'] != "") {
				$username = $postData ['username'];
			} else {
				$username = '';
			}

			if (isset ( $postData ['email'] ) && $postData ['email'] != "") {
				$email = $postData ['email'];
			} else {
				$email = '';
			}

			if (isset ( $postData ['tag1'] ) && $postData ['tag1'] != "") {
				$tag1 = $postData ['tag1'];
			}

			else {
				$tag1 = '';
			}

			if (isset ( $postData ['tag2'] ) && $postData ['tag2'] != "") {
				$tag2 = $postData ['tag2'];
			} else {
				$tag2 = '';
			}

			$person_data = array (
					'id' => $userId,
					'username' => $username,
					'email' => $email,
					'tag1' => $tag1,
					'tag2' => $tag2
			);
			$response = $this->getRecords ( $person_data, "users" ); // first parameter will be the array of record to insert/update second table name third primery key forth if any value is unique set 1 else 0 unique key name
			if ($response == "not_found") {
				$this->sendGeneralResponse ( "Record Not Found.", HTTP_BAD_REQUEST );
			} else {
				if ($response) {
					$this->sendResponse ( $response );
				} else // failure
				{
					$this->interServerError ();
				}
			}
		}
	}

	// ////////////////////////////////////////GENERAL FUNCTIONS////////////////////////////////////////////////////////
	function sendEmail($to, $username = "", $message, $cc = "", $bcc = "", $mode = "") {

		// $to = $_POST['user_email'];
		// echo $message;
		$from = "support@clubexec.net";
		$subject = '';
		$body = '';
		$headers = "From: " . strip_tags ( "support@clubexec.net" ) . "\r\n";
		$headers .= "Reply-To: " . strip_tags ( "no-reply@clubexec.net" ) . "\r\n";
		$headers .= "Return-Path: myplace@clubexec.net\r\n";
		$headers .= "MIME-Version: 1.0\r\n";
		// /$headers .= '-f info@website.com';

		$headers .= "Content-Type: text/html; charset=UTF-8\r\n";
		switch ($mode) // /To go in specific function according to request
		{

			case '1' :
				{
					$subject = 'ClubExec Reset Secret Pin';
					$body .= '<p>Please click <a href="https://www.clubexec.net/web/cms/securedApi/resest_pin.php?uId=' . $message . '">here</a> to reset secret Pin</p>';
					break;
				}
					
			case '2' :
				{
					$subject = 'ClubExec OTP Number';
					$body .= '<p>Please use the below <b>One Time Password</b> to validate your email address<br> Your OTP is: ' . $message . '</p>';
					break;
				}
					
			default :
				{

					$subject = 'ClubExec Reset Password';
					$body .= '<p>Please click <a href="https://www.clubexec.net/web/cms/securedApi/resest_password.php?uId=' . $message . '">here</a> to reset password</p>';
					break;
				}
		}
		if (mail ( $to, $subject, $body, $headers )) {
			return 1;
		} else {
			return 0;
		}
	}
	function validateDeviceToken($postKeys = array()) {

		// exit;
		if ($postKeys ['path'] == 'registerUser' || $postKeys ['path'] == 'forgetPassword' || $postKeys ['path'] == 'verifyMemberShip' || $postKeys ['path'] == 'userLogin' || $postKeys ['path'] == 'regenerateOTP') {
			return true;
			exit ();
		}
		if (isset ( $postKeys ['deviceToken'] ) && $postKeys ['deviceToken'] != '') {

			if ($postKeys ['deviceToken'] == "0") {
				$this->sendGeneralResponse ( "Please enter one time OTP sent at your provided email and mobile.", HTTP_BAD_REQUEST );
				exit ();
			} else {

				if (isset ( $postKeys ['userId'] ) && $postKeys ['userId'] != '') {
					$where = "(id = '" . $postKeys ['userId'] . "'";
					if (isset ( $postKeys ['email'] ) && $postKeys ['email'] != '') {
						$where .= " OR email = '" . $postKeys ['email'] . "')";
					} else {
						$where .= ")";
					}

					$select = "SELECT * FROM users WHERE device_token = '" . $postKeys ['deviceToken'] . "' AND " . $where . "";
					$result = mysqli_query ( $GLOBALS ['link'], $select ); // /WHERE distance < 5
					if (mysqli_num_rows ( $result ) > 0 || $postKeys ['deviceToken'] == '123456') {
						return true;
					} else {
						return false;
					}
				}

				else {
					$this->sendGeneralResponse ( "Please provide device token", HTTP_NOT_FOUND );
				}
			}
		} else {
			$this->sendGeneralResponse ( "Please provide device token", HTTP_NOT_FOUND );
			exit ();
		}
	}
	function validateInput($postKeys = array(), $dataKeys = array(), $requiredKeys = array(), $numericKeys = array()) {
		$invalidKeys = array ();
		$missingValues = array ();
		$numericMissing = array ();
		$missingKeys = array ();
		if ($this->validateDeviceToken ( $postKeys )) {

			if (count ( $postKeys ) > 0) {
				foreach ( $dataKeys as $dataKey => $dataValue ) {
					if (! array_key_exists ( $dataValue, $postKeys )) {
						array_push ( $missingKeys, $dataValue );
					}
				}
				array_push ( $dataKeys, 'email' );
				array_push ( $dataKeys, 'userId' );
				array_push ( $dataKeys, 'deviceToken' );
				foreach ( $postKeys as $key => $value ) {
					if (! in_array ( $key, $dataKeys )) {
						array_push ( $invalidKeys, $key );
					} else {
						if (in_array ( $key, $requiredKeys )) {
							if ($value != '') {
								if (in_array ( $key, $numericKeys )) {
									if (! is_numeric ( $value )) {
										array_push ( $numericMissing, $key );
									}
								}
							} else {
								array_push ( $missingValues, $key );
							}
						}
					}
				}

				if (count ( $invalidKeys ) > 0 || count ( $numericMissing ) > 0 || count ( $missingValues ) > 0 || count ( $missingKeys ) > 0) {
					$response = array ();
					$response ['statusCode'] = HTTP_INVALID_JSON;
					$response ['status'] = false;
					$response ['message'] = 'Something is wrong with provided json.';
					$error = array ();
					if (count ( $missingKeys ) > 0) {
						$error ['missingKeysMsg'] = 'Below keys are missing.';
						$error ['missingKeys'] = $missingKeys;
					}
					if (count ( $invalidKeys ) > 0) {
						$error ['invalidKeysMsg'] = 'Below keys are invalid.';
						$error ['invalidKeys'] = $invalidKeys;
					}
					if (count ( $missingValues ) > 0) {
						$error ['requiredKeyMsg'] = 'Below keys must have values.';
						$error ['requiredKeys'] = $missingValues;
					}
					if (count ( $numericMissing ) > 0) {
						$error ['numericKeyMsg'] = 'Below keys must have numeric values.';
						$error ['numericMissing'] = $numericMissing;
					}

					$response ['error'] = $error;
					$this->encodeJson ( $response );
				} else {
					return true;
				}
			}
		} else {
			$this->sendGeneralResponse ( "Sorry you can use this app in one device at a time, it seems you have already signed in, in another device, are you sure you want to use app in this device?", HTTP_INVALID_DEVICE_TOKEN );
		}
	}
	function validatePartialInput($postKeys = array(), $dataKeys = array(), $requiredKeys = array(), $numericKeys = array()) {
		$invalidKeys = array ();
		$missingValues = array ();
		$numericMissing = array ();
		if (count ( $postKeys ) > 0) {
			foreach ( $postKeys as $key => $value ) {
				if (! in_array ( $key, $dataKeys )) {
					array_push ( $invalidKeys, $key );
				} else {
					if (in_array ( $key, $requiredKeys )) {
						if ($value != '') {
							if (in_array ( $key, $numericKeys )) {
								if (! is_numeric ( $value ) && $value > 0) {
									array_push ( $numericMissing, $key );
								}
							}
						} else {
							array_push ( $missingValues, $key );
						}
					}
				}
			}

			if (count ( $invalidKeys ) > 0 || count ( $numericMissing ) > 0 || count ( $missingValues ) > 0) {
				$response = array ();
				$response ['statusCode'] = HTTP_INVALID_JSON;
				$response ['status'] = false;
				$response ['message'] = 'Something is wrong with provided json.';
				$error = array ();
				if (count ( $invalidKeys ) > 0) {
					$error ['invalidKeysMsg'] = 'Below keys are invalid.';
					$error ['invalidKeys'] = $invalidKeys;
				}
				if (count ( $missingValues ) > 0) {
					$error ['requiredKeyMsg'] = 'Below keys must have values.';
					$error ['requiredKeys'] = $missingValues;
				}
				if (count ( $numericMissing ) > 0) {
					$error ['numericKeyMsg'] = 'Below keys must have numeric values.';
					$error ['numericMissing'] = $numericMissing;
				}

				$response ['error'] = $error;
				echo json_encode ( $response );
			} else {
				return true;
			}
		}
	}
	function insertUpdateRecord($data = array(), $table_name = "", $record_id = "", $is_unique = "", $unique_key_name = "", $condition = "") {
		$column = '';
		$columnValue = '';
		$updateQuery = '';
		$first_key = key ( $data );
		$first_value = $data [$first_key];
		unset ( $data [$first_key] );
		$dataCount = count ( $data );
		$count = 0;
		foreach ( $data as $key => $value ) {
			$count ++;
			if ($dataCount == $count) {
				$column .= $key;
				$columnValue .= "'" . $value . "'";
				$updateQuery .= "" . $key . " = '" . $value . "'";
			} else {
				$column .= $key . ' ,';
				$columnValue .= "'" . $value . "' , ";
				$updateQuery .= "" . $key . " = '" . $value . "' , ";
			}
		}
		if (isset ( $first_value ) && $first_value > 0) {

			if ($is_unique == 1) {
				if ($this->isUnique ( $table_name, $unique_key_name, $data [$unique_key_name] )) {
					return 'exist';
				} else {
					return mysqli_query ( $GLOBALS ['link'], "UPDATE " . $table_name . " SET  " . $updateQuery . " WHERE " . $first_key . " = '" . $first_value . "'" );
				}
			} else {
				return mysqli_query ( $GLOBALS ['link'], "UPDATE " . $table_name . " SET  " . $updateQuery . " WHERE " . $first_key . " = '" . $first_value . "'" );
			}
		} else {
			if ($is_unique == 1) {

				if ($this->isUnique ( $table_name, $unique_key_name, $data [$unique_key_name] )) {
					return 'exist';
				} else {
					return mysqli_query ( $GLOBALS ['link'], "INSERT INTO " . $table_name . " (" . $column . ") VALUES(" . $columnValue . ")" );

				}
			} else {
				return mysqli_query ( $GLOBALS ['link'], "INSERT INTO " . $table_name . " (" . $column . ") VALUES(" . $columnValue . ")" );

			}
		}
	}
	function isUnique($table_name, $column_name, $value) {
		$sqlQuery = mysqli_query ( $GLOBALS ['link'], "SELECT " . $column_name . " FROM " . $table_name . " WHERE " . $column_name . " = '" . $value . "'" );
		if (mysqli_num_rows ( $sqlQuery ) > 0) {
			return true;
		} else {
			return false;
		}
	}
	function deleteRecord($tbl_name, $record_id, $primeryColumn) {
		return mysqli_query ( $GLOBALS ['link'], "DELETE FROM " . $tbl_name . " WHERE " . $primeryColumn . " = '" . $record_id . "'" );
	}
	function deleteRecords($tbl_name, $record_ids, $primeryColumn) {
		if (count ( $record_ids ) > 0) {
			$dataCount = count ( $record_ids );
			$count = 0;
			$idString = '';
			foreach ( $record_ids as $idKey => $idValue ) {
				$count ++;
				if ($dataCount == $count) {
					$idString .= $idValue;
				} else {
					$idString .= $idValue . ' , ';
				}
			}
			return mysqli_query ( $GLOBALS ['link'], "DELETE FROM " . $tbl_name . " WHERE " . $primeryColumn . " IN(" . $idString . ")" );
		}
		return false;
	}
	function getRecords($record_data, $table_name) {
		$where = "";
		$dataCount = count ( $record_data );
		$count = 0;
		foreach ( $record_data as $idKey => $idValue ) {
			$count ++;
			if ($idValue != "") {
				if ($dataCount == $count) {
					$where .= "" . $idKey . " = '" . $idValue . "' ";
				} else {
					$where .= "" . $idKey . " = '" . $idValue . "' AND ";
				}
			}
		}
		// echo $where;
		$split = explode ( " ", $where );
		if ($split [count ( $split ) - 2] == 'AND') {
			$last_space_position = strrpos ( $where, ' ' );
			$where = substr ( $where, 0, $last_space_position - 3 );
		}

		if ($where != "") {
			// echo "SELECT * FROM ".$table_name." WHERE ".$where."";
			$selectRecord = mysqli_query ( $GLOBALS ['link'], "SELECT * FROM " . $table_name . "  WHERE " . $where . "" );
		} else {
			$selectRecord = mysqli_query ( $GLOBALS ['link'], "SELECT * FROM " . $table_name . "" );
		}
		if (mysqli_num_rows ( $selectRecord ) > 0) {
			$responseArray = array ();
			$response = array ();
			$responseArray ['count'] = mysqli_num_rows ( $selectRecord );
			while ( $row = mysqli_fetch_assoc ( $selectRecord ) ) {
				array_push ( $response, $row );
			}
			$responseArray ['response'] = $response;
			return $responseArray;
		} else {
			return "not_found";
		}
	}
	function sendResponse($json_data) {
		$response = array ();
		$data = array ();
		$response ['statusCode'] = HTTP_OK;
		$response ['status'] = true;
		$response ['message'] = 'Action performed successfully';
		$response ['data'] = $json_data;
		$this->encodeJson ( $response );
	}
	function sendGeneralResponse($msg = "", $code = "", $json_data = "") {
		$response = array ();
		$data = array ();
		$response ['statusCode'] = $code;
		$response ['status'] = true;
		$response ['message'] = $msg;
		$response ['data'] = $json_data;
		header ( 'Content-Type: application/json; Charset="UTF-8"' );
		$arr = $this->convertArrayKeysToUtf8 ( $response );
		$this->encodeJson ( $arr );
	}
	function convertArrayKeysToUtf8(array $array) {
		$convertedArray = array ();
		foreach ( $array as $key => $value ) {
			if (! mb_check_encoding ( $key, 'UTF-8' ))
				$key = utf8_encode ( $key );
				if (is_array ( $value ))
				{
					$value = $this->convertArrayKeysToUtf8 ( $value );
					$value = str_replace(array( "\r\n" , "\n" , "\\r\\n"), '', $value);
				}
					
				$convertedArray [$key] = $value;
		}
		return $convertedArray;
	}
	function successMessage() {
		$response = array ();
		$data = array ();
		$response ['statusCode'] = HTTP_OK;
		$response ['status'] = true;
		$response ['message'] = 'Action make successfully';
		$response ['data'] = $data;
		$this->encodeJson ( $response );
	}
	function interServerError($data = "") {
		$response = array ();
		$data = array ();
		$response ['statusCode'] = HTTP_INTERNAL_SERVER_ERROR;
		$response ['status'] = false;
		$response ['message'] = 'Sorry due to internal server error we can not make your request successfull.';
		$response ['data'] = $data;
		$this->encodeJson ( $response );
	}
	function missingKeys() {
		$response = array ();
		$data = array ();
		$response ['statusCode'] = HTTP_BAD_REQUEST;
		$response ['status'] = false;
		$response ['message'] = 'Some required keys/values are missing.';
		$response ['data'] = $data;
		$this->encodeJson ( $response );
	}
	function isDataVerified($postData, $headerInfo) {
		if ($GLOBALS ['security'] == 1) {
			$param1 = '';
			$param2 = '';
			if (isset ( $headerInfo ['Param1'] )) {
				$param1 = $headerInfo ['Param1'];
			} else {
				$param1 = '';
			}
			if (isset ( $headerInfo ['param2'] )) {
				$param2 = $headerInfo ['param2'];
			} else {
				$param2 = '';
			}
			$hashedString = hash ( 'sha256', json_encode ( $postData ) );
			if ($param1 == $hashedString) {
				return true;
			} else {
				return false;
			}
		} else {
			return true;
		}
	}
	function decodeJson($postdata1) {
		if ($GLOBALS ['security'] == 1) {

			$encyptedKey = AES::keyGen ( $GLOBALS ['encrypted_key'] );
			$postData = json_decode ( AES::decryptAes ( $encyptedKey, $postdata1, $GLOBALS ['encyption_iv'] ), true );
			return $postData;
		} else {
			$postData = json_decode ( $postdata1, true );
			return $postData;
		}
	}
	function encodeJson($response) {
		$responseData = array ();
		if (isset ( $GLOBALS ['security'] ) && $GLOBALS ['security'] == 1) {
			header ( 'Content-Type: text/plain' );
			header ( 'hash: ' . hash ( 'sha256', json_encode ( $response ) ) );
			$encyptedKey = AES::keyGen ( $GLOBALS ['encrypted_key'] );
			$responseData = AES::encryptAes ( json_encode ( $response ), $encyptedKey );
		} else {
			header ( 'Content-Type: application/json' );
			$responseData = json_encode ( $response );
		}
		print_r ( $responseData );
		exit ();
	}
	function authentiate_user($headers = array()) {
		if ($GLOBALS ['rest_auth'] == 'basic') {
			if ((isset ( $headers ['Authentication-Code'] ) && $headers ['Authentication-Code'] != '')) {
				$query = mysqli_query ( $GLOBALS ['link'], "SELECT authentication_key FROM api_authentication WHERE authentication_key = 123456789" );
				if (mysqli_num_rows ( $query ) > 0) {
					$row = mysqli_fetch_assoc ( $query );
					if (! isset ( $headers ['Content-Type'] )) {
						$response = array ();
						$data = array ();
						$response ['statusCode'] = HTTP_NOT_ACCEPTABLE;
						$response ['status'] = false;
						$response ['message'] = 'Missing content-type.';
						$response ['data'] = $data;
						echo json_encode ( $response );
					} else {
						if (isset ( $headers ['Content-Type'] ) && $headers ['Content-Type'] != 'application/json') {
							$response = array ();
							$data = array ();
							$response ['statusCode'] = HTTP_NOT_ACCEPTABLE;
							$response ['status'] = false;
							$response ['message'] = 'Invalid content type we support only application/json.';
							$response ['data'] = $data;
							echo json_encode ( $response );
						} else {
							$methods = $GLOBALS ['supported_methods'];
							$method = $_SERVER ['REQUEST_METHOD'];
							if (! in_array ( $method, $methods )) {
								$response = array ();
								$data = array ();
								$response ['statusCode'] = HTTP_METHOD_NOT_ALLOWED;
								$response ['status'] = false;
								$response ['message'] = 'Invalid request method we support only POST.';
								$response ['data'] = $data;
								echo json_encode ( $response );
							} else {
								return true;
							}
						}
					}
				} else {
					$response = array ();
					$data = array ();
					$response ['statusCode'] = HTTP_UNAUTHORIZED;
					$response ['status'] = false;
					$response ['message'] = 'Invalid authorization code.';
					$response ['data'] = $data;
					echo json_encode ( $response );
				}
			} else {
				$response = array ();
				$data = array ();
				$response ['statusCode'] = HTTP_NON_AUTHORITATIVE_INFORMATION;
				$response ['status'] = false;
				$response ['message'] = 'Authorization code is required, please set in header.';
				$response ['data'] = $data;
				echo json_encode ( $response );
			}
		} else {
			if (! isset ( $headers ['Content-Type'] )) {
				$response = array ();
				$data = array ();
				$response ['statusCode'] = HTTP_NOT_ACCEPTABLE;
				$response ['status'] = false;
				$response ['message'] = 'Missing content-type.';
				$response ['data'] = $data;
				echo json_encode ( $response );
			} else {
				if (isset ( $headers ['Content-Type'] ) && $headers ['Content-Type'] != 'application/json') {
					$response = array ();
					$data = array ();
					$response ['statusCode'] = HTTP_NOT_ACCEPTABLE;
					$response ['status'] = false;
					$response ['message'] = 'Invalid content type we support only application/json.';
					$response ['data'] = $data;
					echo json_encode ( $response );
				} else {
					$methods = $GLOBALS ['supported_methods']; // $this->config->item('supported_methods');
					$method = $_SERVER ['REQUEST_METHOD'];
					if (! in_array ( $method, $methods )) {
						$response = array ();
						$data = array ();
						$response ['statusCode'] = HTTP_METHOD_NOT_ALLOWED;
						$response ['status'] = false;
						$response ['message'] = 'Invalid request method we support only POST.';
						$response ['data'] = $data;

						echo json_encode ( $response );
					} else {
						return true;
					}
				}
			}
		}
	}
}

$receiver = new Curl_receiver ();
$receiver->index ();
?>