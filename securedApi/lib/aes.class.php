<?php
class AES {
	
	public static function keyGen($orignalKey)
	{
	   if(strlen($orignalKey) >= 16)
	   {
			$key = substr($orignalKey , 0 , 16);
	   }
	   else
	   {
			$key =$orignalKey;
	   }
	   $byte_array = unpack('C*', $key);	   
	   $keysize = 128;
	   $kRaw = $byte_array ;
	   $k = array();
	   $j = 0;
	   for ($i = 0; $i <= $keysize+40; $i = $i + 8)
	   {
		   if(!empty($kRaw[($i / 8) % (count($kRaw) + 1)]))
		   {
			$k[$j] = $kRaw[($i / 8) % (count($kRaw) + 1)];
			$j = $j + 1;
		   }
	   }
	   $str = implode(array_map("chr", $k));
	   return strip_tags($str);
	}
	
	public static function encryptAes($data , $key)
	{
		$CRYPT = $data;
		$blocksize = 16;//Does 16 Refer To 16 Bytes Or 16 Bits? 16 Bytes = 128 Bits.
		$cryptLength = strlen($CRYPT);
		$cryptLengthModuloBlocksize = $cryptLength % $blocksize;
		$pad = $blocksize - $cryptLengthModuloBlocksize;
		$padString = str_repeat(chr($pad), $pad);
		$CRYPT = $CRYPT . $padString;
		$IV = $GLOBALS['encyption_iv'];///"123456789acvbnmj";
		$encryptionPassword = $key;
		$Encrypted_CRYPT = mcrypt_encrypt(MCRYPT_RIJNDAEL_128, $encryptionPassword, $CRYPT, MCRYPT_MODE_CBC , $IV);
		$Base64_Encrypted_CRYPT = base64_encode($Encrypted_CRYPT);
		return $Base64_Encrypted_CRYPT;
	}
	
	public static function decryptAes($key, $data, $iv) 
	{
		$decode_data = base64_decode($data);
	  	$data = mcrypt_decrypt(MCRYPT_RIJNDAEL_128, $key, $decode_data, MCRYPT_MODE_CBC, $iv);	
			
		//$decData = utf8_encode($data);
		return substr($data, 0, strripos($data,'}')+1);
		//exit;
		//return htmlspecialchars(substr($decData, 0, strripos($decData,'}')+1));
		
	}
	
	public static function verify($value , $compareWith)
	{
		if((isset($value) && $value !='' && $value == $compareWith))
		{return 1;}
		else
		{return 0;}
		
	}
	
}


?>