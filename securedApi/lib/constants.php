<?php  //	if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/*
|--------------------------------------------------------------------------
| File and Directory Modes
|--------------------------------------------------------------------------
|
| These prefs are used when checking and setting modes when working
| with the file system.  The defaults are fine on servers with proper
| security, but you may wish (or even need) to change the values in
| certain environments (Apache running a separate process for each
| user, PHP under CGI with Apache suEXEC, etc.).  Octal values should
| always be used to set the mode correctly.
|
*/
define('FILE_READ_MODE', 0644);
define('FILE_WRITE_MODE', 0666);
define('DIR_READ_MODE', 0755);
define('DIR_WRITE_MODE', 0777);

/*
|--------------------------------------------------------------------------
| File Stream Modes
|--------------------------------------------------------------------------
|
| These modes are used when working with fopen()/popen()
|
*/

define('FOPEN_READ',							'rb');
define('FOPEN_READ_WRITE',						'r+b');
define('FOPEN_WRITE_CREATE_DESTRUCTIVE',		'wb'); // truncates existing file data, use with care
define('FOPEN_READ_WRITE_CREATE_DESTRUCTIVE',	'w+b'); // truncates existing file data, use with care
define('FOPEN_WRITE_CREATE',					'ab');
define('FOPEN_READ_WRITE_CREATE',				'a+b');
define('FOPEN_WRITE_CREATE_STRICT',				'xb');
define('FOPEN_READ_WRITE_CREATE_STRICT',		'x+b');

define('HTTP_NON_AUTHORITATIVE_INFORMATION' , '203');  ///authorization info required
define('HTTP_UNAUTHORIZED' , '110');
define('HTTP_NOT_ACCEPTABLE' , '40');
define('HTTP_BAD_GATEWAY' , '502');
define('HTTP_METHOD_NOT_ALLOWED' , '405');
define('HTTP_NOT_FOUND' , '404');
define('HTTP_INVALID_DEVICE_TOKEN' , '109');
//define('HTTP_INVALID_OTP' , '410');
define('HTTP_OTP_EXPIRED' , '412');
define('HTTP_OK' , '200');
define('HTTP_BAD_REQUEST' , '400');
define('RECORD_ALREADY_EXIST' , '11');
define('HTTP_INTERNAL_SERVER_ERROR' , '500');


define('HTTP_VERIFY_MEMBERSHIP_ALREADY_VERIFIED' , '101');
define('HTTP_VERIFY_MEMBERSHIP_INVALID_EMAIL' , '102');
define('HTTP_VERIFY_MEMBERSHIP_INVALID_MEMBERSHIP_ID' , '103');

define('HTTP_REGISTER_INVALID_EMAIL' , '104');
define('HTTP_REGISTER_ALREADY_REGISTER' , '105');

define('HTTP_USER_LOGIN_INVALID_EMAIL' , '106');
define('HTTP_USER_LOGIN_OTP_SENT' , '108');
define('HTTP_USER_LOGIN_INVALID_DEVICE_TOKEN' , '109');

//define('HTTP_OK' , '200');

define('HTTP_SYNC_SETUP_INVALID_USER_ID' , '111');

define('HTTP_FORGET_PASSWORD_INVALID_EMAIL' , '102');

define('HTTP_GET_PROFILE_INVALID_USER_ID' , '111');

define('HTTP_REDEAM_AIRMILES_INVALID_OUT_PIN' , '112');



define('HTTP_INVALID_OTP' , '110');

define('HTTP_INVALID_JSON' , '107');

/*
 | Precision for calculations performed on decimals 
 */
define("PRECISION", 3);

/* End of file constants.php */
/* Location: ./application/config/constants.php */


