<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>..::Club Exec::..</title>
<link href='http://fonts.googleapis.com/css?family=Ubuntu:500' rel='stylesheet' type='text/css'>
<style>
body {
  background:url('bg.jpg?orig=3');
  margin:0px;
  font-family: 'Ubuntu', sans-serif;
}
h1, h2, h3, h4, h5, h6, a {
  margin:0; padding:0;
}
.login {
  margin:0 auto;
  max-width:500px;
}
.login-header {
  color:#fff;
  text-align:center;
  font-size:180%;
}
.login-header h1 {
   text-shadow: 0px 5px 15px #000;
}
.login-form {
  border:2px solid #999;
  background:#2c3e50;
  border-radius:10px;
  box-shadow:0px 0px 10px #000;
}
.login-form h3 {
  text-align:left;
  margin-left:40px;
  color:#fff;
}
.login-form {
  box-sizing:border-box;
  padding-top:15px;
  margin:50px auto;
  text-align:center;
	overflow: hidden;
}
.login input[type="text"],
.login input[type="password"] {
  width: 100%;
	max-width:400px;
  height:30px;
  font-family: 'Ubuntu', sans-serif;
  margin:10px 0;
  border-radius:5px;
  border:2px solid #f2f2f2;
  outline:none;
  padding-left:10px;
}
.login-form input[type="submit"] {
  height:30px;
  width:150px;
  background:#fff;
  border:1px solid #f2f2f2;
  border-radius:20px;
  color: slategrey;
  text-transform:uppercase;
  font-family: 'Ubuntu', sans-serif;
  cursor:pointer;
}
.sign-up{
  color:#f2f2f2;
  margin-left:-400px;
  cursor:pointer;
  text-decoration:underline;
}
.no-access {
  color:#E86850;
  margin:20px 0px 20px -300px;
  text-decoration:underline;
  cursor:pointer;
}
.try-again {
  color:#f2f2f2;
  text-decoration:underline;
  cursor:pointer;
}

/*Media Querie*/
@media only screen and (min-width : 150px) and (max-width : 530px){
img{
width:100%;}
  .login-form h3 {
    text-align:center;
    margin:0;
  }
  .sign-up, .no-access {
    margin:10px 0;
  }
  .login-button {
    margin-bottom:10px;
  }
}
</style>
<script src="jquery.min"></script>
<script>
$( document ).ready(function(){


});

function validateForm()
{
	var password = document.getElementById("password")
  , confirm_password = document.getElementById("confirm_password");

	  if(password.value != confirm_password.value) {
		confirm_password.setCustomValidity("Passwords Don't Match");
		return false;
	  } else {
		confirm_password.setCustomValidity('');
		return true;
	  }
	}

</script>
</head>

<body>


<div class="login">
  <div class="login-header">
  	<img src="logo.png" align="Logo" />
    <h3>Reset Password</h3>
	<?php
	require_once ("lib/config.php");
	if(isset($_REQUEST['change_pass']))
	{
		if((isset($_REQUEST['password']) && $_REQUEST['password']!='') && (isset($_REQUEST['confirm_password']) && $_REQUEST['confirm_password']!=''))
		{
			$pass = $_REQUEST['password'];
			$userId = $_REQUEST['uId'];
			if($_REQUEST['password'] == $_REQUEST['confirm_password'])
			{
				$res = password_hash($pass , PASSWORD_BCRYPT, array('cost' => 12));
				if(mysqli_query( $GLOBALS['link'] ,"UPDATE users SET password = '".$res."' WHERE id = '".$userId."'"))
				{
					$selectUser = mysqli_query( $GLOBALS['link'] ,"SELECT * FROM users WHERE id = '".$userId."'");
					$res = mysqli_fetch_assoc($selectUser );
					$to = $res['email'];
					$from = "support@clubexec.net";
					$subject = 'Password Reset Confirmation';
					$body = '';
					$headers = "From: " . strip_tags("support@clubexec.net") . "\r\n";
					$headers .= "Reply-To: ". strip_tags("support@clubexec.net") . "\r\n";
					$headers .= "MIME-Version: 1.0\r\n";
					///$headers .= '-f info@website.com';
			
					$headers .= "Content-Type: text/html; charset=UTF-8\r\n";
					
			
					$body .= '<p>Dear User, <br> Your password has been updated successfully.</p>';
					
					if(mail($to, $subject, $body, $headers))
					{
						echo '<span style="color:#006600; font-weight:bold;">Password has been reset successfully.</span>';
					}
					else
					{
						echo '<span style="color:#006600; font-weight:bold;">Sorry due to technical resons we cnat send confimation eamil.</span>';
					}
					
				}
			}
			else
			{
				echo '<span style="color:#FF0000; font-weight:bold;">Password and confirm password does not match.</span>';
			}
		}
		else
		{
			echo '<span style="color:#FF0000; font-weight:bold;">Please enter password and confirm password.</span>';
		}
	}
	 ?>
  </div>
  <div class="login-form">
  <form action="" method="post" onsubmit="return validateForm(this);">
    <h3>Password:</h3>
    <input type="password" placeholder="Password" name="password" id="password" required /><br>
    <h3>Comfirm Password:</h3>
    <input type="password" placeholder="Re-Password" name="confirm_password" id="confirm_password" required />
    <br>
    <input type="submit" value="Change Password" class="login-button" name="change_pass"/>
  </form>
    <br>
  </div>
</div>

</body>
</html>
