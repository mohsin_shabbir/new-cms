<?php 		
 		include_once("common.php");
		include_once("includes/header.php");
?>
        
        <script type="text/javascript" >

			$(function() {
			
			$(".delbutton").click(function(){
			var del_id = $(this).attr("id");
			var tbl = 'tbl_contact_us';
			var info = 'id=' + del_id+'&tbl='+ tbl;
			if(confirm("Sure you want to delete this record? There is NO undo!"))
			{
			$.ajax({
			type: "POST",
			url: "delete.php",
			data: info,
			success: function(){
			}
			});
			$(this).parents(".gradeA").animate({ backgroundColor: "red" }, "fast")
			.animate({ opacity: "hide" }, "slow");
			}
			return false;
			});
			});
				
         </script>
         
         <script type="text/javascript" >
			$(function() {
			$(".checkStyle").click(function(){
			var contactUsId = $(this).attr("id");
			var info = 'contactId=' + contactUsId;
			$.ajax({
			type: "POST",
			url: "getAddress.php",
			data: info,
			success: function(){
				//alert('hello');
				location.reload();
			}
			});
			$(this).parents(".gradeX").animate({ backgroundColor: "red" }, "fast")
			.animate({ opacity: "hide" }, "slow");
			
			return false;
			});
			});
         </script>
	 <!-- DataTables CSS -->
<link rel="stylesheet" href="plugins/datatables/dataTables.bootstrap.css">
<div class="content-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">View Contact Enquries</h1>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                          <?php if($_REQUEST['mode'] == "add" || $_REQUEST['mode'] == "update" || $_REQUEST['mode'] == "delete"){?>
                          <div class="alert alert-success alert-dismissable">
                                   <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                                                   
						        	<strong>Well done!</strong> You have successfully complete this operation.
                     			   </div>
                          <?php } ?>
                        </div>
                        <!-- /.panel-heading -->
                        <div class="panel-body">
                            <div class="table-responsive">
                                <table class="table table-striped table-bordered table-hover" id="dataTables-example">
                                    <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>Name</th>
                                            <th>Phone</th>
                                            <th>Email</th>
                                            <th>Subject</th>
                                            <th>Message</th>
                                            <th>Status</th>
                                            <th>Options</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                <?php	
				$sql = "select * from tbl_contact_us order by id desc";
				$rs = mysql_query($sql) or die(mysql_error());
				if(mysql_num_rows($rs) == 0)
				global $i;
				$i = 1;
				while($oA=@mysql_fetch_array($rs))
				{
				  
				?>
                             <tr class="gradeA checkStyle" <?php if(stripslashes(strip_tags($oA['style_status'])) == 0){?> style="color:red" <?php } ?> onclick="return checkStyle();" id = <?php echo $oA["id"]; ?>>
                                            <td><?php print $i; ?></td>
                                            <td><?php print stripslashes(strip_tags($oA['name'])); ?></td>
                                            <td><?php print stripslashes(strip_tags($oA['phone'])); ?></td>
                                            <td><?php print stripslashes(strip_tags($oA['email'])); ?></td>
                                            <td><?php print stripslashes(strip_tags($oA['subject'])); ?></td>
                                            <td>
											<?php print substr(stripslashes(strip_tags($oA['message'])),0, 30);
                                            if(strlen(stripslashes(strip_tags($oA['message'])))>30){?>
                                            <a href="add_pages.php?cid=<?php print $oA['id']; ?>">  View More...</a>
                                            <?php } ?>
                                            </td>
                                            <td class="center">
											<?php 
                                            switch($oA['status']){
                                            case 1:
                                            print 'Active';
                                            break;
                                            case 0:
                                            print '<font color="red">Inactive</font>';
                                            break;
                                            }
                                            ?>
                                            </td>
                                            <td class="center">
                                            
										
											
												<?php 
						  					 if($_SESSION['user_role']==1 || $_SESSION['user_role']==2){?>  
														
                                            <a class="btn btn-danger delbutton" href="#" title="Delete" id = <?php echo $oA["id"]; ?>>
												<i class="icon-trash icon-white"></i> 
												Delete
											</a>
                                            <?php } ?>
                                            
                                            </td>
                                        </tr>
                                   
               <?php $i++;} ?>
               					 </tbody>
                                </table>
                            </div>
                            <!-- /.table-responsive -->
                            
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->

        </div>
        <!-- /#page-wrapper -->
<?php 
include_once("includes/footer.php");
?>
 <!-- DataTables JavaScript -->
    <script src="js/plugins/dataTables/jquery.dataTables.js"></script>
    <script src="js/plugins/dataTables/dataTables.bootstrap.js"></script>

    <script>
  $(document).ready(function() { 
    $('[data-toggle="tooltip"]').tooltip();  
       /// $('#dataTables-example').dataTable();
		 $('#dataTables-example').DataTable({
          "paging": true,
          "lengthChange": true,
          "searching": true,
          "ordering": true,
          "info": true,
          "autoWidth": true,
		  "scrollX": true
        });
    });
    </script>
	<style>
#dataTables-example {
    display: table;
    white-space: nowrap;
}
</style>