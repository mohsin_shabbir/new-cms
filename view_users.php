<?php 		
 		include_once("common.php");
		include_once("includes/header.php");

		if($_SESSION['user_role']!=1){?>
		<script type="text/javascript">
            window.location="index.php";
         </script>	 <?php }

		
?>
        <script type="text/javascript" >
			$(function() {
			$(".say_it").click(function(){
		    var userViewId = $(this).attr("id");
			//var tbl = 'tbl_property';
			var info = 'usersView=' + userViewId;
			
			$.ajax({
			type: "POST",
			url: "getAddress.php",
			data: info,
			success: function(data){
				$(".showPopup").show(1000);
				$("#response").html(data);
				//location.reload();
			}
			});
			$(this).parents(".gradeA").animate({ backgroundColor: "red" }, "fast")
			.animate({ opacity: "show" }, "slow");
			
			return false;
			});
			});
			function closePopup()
			{
				$(".showPopup").hide(1000);
    //// 3.  $.window.close();
			}
         </script>
        <script type="text/javascript" >

			$(function() {
			
			$(".delbutton").click(function(){
			var del_id = $(this).attr("id");
			var tbl = 'tbl_user';
			var info = 'id=' + del_id+'&tbl='+ tbl;
			if(confirm("Sure you want to delete this record? There is NO undo!"))
			{
			$.ajax({
			type: "POST",
			url: "delete.php",
			data: info,
			success: function(){
			}
			});
			$(this).parents(".gradeA").animate({ backgroundColor: "red" }, "fast")
			.animate({ opacity: "hide" }, "slow");
			}
			return false;
			});
			});
         </script>
	 <!-- DataTables CSS -->
	<link rel="stylesheet" href="plugins/datatables/dataTables.bootstrap.css">
	<div class="content-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">View Users</h1>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                          <?php if($_REQUEST['mode'] == "add" || $_REQUEST['mode'] == "update" || $_REQUEST['mode'] == "delete"){?>
                          <div class="alert alert-success alert-dismissable">
                                   <button aria-hidden="true" data-dismiss="alert" class="close" type="button">�</button>
                                                   
						        	<strong>Well done!</strong> You have successfully complete this operation.
                     			   </div>
                          <?php } 
						   if($_SESSION['user_role']==1 || $_SESSION['user_role']==2){?> <a href="add_users.php">Add User</a> <?php } ?>
                          
                        </div>
                        <!-- /.panel-heading -->
                        <div class="panel-body">
                            <div class="table-responsive">
                                <table class="table table-striped table-bordered table-hover" id="dataTables-example">
                                    <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>User Name</th>
                                            <th>Mobile</th>
											<th>Email</th>
											<th>Role</th>
											<th>Created At</th>
                                            <th>Updated At</th>  
                                            <th>Status</th>
                                            <th>Options</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                <?php	
				
					$where = 'status = 1 OR status = 0 ';
				
				$sql = "select * from tbl_user where ".$where." order by id desc";
				$rs = mysql_query($sql) or die(mysql_error());
				if(mysql_num_rows($rs) == 0)
				global $i;
				$i = 1;
				while($oA=@mysql_fetch_array($rs))
				{
				  
				?>
                                        <tr class="gradeA">
                                            <td><?php print $i; ?></td>
                                            <td><?php print stripslashes(strip_tags($oA['fname'])).' '.stripslashes(strip_tags($oA['lname'])); ?></td>
                                            <td><?php print stripslashes(strip_tags($oA['phone_number'])); ?></td>
											<td><?php print stripslashes(strip_tags($oA['email'])); ?></td>
											  <td class="center">
											<?php 
                                            switch($oA['user_role']){
                                            case 1:
                                            print 'High';
                                            break;
                                            case 2:
                                            print 'Medium';
                                            break;
											case 3:
                                            print 'Low';
                                            break;
                                            }
                                            ?>
                                            </td>
											
                                            
                                            <?php /*?><td>
                                 <?php print substr(stripslashes(strip_tags($oA['description'])),0, 30);
								if(strlen(stripslashes(strip_tags($oA['description'])))>30){?>
                                <a href="add_users.php?cid=<?php print $oA['id']; ?>">  View More...</a>
                                <?php } ?>
                                            </td><?php */?>
											 <td><?php 	$seconds = $oA['created_at'] / 1000;echo date("d/m/Y H:i:s", $seconds); ?></td>
											 <td><?php 	$seconds = $oA['updated_at'] / 1000;echo date("d/m/Y H:i:s", $seconds); ?></td>		
                                            <td class="center">
											<?php 
                                            switch($oA['status']){
                                            case 1:
                                            print 'Active';
                                            break;
                                            case 0:
                                            print '<font color="red">Inactive</font>';
                                            break;
                                            }
                                            ?>
                                            </td>
                                            <td class="center">
                                            <a class="initialism fadeandscale_open btn btn-success say_it" href="#fadeandscale" id = <?php echo $oA["id"]; ?>>View </a>
                                            <a class="btn btn-info" title="Edit" href="add_users.php?cid=<?php print $oA['id']; ?>">
                                            <i class="icon-edit icon-white"></i>  
                                            Edit                                            
                                            </a>
                                            <a class="btn btn-danger delbutton" href="#" title="Delete" id = <?php echo $oA["id"]; ?>>
											<i class="icon-trash icon-white"></i> 
											Delete
										</a>
                                            
                                            </td>
                                        </tr>
                                   
               <?php $i++;} ?>
               					 </tbody>
                                </table>
                            </div>
                            <!-- /.table-responsive -->
                            
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->

        </div>
        <!-- /#page-wrapper -->
<?php 
include_once("includes/footer.php");
?>
 <!-- DataTables JavaScript -->
    <script src="js/plugins/dataTables/jquery.dataTables.js"></script>
    <script src="js/plugins/dataTables/dataTables.bootstrap.js"></script>
    <script>
  $(document).ready(function() { 
    $('[data-toggle="tooltip"]').tooltip();  
       /// $('#dataTables-example').dataTable();
		 $('#dataTables-example').DataTable({
          "paging": true,
          "lengthChange": true,
          "searching": true,
          "ordering": true,
          "info": true,
          "autoWidth": true,
		  "scrollX": true
        });
    });
    </script>
	<style>
#dataTables-example {
    display: table;
    white-space: nowrap;
}
</style>